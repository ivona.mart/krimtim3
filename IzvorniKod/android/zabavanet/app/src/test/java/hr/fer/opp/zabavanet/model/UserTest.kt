package hr.fer.opp.zabavanet.model

import hr.fer.opp.zabavanet.utlis.extensionFunctions.createTimestamp
import org.junit.Assert.*
import org.junit.Test

class UserTest{

    @Test
    fun testUserDateOfBirth(){
        var user : User = User("david","je", "bio@tu", "doktor", "bog")

        assertNull(user.dateOfBirthLong)
        assertNull(user.dateOfBirth)

        var stamp = createTimestamp(2019, 11, 15, 12,12)
        user.dateOfBirth = stamp

        assertEquals(user.dateOfBirthLong , stamp.seconds)

        user.dateOfBirth = null
        assertNotNull(user.dateOfBirth)
        assertEquals(user.dateOfBirthLong, stamp.seconds)

    }
}