package hr.fer.opp.zabavanet.utlis.extensionFunctions

import org.junit.Test

import org.junit.Assert.*

class ExtensionFunctionsKtTest {

    @Test
    fun createTimestamp() {

        val stamptime = createTimestamp(
        year = 2019,
        month = 11,
        day = 15,
        hour = 12,
        minute = 12
        )
        //12. je jer metoda koja se koristi broji mjesece od 0
        assertEquals("12:12 15.12.2019" , stamptime.toPattern(null))
        assertEquals("12:12", createTimestamp(2020, 0, 15, 12, 12).toPattern("HH:mm"))
}


    @Test
    fun timeDifferenceDays() {
        assertEquals(2.0, timeDifferenceDays(
            createTimestamp(2019, 12, 15, 12, 12),
            createTimestamp(2019, 12, 17, 12, 12)), 0.1)
        assertEquals(1.0, timeDifferenceDays(
            createTimestamp(2019, 11, 31, 12, 12),
            createTimestamp(2020, 0, 1, 12, 12)), 0.1)
        assertEquals(1.0, timeDifferenceDays(
            createTimestamp(2020, 1, 29, 12, 12),
            createTimestamp(2020, 2, 1, 12, 12)), 0.1)
        assertNotEquals(1.0, timeDifferenceDays(
            createTimestamp(2020, 4, 30, 12, 12),
            createTimestamp(2020, 5, 1, 12, 12)), 0.1)
        assertEquals(1.0, timeDifferenceDays(
            createTimestamp(2020, 4, 20, 12, 12),
            createTimestamp(2020, 4, 21, 12, 12)), 0.1)
        assertEquals(1.0, timeDifferenceDays(
            createTimestamp(2020, 5, 30, 12, 12),
            createTimestamp(2020, 6, 1, 12, 12)), 0.1)
    }

    @Test
    fun toPattern() {
        assertEquals("12:12 15.01.2020", createTimestamp(2020, 0, 15, 12, 12).toPattern(null))
        assertEquals("12:12", createTimestamp(2020, 0, 15, 12, 12).toPattern("HH:mm"))
        assertEquals("15.01.2020", createTimestamp(2020, 0, 15, 12, 12).toPattern("dd.MM.yyyy"))
        assertEquals("2020.12:12", createTimestamp(2020, 0, 15, 12, 12).toPattern("yyyy.HH:mm"))
    }

    @Test
    fun commaSeparated() {
        assertEquals("1, 2, 3", listOf(1,2,3).commaSeparated())
        assertEquals("null", listOf(null).commaSeparated())
        assertEquals("", listOf("").commaSeparated())
    }


}