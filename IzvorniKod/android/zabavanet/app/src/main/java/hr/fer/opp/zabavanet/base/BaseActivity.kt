package hr.fer.opp.zabavanet.base

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import hr.fer.opp.zabavanet.R

abstract class BaseActivity<T : BaseViewModel> : AppCompatActivity() {

    private lateinit var loadingDialog: Dialog
    internal lateinit var viewModel: T

    companion object {
        private const val TAG = "Zabavanet"
    }

    abstract fun getViewModel(): T

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (!::viewModel.isInitialized) {
            this.viewModel = getViewModel()
        }
    }

    fun showMessage(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    fun showErrorAlert(message: String) {
        val alertDialog = AlertDialog.Builder(this).create()
        alertDialog.setTitle("Error")
        alertDialog.setMessage(message)
        alertDialog.show()
    }

    fun logD(message: String) {
        Log.d(TAG, message)
    }

    fun logW(message: String) {
        Log.w(TAG, message)
    }

    fun hideLoading() {
        if (::loadingDialog.isInitialized) {
            loadingDialog.dismiss()
        }
    }

    fun showLoading() {
        if (!::loadingDialog.isInitialized) {
            val builder = AlertDialog.Builder(this)
            builder.setView(R.layout.dialog_loading)
            loadingDialog = builder.create()
            loadingDialog.setCancelable(false)
            loadingDialog.setCanceledOnTouchOutside(false)

        }
        loadingDialog.show()
    }
}
