package hr.fer.opp.zabavanet.utlis.networking.okhttp.di

import dagger.Module
import dagger.Provides
import hr.fer.opp.zabavanet.app.di.diScope.PerApplication
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor

@Module
class OkHttpModule {

    @PerApplication
    @Provides
    fun okHttpClient(): OkHttpClient {
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.apply { loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY }
        return OkHttpClient.Builder()
            //.connectTimeout(60, TimeUnit.SECONDS)  //todo: set custom timeouts if needed
            //.readTimeout(60, TimeUnit.SECONDS)
            //.writeTimeout(60, TimeUnit.SECONDS)
            //.callTimeout(60, TimeUnit.SECONDS)
            .addInterceptor(loggingInterceptor)
            .build()
    }
}