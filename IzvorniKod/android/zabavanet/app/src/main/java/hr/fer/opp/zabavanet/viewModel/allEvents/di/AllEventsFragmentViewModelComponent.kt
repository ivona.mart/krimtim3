package hr.fer.opp.zabavanet.viewModel.allEvents.di

import dagger.Subcomponent
import hr.fer.opp.zabavanet.app.di.diScope.PerViewModel
import hr.fer.opp.zabavanet.viewModel.allEvents.AllEventsFragmentViewModel

@PerViewModel
@Subcomponent
interface AllEventsFragmentViewModelComponent {

    fun inject(allEventsFragmentViewModel: AllEventsFragmentViewModel)
}