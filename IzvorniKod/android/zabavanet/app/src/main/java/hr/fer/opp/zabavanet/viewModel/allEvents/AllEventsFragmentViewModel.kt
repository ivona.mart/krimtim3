package hr.fer.opp.zabavanet.viewModel.allEvents

import androidx.lifecycle.LiveData
import com.google.firebase.firestore.DocumentReference
import hr.fer.opp.zabavanet.app.di.ApplicationComponent
import hr.fer.opp.zabavanet.base.BaseViewModel
import hr.fer.opp.zabavanet.model.Event
import hr.fer.opp.zabavanet.model.Notification
import hr.fer.opp.zabavanet.utlis.networking.firebaseStorage.FirebaseStorageService
import hr.fer.opp.zabavanet.utlis.networking.firestore.FirestoreService
import hr.fer.opp.zabavanet.utlis.session.Session
import hr.fer.opp.zabavanet.view.allEvents.AllEventsFragment
import javax.inject.Inject

class AllEventsFragmentViewModel : BaseViewModel() {

    @Inject
    lateinit var firestoreService: FirestoreService

    @Inject
    lateinit var storage: FirebaseStorageService

    @Inject
    lateinit var session: Session

    override fun injectToAppComponent(applicationComponent: ApplicationComponent) {
        return applicationComponent.plusAllEvents().inject(this)
    }

    fun getAllEvents(): LiveData<List<Event>> {
        return firestoreService.getAllEvents(errors)
    }

    fun getEventTypes(): LiveData<List<String>> {
        return firestoreService.getEventTypes(errors)
    }

    fun getCities(): LiveData<List<String>> {
        return firestoreService.getCities(errors)
    }

    fun getFilteredEvents(filter: AllEventsFragment.Filter): LiveData<List<Event>> {
        return firestoreService.getFilteredEvents(filter, errors)
    }

    fun getCurrentUserReference(): DocumentReference {
        return firestoreService.getDocumentReference(
            FirestoreService.USERS_COLLECTION,
            session.user?.email!!
        )
    }

    fun addNotification(notification: Notification): LiveData<DocumentReference> {
        return firestoreService.addNotification(session.user?.email!!, notification, errors)
    }
}