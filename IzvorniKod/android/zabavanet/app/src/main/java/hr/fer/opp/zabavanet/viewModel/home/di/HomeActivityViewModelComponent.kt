package hr.fer.opp.zabavanet.viewModel.home.di

import dagger.Subcomponent
import hr.fer.opp.zabavanet.app.di.diScope.PerViewModel
import hr.fer.opp.zabavanet.viewModel.home.HomeActivityViewModel

@PerViewModel
@Subcomponent
interface HomeActivityViewModelComponent {

    fun inject(homeActivityViewModel: HomeActivityViewModel)
}