package hr.fer.opp.zabavanet.utlis.adapter

import android.content.Context
import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import hr.fer.opp.zabavanet.R
import hr.fer.opp.zabavanet.model.Attendance
import kotlinx.android.synthetic.main.item_attendance.view.*

class AttendanceAdapter(
    defaultSelection: Attendance?,
    listener: OnItemSelectedListener<Attendance>
) : BaseSingleOptionAdapter<Attendance>(
    R.layout.item_attendance,
    arrayListOf(Attendance.GOING, Attendance.MAYBE_GOING, Attendance.NOT_GOING),
    defaultSelection,
    listener,
    object : StateChangeListener<Attendance> {

        override fun onSelected(item: Attendance, itemView: View) {
            itemView.text_attendance.typeface = Typeface.DEFAULT_BOLD
            itemView.text_attendance.setTextColor(
                ContextCompat.getColor(
                    itemView.context,
                    R.color.black
                )
            )
        }

        override fun onUnselected(item: Attendance, itemView: View) {
            itemView.text_attendance.typeface = Typeface.DEFAULT
            itemView.text_attendance.setTextColor(
                ContextCompat.getColor(
                    itemView.context,
                    android.R.color.tab_indicator_text
                )
            ) // default text color
        }

    }) {

    override fun onBindViewHolder(holder: BaseSingleOptionViewHolder, position: Int) {
        super.onBindViewHolder(holder, position)

        when (position) {
            0 -> holder.itemView.text_attendance.text = Attendance.GOING.text
            1 -> holder.itemView.text_attendance.text = Attendance.MAYBE_GOING.text
            2 -> holder.itemView.text_attendance.text = Attendance.NOT_GOING.text
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, i: Int): BaseSingleOptionViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)
        val recyclerViewItem = inflater.inflate(R.layout.item_attendance, parent, false)
        recyclerViewItem.layoutParams.width =
            (parent.measuredWidth * 0.84).toInt() / 3
        return BaseSingleOptionViewHolder(
            recyclerViewItem
        )
    }

    class CustomLinearLayoutManager(context: Context) :
        LinearLayoutManager(context, HORIZONTAL, false) {

        override fun canScrollHorizontally(): Boolean {
            return false
        }

        override fun canScrollVertically(): Boolean {
            return false
        }
    }
}