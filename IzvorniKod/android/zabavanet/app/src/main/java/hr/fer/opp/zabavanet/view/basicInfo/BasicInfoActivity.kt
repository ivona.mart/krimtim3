package hr.fer.opp.zabavanet.view.basicInfo

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.OpenableColumns
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.firebase.Timestamp
import com.google.firebase.storage.StorageReference
import hr.fer.opp.zabavanet.R
import hr.fer.opp.zabavanet.base.BaseActivity
import hr.fer.opp.zabavanet.model.MediaModel
import hr.fer.opp.zabavanet.model.User
import hr.fer.opp.zabavanet.utlis.codes.ActionCode
import hr.fer.opp.zabavanet.utlis.codes.RequestCode
import hr.fer.opp.zabavanet.utlis.codes.ResultCode
import hr.fer.opp.zabavanet.utlis.dialogManager.DialogManager
import hr.fer.opp.zabavanet.utlis.extensionFunctions.createTimestamp
import hr.fer.opp.zabavanet.utlis.extensionFunctions.toPattern
import hr.fer.opp.zabavanet.utlis.glide.GlideApp
import hr.fer.opp.zabavanet.view.datePicker.DatePickerFragment
import hr.fer.opp.zabavanet.view.profile.ProfileFragment
import hr.fer.opp.zabavanet.viewModel.basicInfo.BasicInfoActivityViewModel
import kotlinx.android.synthetic.main.activity_basic_info.*

class BasicInfoActivity : BaseActivity<BasicInfoActivityViewModel>() {

    companion object {
        const val INTENT_USER = "user_parameter"
        const val MAX_IMAGE_SIZE = 5 * 1024 * 1024 // in bytes

        private const val MAX_CHARACTER_COUNT_USERNAME = 15
        private const val MAX_CHARACTER_COUNT_FIRST_NAME = 30
        private const val MAX_CHARACTER_COUNT_LAST_NAME = 30
    }

    private lateinit var user: User
    private lateinit var dialogManager: DialogManager
    private lateinit var dateOfBirth: Timestamp
    private lateinit var imageModel: MediaModel
    private lateinit var imageRefs: List<StorageReference>

    private val IMAGE_TYPE = "image/*"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_basic_info)

        user = intent.getSerializableExtra(INTENT_USER) as User
        user.dateOfBirth?.let { dateOfBirth = user.dateOfBirth!! }
        dialogManager = DialogManager(this.supportFragmentManager)

        initUI()
    }

    override fun getViewModel(): BasicInfoActivityViewModel {
        return ViewModelProviders.of(this).get(BasicInfoActivityViewModel::class.java)
    }

    private fun initUI() {

        setListeners()

        viewModel.getImageReferences(user.email!!).observe(this, Observer {
            if (it.isNotEmpty()) {
                imageRefs = it as List<StorageReference>

                GlideApp.with(this).load(imageRefs.first())
                    .into(image_profile_picture)
            } else {
                image_profile_picture.setImageResource(R.drawable.default_profile_picture)
            }
        })

        text_email.text = user.email
        user.username?.let { text_username.setText(it) }
        user.firstName?.let { text_first_name.setText(it) }
        user.lastName?.let { text_last_name.setText(it) }
        user.dateOfBirth?.let { text_date_of_birth.text = it.toPattern("dd.MM.yyyy") }
        user.zodiacSign?.let { text_zodiac_sign.text = it }

    }

    private fun setListeners() {

        btn_edit_profile_picture.setOnClickListener {
            openImagePicker()
        }

        btn_edit_date_of_birth.setOnClickListener {
            dialogManager.openDatePickerDialog(object : DatePickerFragment.DatePickerListener {
                override fun onDateSet(year: Int, month: Int, day: Int) {
                    dateOfBirth = createTimestamp(year, month, day, 0, 0)
                    user.dateOfBirth = dateOfBirth
                    text_date_of_birth.text = dateOfBirth.toPattern("dd.MM.yyyy")
                    text_zodiac_sign.text = user.zodiacSign
                }
            })
        }

        btn_save.setOnClickListener {
            if (text_username.text.isBlank()) {
                showMessage("Username can't be empty!")
                return@setOnClickListener
            }
            if (text_username.text.length > MAX_CHARACTER_COUNT_USERNAME) {
                showMessage("Username can't be longer than " + MAX_CHARACTER_COUNT_USERNAME + " characters!")
                return@setOnClickListener
            }
            if (text_first_name.text.isBlank()) {
                showMessage("First name can't be empty!")
                return@setOnClickListener
            }
            if (text_first_name.text.length > MAX_CHARACTER_COUNT_FIRST_NAME) {
                showMessage("First name can't be longer than " + MAX_CHARACTER_COUNT_FIRST_NAME + " characters!")
                return@setOnClickListener
            }
            if (text_last_name.text.isBlank()) {
                showMessage("Last name can't be empty!")
                return@setOnClickListener
            }
            if (text_last_name.text.length > MAX_CHARACTER_COUNT_LAST_NAME) {
                showMessage("Last name can't be longer than " + MAX_CHARACTER_COUNT_LAST_NAME + " characters!")
                return@setOnClickListener
            }

            if (!::dateOfBirth.isInitialized) {
                showMessage(getString(R.string.must_set_date_of_birth))
                return@setOnClickListener
            } else
                if (dateOfBirth >= Timestamp.now()) {
                showMessage("Date of birth must be in the past")
                return@setOnClickListener
            }

            val username = text_username.text.toString()
            val firstName = text_first_name.text.toString()
            val lastName = text_last_name.text.toString()

            user.apply {
                this.username = username
                this.firstName = firstName
                this.lastName = lastName
                this.dateOfBirth = dateOfBirth
            }


            showLoading()
            viewModel.updateBasicUserInfo(user)
                .observe(this, Observer {
                    if (::imageModel.isInitialized) {
                        viewModel.deleteProfilePicture(user).observe(this, Observer {
                            viewModel.uploadProfilePicture(imageModel, user)
                                .observe(this, Observer {
                                    hideLoading()
                                    finishAct(user)
                                })
                        })
                    } else {
                        hideLoading()
                        finishAct(user)
                    }
                })
        }

    }

    private fun finishAct(user: User) {
        viewModel.saveUserToPreferences(user)
        val resultIntent = Intent()
        resultIntent.putExtra(ProfileFragment.INTENT_USER, user)
        when (intent.action) {
            ActionCode.FIRST_EDIT, ActionCode.GOOGLE_SET_BASIC_INFO -> {
                setResult(ResultCode.SET_BASIC_INFO, resultIntent)
            }
            ActionCode.EDIT -> {

            }
        }
        setResult(ResultCode.SUCCESS, resultIntent)
        finish()
    }

    private fun openImagePicker() {
        val intent = Intent(Intent.ACTION_OPEN_DOCUMENT)
        intent.addCategory(Intent.CATEGORY_OPENABLE)
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, false)
        intent.type = IMAGE_TYPE
        startActivityForResult(intent, RequestCode.IMAGE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        data ?: return

        when (requestCode) {
            RequestCode.IMAGE -> {
                data.data?.let { uri ->
                    getFileSize(uri)?.let {
                        if (it > MAX_IMAGE_SIZE) {
                            showMessage(getString(R.string.image_too_big, MAX_IMAGE_SIZE))
                            return
                        } else {
                            imageModel =
                                MediaModel(
                                    uri.toString(),
                                    MediaModel.MediaType.IMAGE
                                )
                            GlideApp.with(this).load(imageModel.uri).into(image_profile_picture)
                        }
                    }
                }
            }
        }
    }

    private fun getFileSize(uri: Uri): Long? {
        var size: Long? = null

        contentResolver.query(uri, null, null, null, null)?.use {
            it.moveToFirst()
            val sizeColumn = it.getColumnIndex(OpenableColumns.SIZE)
            size = it.getLong(sizeColumn)
            it.close()
        }

        return size
    }

    override fun onBackPressed() {
        if (intent.action == ActionCode.FIRST_EDIT || intent.action == ActionCode.GOOGLE_SET_BASIC_INFO) {
            showMessage(getString(R.string.must_enter_data))
        } else {
            super.onBackPressed()
        }
    }
}
