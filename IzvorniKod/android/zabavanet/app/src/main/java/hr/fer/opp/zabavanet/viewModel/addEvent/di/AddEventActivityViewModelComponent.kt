package hr.fer.opp.zabavanet.viewModel.addEvent.di

import dagger.Subcomponent
import hr.fer.opp.zabavanet.app.di.diScope.PerViewModel
import hr.fer.opp.zabavanet.viewModel.addEvent.AddEventActivityViewModel

@PerViewModel
@Subcomponent
interface AddEventActivityViewModelComponent {

    fun inject(addEventActivityViewModel: AddEventActivityViewModel)
}