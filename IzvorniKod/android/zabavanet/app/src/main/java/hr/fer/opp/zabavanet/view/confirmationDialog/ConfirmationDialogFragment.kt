package hr.fer.opp.zabavanet.view.confirmationDialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import hr.fer.opp.zabavanet.R
import kotlinx.android.synthetic.main.dialog_confirmation.*

class ConfirmationDialogFragment(
    private val prompt: String,
    private val positive: String,
    private val negative: String,
    private val listener: DialogListener
) : DialogFragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.dialog_confirmation, container)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        text_confirmation_prompt.text = prompt
        btn_positive.text = positive
        btn_negative.text = negative

        btn_positive.setOnClickListener {
            listener.onPositive()
            dismiss()
        }

        btn_negative.setOnClickListener {
            listener.onNegative()
            dismiss()
        }
    }

    interface DialogListener {
        fun onPositive()
        fun onNegative()
    }
}