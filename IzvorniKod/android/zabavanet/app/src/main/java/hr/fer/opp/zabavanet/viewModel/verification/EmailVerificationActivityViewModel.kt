package hr.fer.opp.zabavanet.viewModel.verification

import androidx.lifecycle.LiveData
import hr.fer.opp.zabavanet.app.di.ApplicationComponent
import hr.fer.opp.zabavanet.base.BaseViewModel
import hr.fer.opp.zabavanet.model.User
import hr.fer.opp.zabavanet.utlis.networking.firebaseAuth.FirebaseAuthService
import hr.fer.opp.zabavanet.utlis.networking.firestore.FirestoreService
import hr.fer.opp.zabavanet.utlis.session.Session
import javax.inject.Inject

class EmailVerificationActivityViewModel : BaseViewModel() {

    @Inject
    lateinit var firebaseAuthService: FirebaseAuthService

    @Inject
    lateinit var firestoreService: FirestoreService

    @Inject
    lateinit var session: Session

    override fun injectToAppComponent(applicationComponent: ApplicationComponent) {
        applicationComponent.plusVerification().inject(this)
    }

    fun sendVerificationEmail() {
        val firebaseUser = firebaseAuthService.getCurrentUser()
        firebaseUser?.sendEmailVerification()
    }

    fun waitForEmailVerification(email: String, password: String): LiveData<Unit> {
        return firebaseAuthService.waitForVerifyEmail(email, password, errors)
    }

    fun createUserInDatabase(user: User): LiveData<Unit> {
        return firestoreService.createUserInDatabase(user, errors)
    }

    fun saveUserToPreferences(user: User) {
        session.user = user
    }
}