package hr.fer.opp.zabavanet.view.eventDetails

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.SimpleItemAnimator
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.firebase.Timestamp
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.storage.StorageReference
import hr.fer.opp.zabavanet.R
import hr.fer.opp.zabavanet.base.BaseActivity
import hr.fer.opp.zabavanet.model.*
import hr.fer.opp.zabavanet.utlis.adapter.*
import hr.fer.opp.zabavanet.utlis.codes.ActionCode
import hr.fer.opp.zabavanet.utlis.codes.RequestCode
import hr.fer.opp.zabavanet.utlis.codes.ResultCode
import hr.fer.opp.zabavanet.utlis.dialogManager.DialogManager
import hr.fer.opp.zabavanet.utlis.extensionFunctions.commaSeparated
import hr.fer.opp.zabavanet.utlis.extensionFunctions.timeDifferenceDays
import hr.fer.opp.zabavanet.utlis.extensionFunctions.toPattern
import hr.fer.opp.zabavanet.utlis.glide.GlideApp
import hr.fer.opp.zabavanet.view.addEvent.AddEventActivity
import hr.fer.opp.zabavanet.view.addReview.AddReviewDialog
import hr.fer.opp.zabavanet.view.editOrganization.EditOrganizationActivity
import hr.fer.opp.zabavanet.view.organizationProfile.OrganizationProfileActivity
import hr.fer.opp.zabavanet.viewModel.eventDetails.EventDetailsActivityViewModel
import kotlinx.android.synthetic.main.activity_event_details.*
import kotlinx.android.synthetic.main.layout_recycler.*
import java.util.*

class EventDetailsActivity : BaseActivity<EventDetailsActivityViewModel>() {

    companion object {
        val eventStack = Stack<Event>()
    }

    private lateinit var imagesRefs: ArrayList<StorageReference>
    private lateinit var videoRefs: ArrayList<StorageReference>
    private lateinit var reviews: ArrayList<Review>
    private lateinit var eventImagesAdapter: ImageRefAdapter
    private lateinit var reviewsAdapter: ReviewAdapter
    private lateinit var attendanceAdapter: AttendanceAdapter
    private lateinit var dialogManager: DialogManager
    private lateinit var organization: Organization
    private lateinit var interestedVisitorsAdapter: InterestedVisitorsAdapter
    private lateinit var bottomSheetDialog: BottomSheetDialog

    private lateinit var attendees: List<InterestedVisitorsAdapter.Attendee>

    private lateinit var going: ArrayList<User>
    private lateinit var maybeGoing: ArrayList<User>
    private lateinit var notGoing: ArrayList<User>

    private val areUsersLoadedLiveData = MutableLiveData<Boolean>()
    private var areUsersLoaded = false

    private lateinit var event: Event

    private lateinit var currentUser: User

    override fun getViewModel(): EventDetailsActivityViewModel {
        return ViewModelProviders.of(this).get(EventDetailsActivityViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_event_details)

        event = eventStack.pop()

        currentUser = viewModel.getUserFromPreferences()

        dialogManager = DialogManager(this.supportFragmentManager)

        viewModel.errors.observe(this, Observer {
            showErrorAlert(it)
        })

        areUsersLoadedLiveData.observe(this, Observer {
            areUsersLoaded = true
        })

        check(intent.action != null) { "Action must be set" }

        initUI(intent.action)
    }

    private fun initUI(action: String?) {
        setMedia(event)
        text_event_title.text = event.title
        text_event_description.text = event.description
        text_event_location.text = event.location
        text_event_address.text = event.address
        text_start_time.text = event.startTime?.toPattern(null)
        text_end_time.text = event.endTime?.toPattern(null)
        text_event_type.text = event.eventType?.commaSeparated()
        text_attendee_numbers.text = getString(
            R.string.attendance,
            event.going?.size,
            event.maybeGoing?.size,
            event.notGoing?.size
        )

        viewModel.getUserFromDatabase(event.organizer!!).observe(this, Observer {
            viewModel.getOrganization(viewModel.getUserRef(event.organizer!!.id))
                .observe(this, Observer {
                    if (it.isNotEmpty()) { // for those who became organizers by manually setting the role
                        organization = it.first()
                        text_organizer.text = organization.organizationName
                        text_organizer.visibility = View.VISIBLE
                    } else {
                        text_organizer.visibility = View.GONE
                    }
                })
        })

        viewModel.getReviews(event.documentReference).observe(this, Observer {
            if (it.isNotEmpty()) {
                reviews = it as ArrayList<Review>
                event.reviews = reviews
                initReviewsRecycler()

                for (review in reviews) {
                    showLoading()
                    review.userRef?.let { userRef ->
                        viewModel.getUserFromDatabase(userRef).observe(this, Observer { user ->
                            review.user = user
                            event.reviews = reviews
                            hideLoading()
                        })
                    }
                }
            }
        })


        video_event_video.setOnPreparedListener {
            it.isLooping = true
        }

        layout_swipe_to_refresh.setOnRefreshListener {
            viewModel.getEvent(event).observe(this, Observer {
                event = it
                initUI(action)
            })
        }

        text_organizer.setOnClickListener {
            startOrganizationProfileActivity(event.organizer!!)
        }

        when (action) {
            ActionCode.OPEN_AS_VISITOR -> {

                showLoading()
                viewModel.getReviews(event.documentReference).observe(this, Observer {
                    if (it.isNotEmpty()) {
                        for (review in it) {
                            if (review.userRef == viewModel.getUserRef(currentUser.email!!)) {
                                btn_add_review.visibility = View.GONE
                                break
                            } else {
                                checkIfEligibleForReview()
                            }
                        }
                    } else {
                        checkIfEligibleForReview()
                    }
                    hideLoading()
                })

                fab_edit_event.visibility = View.GONE

                initAttendanceRecycler()
            }

            ActionCode.OPEN_AS_ORGANIZER -> {

                going = arrayListOf()
                maybeGoing = arrayListOf()
                notGoing = arrayListOf()

                viewModel.getUsersFromDatabase(event.going!!).observe(this, Observer { going ->
                    this.going.addAll(going)

                    viewModel.getUsersFromDatabase(event.maybeGoing!!)
                        .observe(this, Observer { maybeGoing ->
                            this.maybeGoing.addAll(maybeGoing)

                            viewModel.getUsersFromDatabase(event.notGoing!!)
                                .observe(this, Observer { notGoing ->
                                    this.notGoing.addAll(notGoing)

                                    getProfilePictures(
                                        this.going.distinct(),
                                        this.maybeGoing.distinct(),
                                        this.notGoing.distinct()
                                    )
                                })
                        })
                })


                btn_add_review.visibility = View.GONE
                recycler_attendance.visibility = View.GONE
                fab_edit_event.visibility = View.VISIBLE

                fab_edit_event.setOnClickListener {
                    startEditEventActivity()
                }

                text_attendee_numbers.setOnClickListener {
                    if (event.going.isNullOrEmpty() && event.maybeGoing.isNullOrEmpty() && event.notGoing.isNullOrEmpty()) {
                        return@setOnClickListener
                    }

                    if (areUsersLoaded) {
                        showInterestedVisitors(attendees)
                    } else {
                        waitForCompletion()
                    }
                }
            }
        }
    }

    private fun checkIfEligibleForReview() {
        event.going?.let { going ->
            val currentUserRef = viewModel.getCurrentUserReference()
            if (going.isNotEmpty()
                && going.contains(currentUserRef)
                && timeDifferenceDays(event.endTime!!, Timestamp.now()) <= 2
                && timeDifferenceDays(event.startTime!!, Timestamp.now()) > 0
            ) {

                btn_add_review.visibility = View.VISIBLE

                btn_add_review.setOnClickListener {
                    openAddReviewDialog(currentUserRef)
                }
            }
        }
    }

    private fun waitForCompletion() {
        showLoading()
        areUsersLoadedLiveData.observe(this, Observer {
            hideLoading()
            showInterestedVisitors(attendees)
        })
    }

    private fun getProfilePictures(
        going: List<User>,
        maybeGoing: List<User>,
        notGoing: List<User>
    ) {
        attendees = arrayListOf()

        when {
            going.isNotEmpty() -> queryGoing(going, maybeGoing, notGoing)
            maybeGoing.isNotEmpty() -> queryMaybeGoing(maybeGoing, notGoing)
            notGoing.isNotEmpty() -> queryNotGoing(notGoing)
            else -> areUsersLoadedLiveData.postValue(true)
        }
    }

    private fun queryGoing(
        going: List<User>,
        maybeGoing: List<User>,
        notGoing: List<User>
    ) {
        going.forEach { user ->
            var profilePicture: StorageReference? = null
            viewModel.getImageReferences(user.email!!).observe(this, Observer { goingPics ->
                if (goingPics.isNotEmpty()) {
                    profilePicture = goingPics.first()
                }

                (attendees as ArrayList<InterestedVisitorsAdapter.Attendee>).add(
                    InterestedVisitorsAdapter.Attendee(
                        user,
                        profilePicture,
                        InterestedVisitorsAdapter.Attendance.GOING
                    )
                )

                if (maybeGoing.isNotEmpty()) {
                    queryMaybeGoing(maybeGoing, notGoing)
                } else if (notGoing.isNotEmpty()) {
                    queryNotGoing(notGoing)
                } else {
                    areUsersLoadedLiveData.postValue(true)
                }
            })

        }
    }

    private fun queryMaybeGoing(
        maybeGoing: List<User>,
        notGoing: List<User>
    ) {
        maybeGoing.forEach { user ->
            var maybeGoingProfilePicture: StorageReference? = null
            viewModel.getImageReferences(user.email!!).observe(this, Observer { maybePics ->
                if (maybePics.isNotEmpty()) {
                    maybeGoingProfilePicture = maybePics.first()
                }

                (attendees as ArrayList<InterestedVisitorsAdapter.Attendee>).add(
                    InterestedVisitorsAdapter.Attendee(
                        user,
                        maybeGoingProfilePicture,
                        InterestedVisitorsAdapter.Attendance.MAYBE_GOING
                    )
                )

                if (notGoing.isNotEmpty()) {
                    queryNotGoing(notGoing)
                } else {
                    areUsersLoadedLiveData.postValue(true)
                }
            })

        }
    }

    private fun queryNotGoing(notGoing: List<User>) {
        notGoing.forEach { user ->
            var notGoingProfilePicture: StorageReference? = null
            viewModel.getImageReferences(user.email!!)
                .observe(this, Observer { notPics ->
                    if (notPics.isNotEmpty()) {
                        notGoingProfilePicture = notPics.first()
                    }

                    (attendees as ArrayList<InterestedVisitorsAdapter.Attendee>).add(
                        InterestedVisitorsAdapter.Attendee(
                            user,
                            notGoingProfilePicture,
                            InterestedVisitorsAdapter.Attendance.NOT_GOING
                        )
                    )

                    areUsersLoadedLiveData.postValue(true)
                })

        }
    }

    private fun setMedia(event: Event) {
        layout_swipe_to_refresh.isRefreshing = true

        viewModel.getImageReferences(event.documentReference.id).observe(this, Observer {
            if (it.isNotEmpty()) {
                imagesRefs = it as ArrayList<StorageReference>
                initImagesRecycler()
            }
            layout_swipe_to_refresh.isRefreshing = false
        })

        viewModel.getVideoReferences(event.documentReference.id).observe(this, Observer {
            if (it.isNotEmpty()) {
                videoRefs = it as ArrayList<StorageReference>
                showVideo(videoRefs.first())
            } else {
                if (::videoRefs.isInitialized) {
                    videoRefs.clear()
                }
                removeVideo()

            }
            layout_swipe_to_refresh.isRefreshing = false
        })
    }

    private fun initImagesRecycler() {
        eventImagesAdapter = ImageRefAdapter(imagesRefs, GlideApp.with(this))
        recycler_images.adapter = eventImagesAdapter
        recycler_images.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        layout_images.visibility = View.VISIBLE
    }

    private fun showVideo(storageReference: StorageReference) {
        viewModel.getVideoUri(storageReference).observe(this, Observer {
            layout_video.visibility = View.VISIBLE
            video_event_video.setVideoURI(it)
            video_event_video.start()
        })
    }

    private fun removeVideo() {
        video_event_video.stopPlayback()
        layout_video.visibility = View.GONE
    }

    private fun initReviewsRecycler() {
        reviewsAdapter =
            ReviewAdapter(reviews, object : GenericAdapter.OnClickListener<Review> {
                override fun onClick(item: Review) {
                    dialogManager.openSeeReviewDialog(item)
                }
            })
        recycler_reviews.adapter = reviewsAdapter
        recycler_reviews.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        layout_reviews.visibility = View.VISIBLE
    }

    private fun initAttendanceRecycler() {

        if (event.endTime!! > Timestamp.now()) {

            val userRef = viewModel.getCurrentUserReference()

            val defaultSelection = when {
                event.going!!.contains(userRef) -> {
                    Attendance.GOING
                }
                event.maybeGoing!!.contains(userRef) -> {
                    Attendance.MAYBE_GOING
                }
                event.notGoing!!.contains(userRef) -> {
                    Attendance.NOT_GOING
                }
                else -> {
                    null
                }
            }

            attendanceAdapter =
                AttendanceAdapter(
                    defaultSelection,
                    object : BaseSingleOptionAdapter.OnItemSelectedListener<Attendance> {
                        override fun onSelected(item: Attendance) {
                            updateEventWithAttendance(event, userRef, item)
                        }
                    })

            recycler_attendance.adapter = attendanceAdapter
            recycler_attendance.setHasFixedSize(true)
            recycler_attendance.layoutManager = AttendanceAdapter.CustomLinearLayoutManager(this)
            (recycler_attendance.itemAnimator as SimpleItemAnimator).supportsChangeAnimations =
                false
        } else {
            recycler_attendance.visibility = View.GONE
        }
    }

    private fun updateEventWithAttendance(
        event: Event,
        userRef: DocumentReference,
        attendance: Attendance
    ) {
        when (attendance) {
            Attendance.GOING -> {
                val maybe = event.maybeGoing as ArrayList
                maybe.remove(userRef)
                event.maybeGoing = maybe

                val not = event.notGoing as ArrayList
                not.remove(userRef)
                event.notGoing = not

                if (!event.going!!.contains(userRef)) {
                    val attending = event.going as ArrayList
                    attending.add(userRef)
                    event.going = attending
                }
            }

            Attendance.MAYBE_GOING -> {
                if (!event.maybeGoing!!.contains(userRef)) {
                    val maybe = event.maybeGoing as ArrayList
                    maybe.add(userRef)
                    event.maybeGoing = maybe
                }

                val not = event.notGoing as ArrayList
                not.remove(userRef)
                event.notGoing = not

                val attending = event.going as ArrayList
                attending.remove(userRef)
                event.going = attending
            }

            Attendance.NOT_GOING -> {
                val maybe = event.maybeGoing as ArrayList
                maybe.remove(userRef)
                event.maybeGoing = maybe

                if (!event.notGoing!!.contains(userRef)) {
                    val not = event.notGoing as ArrayList
                    not.add(userRef)
                    event.notGoing = not
                }

                val attending = event.going as ArrayList
                attending.remove(userRef)
                event.going = attending
            }
        }

        text_attendee_numbers.text = getString(
            R.string.attendance,
            event.going!!.size,
            event.maybeGoing!!.size,
            event.notGoing!!.size
        )

        viewModel.updateAttendance(event, userRef, attendance)
            .observe(this@EventDetailsActivity, Observer {
                logD("Attendance updated")
            })
    }

    private fun startEditEventActivity() {
        val intent = Intent(this, AddEventActivity::class.java)
        intent.putExtra(AddEventActivity.USER_PARAM, currentUser)
        intent.action = ActionCode.EDIT

        AddEventActivity.event = event

        if (::imagesRefs.isInitialized) {
            AddEventActivity.imageRefs = imagesRefs
        }
        if (::videoRefs.isInitialized && videoRefs.isNotEmpty()) {
            AddEventActivity.videoRefs = videoRefs
        }
        startActivityForResult(intent, RequestCode.EDIT)
    }

    private fun openAddReviewDialog(currentUser: DocumentReference) {
        dialogManager.openAddReviewDialog(object : AddReviewDialog.DialogListener {
            override fun onSave(text: String, numberOfStars: Int) {

                btn_add_review.visibility = View.GONE

                val review = Review(Timestamp.now(), text, currentUser, numberOfStars)

                showLoading()
                viewModel.createReviewInDatabase(event, review)
                    .observe(this@EventDetailsActivity, Observer {
                        viewModel.getUserFromDatabase(review.userRef!!)
                            .observe(this@EventDetailsActivity, Observer { user ->
                                review.user = user
                                if (::reviews.isInitialized) {
                                    reviews.add(review)
                                    event.reviews = reviews
                                } else {
                                    reviews = arrayListOf(review)
                                }

                                if (::reviewsAdapter.isInitialized) {
                                    reviewsAdapter.notifyDataSetChanged()
                                } else {
                                    initReviewsRecycler()
                                }

                                hideLoading()
                            })
                    })
            }
        })
    }

    private fun startOrganizationProfileActivity(userRef: DocumentReference) {
        showLoading()
        viewModel.getUserFromDatabase(userRef).observe(this, Observer {
            hideLoading()
            val intent = Intent(this, OrganizationProfileActivity::class.java)
            intent.putExtra(EditOrganizationActivity.INTENT_USER, it)
            intent.action = ActionCode.SHOW_ORGANIZATION_PROFILE

            startActivity(intent)
        })
    }

    private fun showInterestedVisitors(attendees: List<InterestedVisitorsAdapter.Attendee>) {

        val distinct = attendees.distinct()
        if (!::bottomSheetDialog.isInitialized) {
            bottomSheetDialog = BottomSheetDialog(this)
            bottomSheetDialog.setContentView(R.layout.layout_recycler)

            interestedVisitorsAdapter = InterestedVisitorsAdapter(
                distinct,
                GlideApp.with(this),
                object : InterestedVisitorsAdapter.OnClickListener {
                    override fun onClick(item: InterestedVisitorsAdapter.Attendee, position: Int) {

                    }

                    override fun onLongClick(
                        item: InterestedVisitorsAdapter.Attendee,
                        position: Int
                    ): Boolean {
                        return false
                    }
                })

            bottomSheetDialog.recycler.adapter = interestedVisitorsAdapter
            bottomSheetDialog.recycler.layoutManager = LinearLayoutManager(this)
        } else {
            interestedVisitorsAdapter.userList = distinct
            interestedVisitorsAdapter.notifyDataSetChanged()
        }

        bottomSheetDialog.show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == RequestCode.EDIT) {
            when (resultCode) {
                ResultCode.SUCCESS -> {
                    showMessage(getString(R.string.event_updated))
                    require(eventStack.isNotEmpty()) { "Must push last event to stack" }
                    initUI(ActionCode.OPEN_AS_ORGANIZER)
                }
                ResultCode.FAILURE -> showMessage(getString(R.string.event_update_failed))
                ResultCode.DISCARDED -> showMessage(getString(R.string.discarded))
            }
        }
    }
}
