package hr.fer.opp.zabavanet.viewModel.myEvents.di

import dagger.Subcomponent
import hr.fer.opp.zabavanet.viewModel.myEvents.MyEventsFragmentViewModel

@Subcomponent
interface MyEventsFragmentViewModelComponent {

    fun inject(myEventsFragmentViewModel: MyEventsFragmentViewModel)
}