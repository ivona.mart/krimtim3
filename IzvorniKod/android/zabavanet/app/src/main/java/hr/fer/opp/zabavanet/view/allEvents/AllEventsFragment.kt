package hr.fer.opp.zabavanet.view.allEvents

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.firebase.Timestamp
import hr.fer.opp.zabavanet.R
import hr.fer.opp.zabavanet.base.BaseFragment
import hr.fer.opp.zabavanet.model.Event
import hr.fer.opp.zabavanet.model.Notification
import hr.fer.opp.zabavanet.utlis.adapter.EventsAdapter
import hr.fer.opp.zabavanet.utlis.codes.ActionCode
import hr.fer.opp.zabavanet.utlis.dialogManager.DialogManager
import hr.fer.opp.zabavanet.utlis.extensionFunctions.createTimestamp
import hr.fer.opp.zabavanet.utlis.extensionFunctions.subtractDays
import hr.fer.opp.zabavanet.utlis.extensionFunctions.toPattern
import hr.fer.opp.zabavanet.view.datePicker.DatePickerFragment
import hr.fer.opp.zabavanet.view.eventDetails.EventDetailsActivity
import hr.fer.opp.zabavanet.view.timePicker.TimePickerFragment
import hr.fer.opp.zabavanet.viewModel.allEvents.AllEventsFragmentViewModel
import kotlinx.android.synthetic.main.dialog_filters.*
import kotlinx.android.synthetic.main.fragment_all_events.*

class AllEventsFragment : BaseFragment<AllEventsFragmentViewModel>() {

    private var defaultTimeAfter = Timestamp.now().subtractDays(1)

    private lateinit var eventsAdapter: EventsAdapter
    private lateinit var bottomSheetDialog: BottomSheetDialog
    private lateinit var cities: List<String>
    private lateinit var eventTypes: List<String>
    private var filter = Filter(null, null, defaultTimeAfter, null)
    private lateinit var dialogManager: DialogManager
    private lateinit var DONT_FILTER: String
    private lateinit var events: List<EventsAdapter.EventType>

    companion object {

        @JvmStatic
        fun newInstance() =
            AllEventsFragment()
    }

    override fun getViewModel(): AllEventsFragmentViewModel {
        return ViewModelProviders.of(this).get(AllEventsFragmentViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_all_events, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.errors.observe(this, Observer {
            showErrorAlert(it)
            hideLoading()
        })

        initUI(true)
    }

    private fun initUI(showLoadingDialog: Boolean) {

        DONT_FILTER = getString(R.string.don_t_filter)

        if(showLoadingDialog) showLoading() else layout_swipe_to_refresh.isRefreshing = true
        viewModel.getFilteredEvents(filter).observe(this, Observer {
            initEventsRecycler(it)
            hideLoading()
            layout_swipe_to_refresh.isRefreshing = false
        })

        fab_set_filters.setOnClickListener {
            openFilerDialog()
        }

        layout_swipe_to_refresh.setOnRefreshListener {
            initUI(false)
        }
    }

    private fun convertEventToEventType(events: List<Event>): List<EventsAdapter.EventType> {
        val eventTypes = arrayListOf<EventsAdapter.EventType>()
        val userRef = viewModel.getCurrentUserReference()
        for (event in events) {
            if (event.organizer == userRef) {
                (eventTypes).add(
                    EventsAdapter.EventType(
                        event,
                        EventsAdapter.ViewType.ORGANIZER
                    )
                )
            } else if (event.going!!.contains(userRef)) {
                (eventTypes).add(
                    EventsAdapter.EventType(
                        event,
                        EventsAdapter.ViewType.GOING
                    )
                )
            } else if (event.maybeGoing!!.contains(userRef)) {
                (eventTypes).add(
                    EventsAdapter.EventType(
                        event,
                        EventsAdapter.ViewType.MAYBE_GOING
                    )
                )
            } else {
                (eventTypes).add(
                    EventsAdapter.EventType(
                        event,
                        EventsAdapter.ViewType.REGULAR
                    )
                )
            }
        }
        return eventTypes
    }

    private fun initEventsRecycler(events: List<Event>) {
        val eventTypes = convertEventToEventType(events)
        if (!::eventsAdapter.isInitialized) {
            eventsAdapter = EventsAdapter(
                eventTypes,
                object : EventsAdapter.OnClickListener {
                    override fun onClick(item: EventsAdapter.EventType) {
                        startEventDetailsActivity(item)
                    }

                    override fun onLongClick(item: EventsAdapter.EventType): Boolean {
                        return false
                    }
                }
            )

            recycler_all_events.adapter = eventsAdapter
            recycler_all_events.layoutManager = LinearLayoutManager(context)
        } else {
            eventsAdapter.eventList = eventTypes
            eventsAdapter.notifyDataSetChanged()
        }
        this.events = eventTypes
    }

    private fun startEventDetailsActivity(event: EventsAdapter.EventType) {
        val intent = Intent(context, EventDetailsActivity::class.java)
        if (event.viewType == EventsAdapter.ViewType.ORGANIZER) {
            intent.action = ActionCode.OPEN_AS_ORGANIZER

        } else {
            intent.action = ActionCode.OPEN_AS_VISITOR
        }
        EventDetailsActivity.eventStack.push(event.event)
        startActivity(intent)
    }

    private fun openFilerDialog() {
        if (!::cities.isInitialized || !::eventTypes.isInitialized) {
            showLoading()
            viewModel.getCities().observe(this, Observer { cities ->
                this.cities = arrayListOf(DONT_FILTER)
                (this.cities as ArrayList).addAll(cities)
                viewModel.getEventTypes().observe(this, Observer { eventTypes ->
                    this.eventTypes = arrayListOf(DONT_FILTER)
                    (this.eventTypes as ArrayList).addAll(eventTypes)
                    openFilerDialog()
                })
            })
        } else {
            if (!::dialogManager.isInitialized) {
                dialogManager = DialogManager(activity!!.supportFragmentManager)
            }

            if (!::bottomSheetDialog.isInitialized) {
                bottomSheetDialog = BottomSheetDialog(context!!)

                bottomSheetDialog.apply {
                    setContentView(R.layout.dialog_filters)

                    text_after.text = filter.timeAfter.toPattern(null)

                    val citiesAdapter =
                        ArrayAdapter<String>(
                            context,
                            R.layout.item_spinner_item,
                            cities
                        )
                    val eventTypesAdapter =
                        ArrayAdapter<String>(
                            context,
                            R.layout.item_spinner_item,
                            eventTypes
                        )

                    spinner_cities.adapter = citiesAdapter
                    spinner_event_type.adapter = eventTypesAdapter

                    setBottomSheetListeners()
                }
            }

            hideLoading()
            bottomSheetDialog.show()
        }
    }

    private fun setBottomSheetListeners() {
        bottomSheetDialog.apply {
            btn_edit_after_time.setOnClickListener {
                dialogManager.openDatePickerDialog(object :
                    DatePickerFragment.DatePickerListener {
                    override fun onDateSet(year: Int, month: Int, day: Int) {
                        dialogManager.openTimePickerDialog(
                            null,
                            null,
                            object : TimePickerFragment.TimePickerListener {
                                override fun onTimeSet(hourOfDay: Int, minute: Int) {
                                    filter.timeAfter =
                                        createTimestamp(year, month, day, hourOfDay, minute)
                                    updateFilterDialog()
                                }
                            })
                    }
                })
            }

            spinner_cities.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                    val current = cities[p2]

                    if (current == DONT_FILTER) {
                        filter.city = null
                    } else {
                        filter.city = current
                    }
                }

                override fun onNothingSelected(p0: AdapterView<*>?) {
                }
            }

            spinner_event_type.onItemSelectedListener =
                object : AdapterView.OnItemSelectedListener {
                    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                        val current = eventTypes[p2]

                        if (current == DONT_FILTER) {
                            filter.eventType = null
                        } else {
                            filter.eventType = current
                        }
                    }

                    override fun onNothingSelected(p0: AdapterView<*>?) {
                    }
                }

            btn_edit_before_time.setOnClickListener {
                dialogManager.openDatePickerDialog(object :
                    DatePickerFragment.DatePickerListener {
                    override fun onDateSet(year: Int, month: Int, day: Int) {
                        dialogManager.openTimePickerDialog(
                            null,
                            null,
                            object : TimePickerFragment.TimePickerListener {
                                override fun onTimeSet(hourOfDay: Int, minute: Int) {
                                    filter.timeBefore =
                                        createTimestamp(year, month, day, hourOfDay, minute)
                                    updateFilterDialog()
                                }
                            })
                    }
                })
            }

            btn_reset_filter.setOnClickListener {
                filter.city = null
                filter.eventType = null
                filter.timeAfter = null
                filter.timeBefore = null
                updateFilterDialog()

            }

            btn_apply_filter.setOnClickListener {
                applyFilter()
            }

            btn_notify_me.setOnClickListener {
                viewModel.addNotification(
                    Notification(
                        getString(R.string.notification_title),
                        getString(R.string.notification_content),
                        filter.eventType,
                        filter.city,
                        filter.timeAfter,
                        filter.timeBefore
                    )
                ).observe(this@AllEventsFragment, Observer {
                    showMessage(getString(R.string.subscribed))
                })
            }
        }
    }

    private fun applyFilter() {
        showLoading()

        viewModel.getFilteredEvents(filter)
            .observe(this@AllEventsFragment, Observer {
                initEventsRecycler(it)
                hideLoading()
            })

        dismissFilterDialog()
    }

    private fun dismissFilterDialog() {
        if (::bottomSheetDialog.isInitialized) {
            bottomSheetDialog.dismiss()
        }
    }

    private fun updateFilterDialog() {
        if (::bottomSheetDialog.isInitialized) {
            bottomSheetDialog.apply {
                filter.city?.let {
                    spinner_cities.setSelection(cities.indexOf(it))
                } ?: spinner_cities.setSelection(0)

                filter.eventType?.let {
                    spinner_event_type.setSelection(eventTypes.indexOf(it))
                } ?: spinner_event_type.setSelection(0)

                text_after.text = filter.timeAfter.toPattern(null)

                text_before.text = filter.timeBefore.toPattern(null)
            }
        }
    }

    data class Filter(
        var city: String?,
        var eventType: String?,
        var timeAfter: Timestamp?,
        var timeBefore: Timestamp?
    )

    override fun onResume() {
        super.onResume()

        initUI(true)
    }
}
