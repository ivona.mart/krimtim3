package hr.fer.opp.zabavanet.utlis.adapter

import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.storage.StorageReference
import hr.fer.opp.zabavanet.R
import hr.fer.opp.zabavanet.utlis.glide.GlideRequests
import kotlinx.android.synthetic.main.item_image.view.*

class ImagesCombinedAdapter(
    var imageList: List<Model>,
    glideRequests: GlideRequests,
    private val listener: OnClickListener
) :
    RecyclerView.Adapter<ImagesCombinedAdapter.ViewHolder>() {

    private val requestBuilder = glideRequests.asDrawable().fitCenter()

    override fun getItemCount(): Int {
        return imageList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val current = imageList[position]

        when (holder.itemViewType) {
            Type.REFERENCE.intValue -> {
                requestBuilder.clone().load(current.ref).into(holder.itemView.image)
            }

            Type.URI.intValue -> {
                requestBuilder.clone().load(current.uri).into(holder.itemView.image)
            }
        }

        holder.itemView.setOnClickListener {
            listener.onClick(current, position)
        }

        holder.itemView.setOnLongClickListener {
            listener.onLongClick(current, position)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_image, parent, false)

        return ViewHolder(view)
    }

    override fun getItemViewType(position: Int): Int {
        return imageList[position].type.intValue
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    interface OnClickListener {
        fun onClick(item: Model, position: Int)
        fun onLongClick(item: Model, position: Int): Boolean
    }

    data class Model(val ref: StorageReference?, val uri: Uri?, val type: Type)

    enum class Type(val intValue: Int) {
        REFERENCE(1),
        URI(2)
    }
}