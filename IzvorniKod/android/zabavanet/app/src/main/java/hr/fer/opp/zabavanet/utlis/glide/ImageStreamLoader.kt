package hr.fer.opp.zabavanet.utlis.glide

import com.bumptech.glide.load.Options
import com.bumptech.glide.load.model.ModelLoader
import com.bumptech.glide.load.model.ModelLoaderFactory
import com.bumptech.glide.load.model.MultiModelLoaderFactory
import com.bumptech.glide.signature.ObjectKey
import hr.fer.opp.zabavanet.model.MediaModel
import okhttp3.OkHttpClient
import java.io.InputStream

class ImageStreamLoader(private val okHttpClient: OkHttpClient) :
    ModelLoader<MediaModel, InputStream> {

    class Factory(private val okHttpClient: OkHttpClient) :
        ModelLoaderFactory<MediaModel, InputStream> {
        override fun build(multiFactory: MultiModelLoaderFactory): ModelLoader<MediaModel, InputStream> {
            return ImageStreamLoader(okHttpClient)
        }

        override fun teardown() {
            // nothing
        }
    }

    override fun buildLoadData(
        model: MediaModel,
        width: Int,
        height: Int,
        options: Options
    ): ModelLoader.LoadData<InputStream>? {

        val uri = model.uri

        return ModelLoader.LoadData(ObjectKey(uri), ImageStreamFetcher(okHttpClient, uri))
    }

    override fun handles(model: MediaModel): Boolean {
        return true
    }
}