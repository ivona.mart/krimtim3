package hr.fer.opp.zabavanet.viewModel.editOrganization

import android.net.Uri
import androidx.lifecycle.LiveData
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.storage.StorageReference
import hr.fer.opp.zabavanet.app.di.ApplicationComponent
import hr.fer.opp.zabavanet.base.BaseViewModel
import hr.fer.opp.zabavanet.model.MediaModel
import hr.fer.opp.zabavanet.model.Organization
import hr.fer.opp.zabavanet.model.User
import hr.fer.opp.zabavanet.utlis.networking.firebaseStorage.FirebaseStorageService
import hr.fer.opp.zabavanet.utlis.networking.firestore.FirestoreService
import hr.fer.opp.zabavanet.utlis.session.Session
import javax.inject.Inject

class EditOrganizationActivityViewModel : BaseViewModel() {

    @Inject
    lateinit var storage: FirebaseStorageService

    @Inject
    lateinit var firestoreService: FirestoreService

    @Inject
    lateinit var session: Session

    override fun injectToAppComponent(applicationComponent: ApplicationComponent) {
        applicationComponent.plusEditOrganization().inject(this)
    }

    fun updateOrganizationInfo(user: User, organization: Organization): LiveData<Unit> {
        return firestoreService.updateOrganizationInfo(user, organization, errors)
    }

    fun deleteOrganizationPicture(organizationId: String): LiveData<Unit> {
        return storage.deleteProfilePicture(organizationId, errors)
    }

    fun uploadOrganizationPicture(
        mediaModel: MediaModel,
        organizationId: String
    ): LiveData<StorageReference> {
        return storage.uploadProfilePicture(
            mediaModel,
            organizationId,
            errors
        )
    }

    fun createOrganizationInDatabase(
        user: User,
        organization: Organization
    ): LiveData<DocumentReference> {
        return firestoreService.createOrganizationInDatabase(user, organization, errors)
    }

    fun saveUserToPreferences(user: User) {
        session.user = user
    }

    fun getOrganizationPictureRef(user: User): LiveData<List<StorageReference>> {
        return storage.getImageReferences(user.organizationId!!, errors)
    }

    fun getUri(storageReference: StorageReference): LiveData<Uri> {
        return storage.getUri(storageReference, errors)
    }
}