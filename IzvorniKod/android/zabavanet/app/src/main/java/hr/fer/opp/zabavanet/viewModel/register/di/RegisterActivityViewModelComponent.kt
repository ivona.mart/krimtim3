package hr.fer.opp.zabavanet.viewModel.register.di

import dagger.Subcomponent
import hr.fer.opp.zabavanet.app.di.diScope.PerViewModel
import hr.fer.opp.zabavanet.viewModel.register.RegisterActivityViewModel

@PerViewModel
@Subcomponent
interface RegisterActivityViewModelComponent {

    fun inject(registerActivityViewModel: RegisterActivityViewModel)
}