package hr.fer.opp.zabavanet.app.di.diScope

import javax.inject.Scope

@Scope
@kotlin.annotation.Retention
annotation class PerApplication