package hr.fer.opp.zabavanet.viewModel.eventDetails.di

import dagger.Subcomponent
import hr.fer.opp.zabavanet.viewModel.eventDetails.EventDetailsActivityViewModel

@Subcomponent
interface EventDetailsActivityViewModelComponent {

    fun inject(eventDetailsActivityViewModel: EventDetailsActivityViewModel)
}