package hr.fer.opp.zabavanet.utlis.adapter

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import hr.fer.opp.zabavanet.R
import hr.fer.opp.zabavanet.model.Event
import hr.fer.opp.zabavanet.utlis.extensionFunctions.toPattern
import kotlinx.android.synthetic.main.item_event_short.view.*

class EventsAdapter(
    var eventList: List<EventType>,
    private val listener: OnClickListener
) : RecyclerView.Adapter<EventsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)
        val recyclerViewItem = inflater.inflate(R.layout.item_event_short, parent, false)
        return ViewHolder(recyclerViewItem)
    }

    override fun getItemCount(): Int {
        return eventList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val current = eventList[position]
        val event = current.event

        holder.itemView.text_title.text = event.title
        holder.itemView.text_description.text = event.description
        holder.itemView.text_start_time_end_time.text = holder.itemView.context.getString(
            R.string.from_time_to_time,
            event.startTime?.toPattern(null),
            event.endTime?.toPattern(null)
        )

        holder.itemView.setOnClickListener {
            listener.onClick(current)
        }

        holder.itemView.setOnLongClickListener {
            listener.onLongClick(current)
        }

        when (holder.itemViewType) {
            ViewType.ORGANIZER.intValue -> {

                holder.itemView.background =
                    ContextCompat.getDrawable(
                        holder.itemView.context,
                        R.drawable.background_event_organizer
                    )
            }

            ViewType.GOING.intValue -> {
                holder.itemView.setBackgroundColor(Color.GREEN)
                holder.itemView.background =
                    ContextCompat.getDrawable(
                        holder.itemView.context,
                        R.drawable.background_event_going
                    )
            }

            ViewType.MAYBE_GOING.intValue -> {
                holder.itemView.background =
                    ContextCompat.getDrawable(
                        holder.itemView.context,
                        R.drawable.background_event_maybe_going
                    )
            }

            ViewType.REGULAR.intValue -> {

            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return eventList[position].viewType.intValue
    }

    interface OnClickListener {
        fun onClick(item: EventType)
        fun onLongClick(item: EventType): Boolean
    }

    data class EventType(val event: Event, val viewType: ViewType)

    enum class ViewType(val intValue: Int) {
        ORGANIZER(1),
        GOING(2),
        REGULAR(3),
        MAYBE_GOING(4)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}