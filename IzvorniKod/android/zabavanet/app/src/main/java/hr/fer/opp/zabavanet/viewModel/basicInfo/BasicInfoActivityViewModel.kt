package hr.fer.opp.zabavanet.viewModel.basicInfo

import androidx.lifecycle.LiveData
import com.google.firebase.storage.StorageReference
import hr.fer.opp.zabavanet.app.di.ApplicationComponent
import hr.fer.opp.zabavanet.base.BaseViewModel
import hr.fer.opp.zabavanet.model.MediaModel
import hr.fer.opp.zabavanet.model.User
import hr.fer.opp.zabavanet.utlis.networking.firebaseAuth.FirebaseAuthService
import hr.fer.opp.zabavanet.utlis.networking.firebaseStorage.FirebaseStorageService
import hr.fer.opp.zabavanet.utlis.networking.firestore.FirestoreService
import hr.fer.opp.zabavanet.utlis.session.Session
import javax.inject.Inject

class BasicInfoActivityViewModel : BaseViewModel() {

    @Inject
    lateinit var firestoreService: FirestoreService

    @Inject
    lateinit var firebaseAuthService: FirebaseAuthService

    @Inject
    lateinit var storage: FirebaseStorageService

    @Inject
    lateinit var session: Session

    override fun injectToAppComponent(applicationComponent: ApplicationComponent) {
        applicationComponent.plusBasicInfo().inject(this)
    }

    fun updateBasicUserInfo(
        user: User
    ): LiveData<Any> {
        return firestoreService.updateBasicUserInfo(user, errors)
    }

    fun deleteProfilePicture(user: User): LiveData<Unit> {
        return storage.deleteProfilePicture(user.email!!, errors)
    }

    fun uploadProfilePicture(mediaModel: MediaModel, user: User): LiveData<StorageReference> {
        return storage.uploadProfilePicture(mediaModel, user.email!!, errors)
    }

    fun saveUserToPreferences(user: User){
        session.user = user
    }

    fun getImageReferences(email: String): LiveData<List<StorageReference>> {
        return storage.getImageReferences(email, errors)
    }

}