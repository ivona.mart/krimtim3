package hr.fer.opp.zabavanet.viewModel.addEvent

import android.net.Uri
import androidx.lifecycle.LiveData
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.storage.StorageReference
import hr.fer.opp.zabavanet.app.di.ApplicationComponent
import hr.fer.opp.zabavanet.base.BaseViewModel
import hr.fer.opp.zabavanet.model.Event
import hr.fer.opp.zabavanet.model.User
import hr.fer.opp.zabavanet.utlis.networking.firebaseStorage.FirebaseStorageService
import hr.fer.opp.zabavanet.utlis.networking.firestore.FirestoreService
import hr.fer.opp.zabavanet.utlis.session.Session
import javax.inject.Inject

class AddEventActivityViewModel : BaseViewModel() {

    @Inject
    lateinit var firestoreService: FirestoreService

    @Inject
    lateinit var storage: FirebaseStorageService

    @Inject
    lateinit var session: Session

    override fun injectToAppComponent(applicationComponent: ApplicationComponent) {
        applicationComponent.plusAddEvent().inject(this)
    }

    fun createEventInDatabase(event: Event): LiveData<DocumentReference> {
        return firestoreService.createEventInDatabase(event, errors)
    }

    fun getUserDocRef(user: User): DocumentReference? {
        return firestoreService.getDocumentReference(
            FirestoreService.USERS_COLLECTION,
            user.email!!
        )
    }

    fun getCities(): LiveData<List<String>> {
        return firestoreService.getCities(errors)
    }

    fun getEventTypes(): LiveData<List<String>> {
        return firestoreService.getEventTypes(errors)
    }

    fun updateEventInDatabase(event: Event): LiveData<Unit> {
        return firestoreService.updateEvent(event, errors)
    }

    fun getUri(storageReference: StorageReference): LiveData<Uri> {
        return storage.getUri(storageReference, errors)
    }

    fun deleteMedia(refs: List<StorageReference>) {
        storage.deleteMedia(refs)
    }
}