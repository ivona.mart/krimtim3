package hr.fer.opp.zabavanet.viewModel.payment.di

import dagger.Subcomponent
import hr.fer.opp.zabavanet.app.di.diScope.PerViewModel
import hr.fer.opp.zabavanet.viewModel.payment.PaymentActivityViewModel


@PerViewModel
@Subcomponent
interface PaymentViewModelComponent {
    fun inject(paymentActivityViewModel: PaymentActivityViewModel)
}