package hr.fer.opp.zabavanet.view.register

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import hr.fer.opp.zabavanet.R
import hr.fer.opp.zabavanet.base.BaseActivity
import hr.fer.opp.zabavanet.view.emailVerification.EmailVerificationActivity
import hr.fer.opp.zabavanet.viewModel.register.RegisterActivityViewModel
import kotlinx.android.synthetic.main.activity_register.*

class RegisterActivity : BaseActivity<RegisterActivityViewModel>() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        initUI()
    }

    override fun getViewModel(): RegisterActivityViewModel {
        return ViewModelProviders.of(this).get(RegisterActivityViewModel::class.java)
    }

    private fun initUI() {
        viewModel.errors.observe(this, Observer {
            hideLoading()
            showErrorAlert(it)
        })

        btn_register.setOnClickListener { registerUser() }
    }

    private fun registerUser() {
        val email = text_email.text.toString()
        val password = text_password.text.toString()

        if (TextUtils.isEmpty(email)) {
            showMessage(getString(R.string.please_enter_email))
            return
        }

        if (TextUtils.isEmpty(password)) {
            showMessage(getString(R.string.please_enter_pass))
            return
        }

        showLoading()

        viewModel.createUser(email, password).observe(this, Observer {
            showMessage(getString(R.string.registered_successfully))
            viewModel.sendVerificationEmail()
            val intent = Intent(this@RegisterActivity, EmailVerificationActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
            intent.putExtra(EmailVerificationActivity.INTENT_PARAM_EMAIL, email)
            intent.putExtra(EmailVerificationActivity.INTENT_PARAM_PASSWORD, password)
            startActivity(intent)
            hideLoading()
            finish()
        })
    }
}
