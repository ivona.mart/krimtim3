package hr.fer.opp.zabavanet.utlis.codes

object ResultCode {
    const val SUCCESS = 0
    const val FAILURE = 1
    const val SET_BASIC_INFO = 2
    const val DISCARDED = 3
}