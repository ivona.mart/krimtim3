package hr.fer.opp.zabavanet.utlis.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import hr.fer.opp.zabavanet.R
import hr.fer.opp.zabavanet.model.Notification
import hr.fer.opp.zabavanet.utlis.extensionFunctions.toPattern
import kotlinx.android.synthetic.main.item_notification_short.view.*

class NotificationsAdapter(
    var notifications: List<Notification>,
    private val listener: OnClickListener
) :
    RecyclerView.Adapter<NotificationsAdapter.ViewHolder>() {

    override fun getItemCount(): Int {
        return notifications.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val current = notifications[position]

        current.eventType?.let {
            holder.itemView.text_event_type.visibility = View.VISIBLE
            holder.itemView.text_event_type.text = it
        }

        current.location?.let {
            holder.itemView.text_event_location.visibility = View.VISIBLE
            holder.itemView.text_event_location.text = it
        }

        if (current.after != null && current.before != null) {
            holder.itemView.text_event_time.text = holder.itemView.context.getString(
                R.string.from_time_to_time,
                current.after.toPattern(null),
                current.before.toPattern(null)
            )
        } else if (current.after != null && current.before == null) {
            holder.itemView.text_event_time.text = holder.itemView.context.getString(
                R.string.time_after,
                current.after.toPattern(null)
            )
        } else if (current.after == null && current.before != null) {
            holder.itemView.text_event_time.text = holder.itemView.context.getString(
                R.string.time_before,
                current.before.toPattern(null)
            )
        } else {
            holder.itemView.text_event_time.visibility = View.GONE
        }

        holder.itemView.setOnClickListener {
            listener.onClick(current, position)
        }

        holder.itemView.setOnLongClickListener {
            listener.onLongClick(current, position)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)
        val recyclerViewItem = inflater.inflate(R.layout.item_notification_short, parent, false)
        return ViewHolder(recyclerViewItem)
    }

    interface OnClickListener {
        fun onClick(item: Notification, position: Int)
        fun onLongClick(item: Notification, position: Int): Boolean
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}