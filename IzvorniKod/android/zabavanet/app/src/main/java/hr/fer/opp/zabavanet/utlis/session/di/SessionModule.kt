package hr.fer.opp.zabavanet.utlis.session.di

import dagger.Module
import dagger.Provides
import hr.fer.opp.zabavanet.app.App
import hr.fer.opp.zabavanet.app.di.diScope.PerApplication
import hr.fer.opp.zabavanet.utlis.session.Session

@Module
class SessionModule(app: App) {

    private var session: Session =
        Session(app)

    @PerApplication
    @Provides
    fun provideSession(): Session {
        return session
    }
}