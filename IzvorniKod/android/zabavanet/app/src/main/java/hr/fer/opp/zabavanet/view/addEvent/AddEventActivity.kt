package hr.fer.opp.zabavanet.view.addEvent

import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.OpenableColumns
import android.view.View
import android.widget.ArrayAdapter
import androidx.core.net.toUri
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.Timestamp
import com.google.firebase.storage.StorageReference
import hr.fer.opp.zabavanet.R
import hr.fer.opp.zabavanet.base.BaseActivity
import hr.fer.opp.zabavanet.model.Event
import hr.fer.opp.zabavanet.model.MediaModel
import hr.fer.opp.zabavanet.model.User
import hr.fer.opp.zabavanet.utlis.adapter.ImagesCombinedAdapter
import hr.fer.opp.zabavanet.utlis.codes.ActionCode
import hr.fer.opp.zabavanet.utlis.codes.RequestCode
import hr.fer.opp.zabavanet.utlis.codes.ResultCode
import hr.fer.opp.zabavanet.utlis.dialogManager.DialogManager
import hr.fer.opp.zabavanet.utlis.extensionFunctions.commaSeparated
import hr.fer.opp.zabavanet.utlis.extensionFunctions.createTimestamp
import hr.fer.opp.zabavanet.utlis.extensionFunctions.toPattern
import hr.fer.opp.zabavanet.utlis.glide.GlideApp
import hr.fer.opp.zabavanet.utlis.mediaUploaderService.MediaUploaderService
import hr.fer.opp.zabavanet.view.confirmationDialog.ConfirmationDialogFragment
import hr.fer.opp.zabavanet.view.datePicker.DatePickerFragment
import hr.fer.opp.zabavanet.view.eventDetails.EventDetailsActivity
import hr.fer.opp.zabavanet.view.timePicker.TimePickerFragment
import hr.fer.opp.zabavanet.viewModel.addEvent.AddEventActivityViewModel
import kotlinx.android.synthetic.main.activity_add_event.*

@Suppress("DEPRECATION")
class AddEventActivity : BaseActivity<AddEventActivityViewModel>() {

    companion object {
        var event: Event? = null
        var imageRefs: List<StorageReference>? = null
        var videoRefs: List<StorageReference>? = null

        const val USER_PARAM = "user"
        const val MAX_IMAGE_SIZE = 5 * 1024 * 1024 // in bytes
        const val MAX_IMAGE_COUNT = 10
        const val MAX_VIDEO_SIZE = 20 * 1024 * 1024 // in bytes
        private const val IMAGE_TYPE = "image/*"
        private const val VIDEO_TYPE = "video/*"
    }

    private lateinit var user: User
    private lateinit var dialogManager: DialogManager
    private lateinit var startTime: Timestamp
    private lateinit var endTime: Timestamp

    private lateinit var combinedImageModels: ArrayList<ImagesCombinedAdapter.Model>
    private lateinit var selectedImagesAdapter: ImagesCombinedAdapter
    private lateinit var videoModels: ArrayList<MediaModel>
    private lateinit var cities: List<String>
    private lateinit var citiesAdapter: ArrayAdapter<String>
    private lateinit var eventTypes: List<String>
    private var eventTypesBooleanArray = arrayListOf<Boolean>()
    private lateinit var eventTypesSelected: ArrayList<String>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_event)

        user = intent.getSerializableExtra(USER_PARAM) as User

        dialogManager = DialogManager(this.supportFragmentManager)

        viewModel.errors.observe(this, Observer {
            finishAct(intent.action, ResultCode.FAILURE)
            hideLoading()
        })

        showLoading()
        viewModel.getCities().observe(this, Observer { cities ->
            this.cities = cities
            viewModel.getEventTypes().observe(this, Observer { eventTypes ->
                this.eventTypes = eventTypes as ArrayList<String>
                for (i in eventTypes.indices) {
                    eventTypesBooleanArray.add(false)
                }
                hideLoading()

                initUI(intent.action)
            })
        })
    }

    override fun getViewModel(): AddEventActivityViewModel {
        return ViewModelProviders.of(this).get(AddEventActivityViewModel::class.java)
    }

    private fun initUI(action: String?) {

        citiesAdapter = ArrayAdapter(this, R.layout.item_spinner_item, cities)
        spinner_cities.adapter = citiesAdapter

        text_start_time.text = getString(R.string.event_start_time)
        text_end_time.text = getString(R.string.event_end_time)

        btn_add_images.text = getString(R.string.add_images, 0, MAX_IMAGE_COUNT)
        btn_add_video.text = getString(R.string.add_video, 0, 1)

        when (action) {
            ActionCode.CREATE -> {

            }

            ActionCode.EDIT -> {

                event?.let { event ->
                    setTextFields(event)

                    extractReceivedImages()

                    extractReceivedVideos()

                    spinner_cities.setSelection(cities.indexOf(event.location))
                }
            }
        }

        setListeners(action)

    }

    private fun extractReceivedVideos() {
        videoRefs?.let {
            if (it.isNotEmpty()) {
                viewModel.getUri(it.first()).observe(this, Observer {
                    videoModels =
                        arrayListOf(MediaModel(it.toString(), MediaModel.MediaType.VIDEO))
                    updateVideo(videoModels)
                })
            }
        }
    }

    private fun extractReceivedImages() {
        combinedImageModels = arrayListOf()
        combinedImageModels.apply {
            imageRefs?.forEach {
                add(
                    ImagesCombinedAdapter.Model(
                        it,
                        null,
                        ImagesCombinedAdapter.Type.REFERENCE
                    )
                )
            }
            updateImagesRecycler(this)
        }
    }

    private fun setTextFields(event: Event) {
        text_event_title.setText(event.title)
        text_event_description.setText(event.description)
        text_event_address.setText(event.address)

        setPreselectedEventTypes(event)
        setEventTimes(event)
    }

    private fun setEventTimes(event: Event) {
        startTime = event.startTime!!
        endTime = event.endTime!!
        text_start_time.text = startTime.toPattern(null)
        text_end_time.text = endTime.toPattern(null)
    }

    private fun setPreselectedEventTypes(event: Event) {
        eventTypesSelected = event.eventType as ArrayList<String>
        text_event_types.text = eventTypesSelected.commaSeparated()

        eventTypesBooleanArray.clear()
        for (eventType in eventTypes) {
            if (eventTypesSelected.contains(eventType)) {
                eventTypesBooleanArray.add(true)
            } else {
                eventTypesBooleanArray.add(false)
            }
        }
    }

    private fun setListeners(action: String?) {
        btn_edit_start_time.setOnClickListener {
            dialogManager.openDatePickerDialog(object : DatePickerFragment.DatePickerListener {
                override fun onDateSet(year: Int, month: Int, day: Int) {
                    dialogManager.openTimePickerDialog(
                        null,
                        null,
                        object : TimePickerFragment.TimePickerListener {
                            override fun onTimeSet(hourOfDay: Int, minute: Int) {
                                val result = createTimestamp(year, month, day, hourOfDay, minute)
                                if (::endTime.isInitialized && result > endTime) {
                                    showMessage(getString(R.string.end_time_must_be_greater_than_start_time))
                                } else {
                                    startTime = result
                                    text_start_time.text = startTime.toPattern(null)
                                }
                            }
                        })
                }
            })
        }

        btn_edit_end_time.setOnClickListener {
            dialogManager.openDatePickerDialog(object : DatePickerFragment.DatePickerListener {
                override fun onDateSet(year: Int, month: Int, day: Int) {
                    dialogManager.openTimePickerDialog(
                        null,
                        null,
                        object : TimePickerFragment.TimePickerListener {
                            override fun onTimeSet(hourOfDay: Int, minute: Int) {
                                val result = createTimestamp(year, month, day, hourOfDay, minute)
                                if (::startTime.isInitialized && result < startTime) {
                                    showMessage(getString(R.string.end_time_must_be_greater_than_start_time))
                                } else {
                                    endTime = result
                                    text_end_time.text = endTime.toPattern(null)
                                }
                            }
                        })
                }
            })
        }

        btn_edit_event_types.setOnClickListener {
            val copyOfBooleanArray = eventTypesBooleanArray.toBooleanArray().copyOf()
            val array = (eventTypes as ArrayList).toArray(arrayOf<String>())

            val builder = AlertDialog.Builder(this)
            builder.apply {

                title = getString(R.string.select_event_types)

                setPositiveButton(getString(R.string.ok)) { _, which ->

                    if (which == DialogInterface.BUTTON_POSITIVE) {

                        for ((i, value) in copyOfBooleanArray.withIndex()) {
                            eventTypesBooleanArray[i] = value
                        }

                        eventTypesSelected = arrayListOf()

                        for ((i, value) in eventTypesBooleanArray.withIndex()) {
                            if (value) {
                                eventTypesSelected.add(eventTypes[i])
                            }
                        }

                        text_event_types.text = eventTypesSelected.commaSeparated()
                    }
                }

                setNegativeButton(getString(R.string.cancel), null)

                setMultiChoiceItems(
                    array, eventTypesBooleanArray.toBooleanArray()
                ) { _, position, isChecked ->
                    copyOfBooleanArray[position] = isChecked
                }
            }

            builder.create().show()
        }

        btn_add_images.setOnClickListener {
            openImagePicker()
        }

        video_selected_video.setOnPreparedListener {
            it.isLooping = true
            it.setVolume(0f, 0f)
        }

        btn_add_video.setOnClickListener {
            openVideoPicker()
        }

        btn_save_event.setOnClickListener {
            val mediaModels = arrayListOf<MediaModel>()

            if (!::combinedImageModels.isInitialized || combinedImageModels.isEmpty()) {
                showMessage(getString(R.string.there_must_be_at_least_one_image))
                return@setOnClickListener
            } else {
                combinedImageModels.forEach { image ->
                    if (image.type == ImagesCombinedAdapter.Type.URI) {
                        mediaModels.add(
                            MediaModel(
                                image.uri.toString(),
                                MediaModel.MediaType.IMAGE
                            )
                        )
                    }
                }
            }


            if (::videoModels.isInitialized) {
                mediaModels.addAll(videoModels)
            }

            if (!::eventTypesSelected.isInitialized || eventTypesSelected.isEmpty()) {
                showMessage(getString(R.string.event_type_must_be_set))
                return@setOnClickListener
            }

            if (!::startTime.isInitialized || !::endTime.isInitialized) {
                showMessage(getString(R.string.event_times_are_mandatory))
                return@setOnClickListener
            }

            val title = text_event_title.text.toString()
            val description = text_event_description.text.toString()
            val address = text_event_address.text.toString()

            if (title.isBlank() || description.isBlank() || address.isBlank()) {
                showMessage(getString(R.string.all_fields_are_mandatory))
                return@setOnClickListener
            }

            event?.apply {
                this.title = title
                this.description = description
                eventType = eventTypesSelected
                location = spinner_cities.selectedItem.toString()
                this.address = address
                this.startTime = this@AddEventActivity.startTime
                this.endTime = this@AddEventActivity.endTime
            } ?: let {
                event = Event(
                    title,
                    description,
                    eventTypesSelected,
                    startTime,
                    endTime,
                    arrayListOf(),
                    arrayListOf(),
                    arrayListOf(),
                    viewModel.getUserDocRef(user),
                    arrayListOf(),
                    spinner_cities.selectedItem.toString(),
                    address,
                    Timestamp.now()
                )
            }
            event?.let { event ->
                when (action) {
                    ActionCode.CREATE -> {
                        viewModel.createEventInDatabase(event).observe(this, Observer {
                            hideLoading()
                            event.documentReference = it
                            if (combinedImageModels.isNotEmpty()) {
                                val imageModels = arrayListOf<MediaModel>()
                                combinedImageModels.forEach { image ->
                                    if (image.type == ImagesCombinedAdapter.Type.URI) {
                                        imageModels.add(
                                            MediaModel(
                                                image.uri.toString(),
                                                MediaModel.MediaType.IMAGE
                                            )
                                        )
                                    } else {
                                        finishAct(action, ResultCode.FAILURE)
                                    }
                                }
                                if (mediaModels.isNotEmpty()) {
                                    startUpload(mediaModels, it.id)
                                }
                            }
                            finishAct(action, ResultCode.SUCCESS)
                        })
                    }
                    ActionCode.EDIT -> {
                        viewModel.updateEventInDatabase(event).observe(this, Observer {
                            hideLoading()
                            if (combinedImageModels.isNotEmpty()) {
                                val imageModels = arrayListOf<MediaModel>()
                                combinedImageModels.forEach { image ->
                                    when (image.type) {
                                        ImagesCombinedAdapter.Type.URI ->
                                            imageModels.add(
                                                MediaModel(
                                                    image.uri.toString(),
                                                    MediaModel.MediaType.IMAGE
                                                )
                                            )

                                        ImagesCombinedAdapter.Type.REFERENCE ->
                                            (imageRefs as ArrayList).remove(image.ref)
                                    }
                                }

                                videoRefs?.let { videoRefs ->
                                    if (videoRefs.isNotEmpty()) {
                                        if (videoModels.isNotEmpty()) {
                                            viewModel.getUri(videoRefs.first())
                                                .observe(this, Observer { oldUri ->
                                                    if (oldUri != videoModels.first().uri.toUri()) {
                                                        viewModel.deleteMedia(videoRefs)
                                                        mediaModels.add(videoModels.first())
                                                    }
                                                })
                                            deleteOldImages(mediaModels)
                                        } else {
                                            viewModel.deleteMedia(videoRefs)
                                        }
                                    }
                                } ?: deleteOldImages(mediaModels)
                            }
                            finishAct(action, ResultCode.SUCCESS)
                        })
                    }
                }
            }
            showLoading()
        }
    }

    private fun deleteOldImages(mediaModels: ArrayList<MediaModel>) {
        viewModel.deleteMedia(imageRefs!!)
        if (mediaModels.isNotEmpty()) {
            startUpload(
                mediaModels,
                event?.documentReference!!.id
            )
        }
    }

    private fun finishAct(action: String?, result: Int) {
        hideLoading()
        when (action) {
            ActionCode.EDIT -> {
                EventDetailsActivity.eventStack.push(event)
            }

            ActionCode.CREATE -> {

            }
        }
        setResult(result)
        finish()
    }

    private fun openImagePicker() {
        val intent = Intent(Intent.ACTION_OPEN_DOCUMENT)
        intent.addCategory(Intent.CATEGORY_OPENABLE)
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true)
        intent.type = IMAGE_TYPE
        startActivityForResult(intent, RequestCode.IMAGE)
    }

    private fun openVideoPicker() {
        val intent = Intent(Intent.ACTION_OPEN_DOCUMENT)
        intent.addCategory(Intent.CATEGORY_OPENABLE)
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, false)
        intent.type = VIDEO_TYPE
        startActivityForResult(intent, RequestCode.VIDEO)
    }


    private fun startUpload(mediaModels: ArrayList<MediaModel>, subfolder: String) {
        val intent = Intent(this, MediaUploaderService::class.java)
        intent.putExtra(MediaUploaderService.INTENT_MEDIA_MODELS, mediaModels)
        intent.putExtra(MediaUploaderService.INTENT_SUBFOLDER, subfolder)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForegroundService(intent)
        } else {
            startService(intent)
        }
    }

    private fun updateVideo(videoModels: List<MediaModel>) {
        btn_add_video.text = getString(R.string.add_video, videoModels.size, 1)
        if (videoModels.isNotEmpty()) {
            showVideo(videoModels[0].uri.toUri())
        } else {
            removeVideo()
        }
    }

    private fun updateImagesRecycler(combinedImageModels: List<ImagesCombinedAdapter.Model>) {
        val recycler = recycler_selected_images

        btn_add_images.text =
            getString(R.string.add_images, combinedImageModels.size, MAX_IMAGE_COUNT)

        if (combinedImageModels.isEmpty()) {
            recycler.visibility = View.GONE
        } else {
            recycler.visibility = View.VISIBLE
        }

        if (!::selectedImagesAdapter.isInitialized) {

            selectedImagesAdapter =
                ImagesCombinedAdapter(
                    combinedImageModels,
                    GlideApp.with(this),
                    onCombinedAdapterClick
                )

            recycler.adapter = selectedImagesAdapter
            recycler.layoutManager =
                LinearLayoutManager(
                    this,
                    LinearLayoutManager.HORIZONTAL,
                    false
                )

        } else if (combinedImageModels.isNotEmpty()) {

            selectedImagesAdapter.imageList = combinedImageModels
            selectedImagesAdapter.notifyDataSetChanged()
        }
    }

    private fun showVideo(uri: Uri) {
        video_selected_video.visibility = View.VISIBLE
        video_selected_video.setOnLongClickListener(onVideoLongClick)
        video_selected_video.setVideoURI(uri)
        video_selected_video.start()
    }

    private fun removeVideo() {
        video_selected_video.stopPlayback()
        video_selected_video.visibility = View.GONE
    }

    private fun getFileSize(uri: Uri): Long? {
        var size: Long? = null

        contentResolver.query(uri, null, null, null, null)?.use {
            it.moveToFirst()
            val sizeColumn = it.getColumnIndex(OpenableColumns.SIZE)
            size = it.getLong(sizeColumn)
            it.close()
        }

        return size
    }

    private val onCombinedAdapterClick = object : ImagesCombinedAdapter.OnClickListener {
        override fun onClick(item: ImagesCombinedAdapter.Model, position: Int) {
            // todo
        }

        override fun onLongClick(item: ImagesCombinedAdapter.Model, position: Int): Boolean {
            dialogManager.openConfirmationDialog(getString(R.string.remove_image),
                getString(R.string.yes),
                getString(R.string.no),
                object : ConfirmationDialogFragment.DialogListener {
                    override fun onNegative() {
                    }

                    override fun onPositive() {
                        combinedImageModels.remove(item)
                        updateImagesRecycler(combinedImageModels)
                    }
                })
            return true
        }
    }

    private val onVideoLongClick = View.OnLongClickListener {
        dialogManager.openConfirmationDialog(getString(R.string.remove_video),
            getString(R.string.yes),
            getString(R.string.no),
            object : ConfirmationDialogFragment.DialogListener {
                override fun onNegative() {
                }

                override fun onPositive() {
                    videoModels.clear()
                    updateVideo(videoModels)
                }
            })

        true
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        data ?: return

        when (requestCode) {

            RequestCode.IMAGE -> {

                if (!::combinedImageModels.isInitialized) {
                    combinedImageModels = arrayListOf()
                }

                // when only one image is selected
                data.data?.let { uri ->

                    if (1 + combinedImageModels.size > MAX_IMAGE_COUNT) {
                        showMessage(getString(R.string.too_many_images, MAX_IMAGE_COUNT))
                        return
                    }

                    getFileSize(uri)?.let {

                        if (it > MAX_IMAGE_SIZE) {
                            showMessage(getString(R.string.image_too_big, MAX_IMAGE_SIZE))
                            return
                        } else {
                            combinedImageModels.add(
                                ImagesCombinedAdapter.Model(
                                    null,
                                    uri,
                                    ImagesCombinedAdapter.Type.URI
                                )
                            )
                        }
                    }
                }

                // when multiple images are selected
                data.clipData?.let { clipData ->

                    val maxImages =
                        if (clipData.itemCount + combinedImageModels.size <= MAX_IMAGE_COUNT) {
                            clipData.itemCount
                        } else {
                            showMessage(getString(R.string.too_many_images, MAX_IMAGE_COUNT))
                            MAX_IMAGE_COUNT - combinedImageModels.size
                        }

                    for (i in 0 until maxImages) {

                        val uri = data.clipData!!.getItemAt(i).uri
                        val size = getFileSize(uri) ?: continue

                        if (size > MAX_IMAGE_SIZE) {
                            showMessage(getString(R.string.image_too_big, MAX_IMAGE_SIZE))
                            continue
                        } else {
                            combinedImageModels.add(
                                ImagesCombinedAdapter.Model(
                                    null,
                                    uri,
                                    ImagesCombinedAdapter.Type.URI
                                )
                            )

                        }
                    }
                }

                updateImagesRecycler(combinedImageModels)
            }

            RequestCode.VIDEO -> {

                data.data?.let { uri ->
                    getFileSize(uri)?.let {
                        if (it > MAX_VIDEO_SIZE) {
                            showMessage(getString(R.string.video_too_big, MAX_VIDEO_SIZE))
                            return
                        } else {
                            videoModels =
                                arrayListOf(MediaModel(uri.toString(), MediaModel.MediaType.VIDEO))
                            updateVideo(videoModels)
                        }
                    }
                }
            }
        }
    }

    override fun onBackPressed() {
        finishAct(intent.action, ResultCode.DISCARDED)
    }
}