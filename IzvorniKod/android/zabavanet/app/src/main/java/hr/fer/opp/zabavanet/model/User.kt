package hr.fer.opp.zabavanet.model

import com.google.firebase.Timestamp
import hr.fer.opp.zabavanet.utlis.extensionFunctions.determineZodiacSign
import java.io.Serializable
import java.util.*

data class User(
    var firstName: String?,
    var lastName: String?,
    var email: String?,
    var username: String?,
    var role: String?
) : Serializable {

    var dateOfBirthLong: Long? = null
        set(value) {
            field = value
            if (value != null && Timestamp(value, 0) != dateOfBirth) {
                dateOfBirth = Timestamp(value, 0)
            }
        }

    @Transient
    var dateOfBirth: Timestamp? = null
        set(value) {
            field = value
            if (value != null) {
                dateOfBirthLong = value.seconds
                zodiacSign = determineZodiacSign(value)
            }
        }
        get() {
            if (field == null && dateOfBirthLong != null) {
                field = Timestamp(dateOfBirthLong!!, 0)
            }
            return field
        }

    var zodiacSign: String? = null
        private set

    var organizationId: String? = null

    @Transient
    var membershipDuration: Timestamp? = null
        set(value) {
            field = value
            if (value != null) {
                membershipDurationLong = value.seconds
            }
        }
        get() {
            if (field == null && membershipDurationLong != null) {
                field = Timestamp(membershipDurationLong!!, 0)
            }
            return field
        }

    var membershipDurationLong: Long? = null
        set(value) {
            field = value
            if (value != null && Timestamp(value, 0) != membershipDuration) {
                membershipDuration = Timestamp(value, 0)
            }
        }

    companion object {
        const val FIRST_NAME = "firstName"
        const val LAST_NAME = "lastName"
        const val EMAIL = "email"
        const val USERNAME = "username"
        const val ROLE = "role"
        const val DATE_OF_BIRTH = "dateOfBirth"
        const val ZODIAC_SIGN = "zodiacSign"
        const val MEMEBERSHIP_DURATION = "membershipDuration"
    }

    constructor() : this(null, null, null, null, null)

    fun asHashMap(): HashMap<String, Any?> {
        return hashMapOf(
            FIRST_NAME to firstName,
            LAST_NAME to lastName,
            EMAIL to email,
            USERNAME to username,
            ROLE to role,
            DATE_OF_BIRTH to dateOfBirth,
            ZODIAC_SIGN to zodiacSign,
            MEMEBERSHIP_DURATION to membershipDuration
        )
    }
    fun roleMap(): Map<String, Any?> {
        return mapOf(
            ROLE to role,
            MEMEBERSHIP_DURATION to membershipDuration
        )
    }
}

