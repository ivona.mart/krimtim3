package hr.fer.opp.zabavanet.viewModel.profile

import androidx.lifecycle.LiveData
import com.google.firebase.storage.StorageReference
import hr.fer.opp.zabavanet.app.di.ApplicationComponent
import hr.fer.opp.zabavanet.base.BaseViewModel
import hr.fer.opp.zabavanet.model.Notification
import hr.fer.opp.zabavanet.model.User
import hr.fer.opp.zabavanet.utlis.networking.firebaseAuth.FirebaseAuthService
import hr.fer.opp.zabavanet.utlis.networking.firebaseStorage.FirebaseStorageService
import hr.fer.opp.zabavanet.utlis.networking.firestore.FirestoreService
import hr.fer.opp.zabavanet.utlis.session.Session
import hr.fer.opp.zabavanet.view.eventDetails.EventDetailsActivity
import javax.inject.Inject

class ProfileFragmentViewModel : BaseViewModel () {

    @Inject
    lateinit var storage: FirebaseStorageService

    @Inject
    lateinit var firebaseAuthService: FirebaseAuthService

    @Inject
    lateinit var firestoreService: FirestoreService

    @Inject
    lateinit var session: Session

    override fun injectToAppComponent(applicationComponent: ApplicationComponent) {
        applicationComponent.plusMyProfile().inject(this)
    }

    fun signOut() {
        while (!EventDetailsActivity.eventStack.empty()) {
            EventDetailsActivity.eventStack.pop()
        }
        session.user = null
        firebaseAuthService.signOut()
    }

    fun getImageReferences(email: String): LiveData<List<StorageReference>> {
        return storage.getImageReferences(email, errors)
    }

    fun getNotifications(user: User): LiveData<List<Notification>> {
        return firestoreService.getNotifications(user.email!!, errors)
    }

    fun deleteNotification(notification: Notification): LiveData<Unit> {
        return firestoreService.deleteNotification(notification.documentReference, errors)
    }

}