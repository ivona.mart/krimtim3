package hr.fer.opp.zabavanet.viewModel.login

import androidx.lifecycle.LiveData
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseUser
import hr.fer.opp.zabavanet.app.di.ApplicationComponent
import hr.fer.opp.zabavanet.base.BaseViewModel
import hr.fer.opp.zabavanet.model.User
import hr.fer.opp.zabavanet.utlis.networking.firebaseAuth.FirebaseAuthService
import hr.fer.opp.zabavanet.utlis.networking.firestore.FirestoreService
import hr.fer.opp.zabavanet.utlis.session.Session
import javax.inject.Inject

class LoginActivityViewModel : BaseViewModel() {

    @Inject
    lateinit var firebaseAuthService: FirebaseAuthService

    @Inject
    lateinit var firestoreService: FirestoreService

    @Inject
    lateinit var session: Session

    override fun injectToAppComponent(applicationComponent: ApplicationComponent) {
        applicationComponent.plusLogin().inject(this)
    }

    fun signIn(email: String, password: String): LiveData<AuthResult> {
        return firebaseAuthService.signInWithEmailAndPassword(email, password, errors)
    }

    fun isEmailVerified(): Boolean {
        return firebaseAuthService.getCurrentUser()!!.isEmailVerified
    }

    fun logInWithCredentials(
        account: GoogleSignInAccount,
        webClientId: String
    ): LiveData<Unit> {
        return firebaseAuthService.signInWithCredential(account, webClientId, errors)
    }

    fun createUserInDatabase(user: User): LiveData<Unit> {
        return firestoreService.createUserInDatabase(user, errors)
    }

    fun getUserFromDatabase(email: String?): LiveData<User> {
        return firestoreService.getUserFromDatabase(email, errors)
    }

    fun getCurrentUser(): FirebaseUser? {
        return firebaseAuthService.getCurrentUser()
    }

    fun resetPassword(email: String): LiveData<Unit> {
        return firebaseAuthService.resetPassword(email, errors)
    }

    fun saveUserToPreferences(user: User) {
        session.user = user
    }
}