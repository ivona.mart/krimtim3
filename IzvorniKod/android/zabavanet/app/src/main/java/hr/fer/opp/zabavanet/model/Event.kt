package hr.fer.opp.zabavanet.model

import com.google.firebase.Timestamp
import com.google.firebase.firestore.DocumentReference

data class Event(
    var title: String?,
    var description: String?,
    var eventType: List<String>?,
    var startTime: Timestamp?,
    var endTime: Timestamp?,
    var going: List<DocumentReference>?,
    var maybeGoing: List<DocumentReference>?,
    var notGoing: List<DocumentReference>?,
    val organizer: DocumentReference?,
    var reviews: List<Review>?,
    var location: String?,
    var address: String?,
    val createdAt: Timestamp?
) {

    companion object {
        const val TITLE = "title"
        const val DESCRIPTION = "description"
        const val START_TIME = "startTime"
        const val END_TIME = "endTime"
        const val ORGANIZER = "organizer"
        const val EVENT_TYPE = "eventType"
        const val LOCATION = "location"
        const val ADDRESS = "address"
        const val CREATED_AT = "createdAt"

        const val GOING = "going"
        const val MAYBE_GOING = "maybeGoing"
        const val NOT_GOING = "notGoing"
    }

    lateinit var documentReference: DocumentReference

    constructor() : this(
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null
    )

    fun asHashMap(): HashMap<String, Any?> {
        return hashMapOf(
            TITLE to title,
            DESCRIPTION to description,
            START_TIME to startTime,
            END_TIME to endTime,
            ORGANIZER to organizer,
            EVENT_TYPE to eventType,
            GOING to going,
            MAYBE_GOING to maybeGoing,
            NOT_GOING to notGoing,
            LOCATION to location,
            ADDRESS to address,
            CREATED_AT to createdAt
        )
    }

    fun attendanceAsMap(): Map<String, List<DocumentReference>?> {
        return mapOf(
            GOING to going,
            MAYBE_GOING to maybeGoing,
            NOT_GOING to notGoing
        )
    }
}