package hr.fer.opp.zabavanet.utlis.adapter

import android.widget.ImageView
import hr.fer.opp.zabavanet.R
import hr.fer.opp.zabavanet.model.Review
import kotlinx.android.synthetic.main.item_review_short.view.*

class ReviewAdapter(
    private var reviews: ArrayList<Review>,
    private val listener: OnClickListener<Review>
) :
    GenericAdapter<Review>(
        R.layout.item_review_short, reviews, listener
    ) {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val current = reviews[position]

        holder.itemView.let {
            val stars =
                arrayListOf(it.img_star1, it.img_star2, it.img_star3, it.img_star4, it.img_star5)
            it.text_review_text.text = current.text
            setStarRating(stars, current.rating!!.minus(1))
        }

        holder.itemView.setOnClickListener {
            listener.onClick(current)
        }
    }

    private fun setStarRating(stars: ArrayList<ImageView>, numberOfStars: Int) {
        val totalNumberOfStars = stars.size
        check(totalNumberOfStars >= numberOfStars) { "Rating is higher than there are stars" }

        for (i in 0..numberOfStars) {
            stars[i].setImageResource(R.drawable.star_gold)
        }

        for (i in numberOfStars + 1 until totalNumberOfStars) {
            stars[i].setImageResource(R.drawable.star_grey)
        }
    }
}