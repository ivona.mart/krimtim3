package hr.fer.opp.zabavanet.utlis.extensionFunctions

import androidx.annotation.Nullable
import com.google.firebase.Timestamp
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.Query
import hr.fer.opp.zabavanet.model.Event
import java.text.SimpleDateFormat
import java.util.*

fun Timestamp?.toPattern(@Nullable pattern: String?): String {

    if (this == null) return ""

    val pat = pattern ?: "HH:mm dd.MM.yyyy"

    val date = this.toDate()
    val format = SimpleDateFormat(pat, Locale.GERMANY)
    return format.format(date)
}

/**
 * @param month January = 0, December = 11
 */
fun createTimestamp(year: Int, month: Int, day: Int, hour: Int, minute: Int): Timestamp {
    val cal = Calendar.getInstance()
    cal.set(Calendar.YEAR, year)
    cal.set(Calendar.MONTH, month)
    cal.set(Calendar.DAY_OF_MONTH, day)
    cal.set(Calendar.HOUR_OF_DAY, hour)
    cal.set(Calendar.MINUTE, minute)

    val date = cal.time
    return Timestamp(date)
}

fun List<*>.commaSeparated(): String {
    val sb = StringBuilder()
    var prefix = ""
    this.forEach {
        sb.append(prefix)
        prefix = ", "
        sb.append(it.toString())
    }
    return sb.toString()
}

fun Query.queryLocation(location: String?): Query {
    return if (location != null) {
        this.whereEqualTo(Event.LOCATION, location)
    } else {
        this
    }
}

fun Query.queryEventType(eventType: String?): Query {
    return if (eventType != null) {
        this.whereArrayContains(Event.EVENT_TYPE, eventType)
    } else {
        this
    }
}

fun Query.queryTimeAfter(timeAfter: Timestamp?): Query {
    return if (timeAfter != null) {
        this.whereGreaterThanOrEqualTo(Event.START_TIME, timeAfter)
    } else {
        this
    }
}

fun Query.queryTimeBefore(timeBefore: Timestamp?): Query {
    return if (timeBefore != null) {
        this.whereLessThanOrEqualTo(Event.START_TIME, timeBefore)
    } else {
        this
    }
}

fun Query.queryOrganizersEvents(organizerRef: DocumentReference?): Query {
    return if (organizerRef != null) {
        this.whereEqualTo(Event.ORGANIZER, organizerRef)
    } else {
        this
    }
}

fun Query.queryUserGoingEvents(userRef: DocumentReference?): Query {
    return if (userRef != null) {
        this.whereArrayContains(Event.GOING, userRef)
    } else {
        this
    }
}

fun timeDifferenceDays(from: Timestamp, to: Timestamp): Double {
    val diffSeconds = (to.seconds - from.seconds).toDouble()
    return diffSeconds / (24 * 60 * 60)
}

fun Timestamp.subtractYears(numberOfYears: Int): Timestamp {
    return this.add(Calendar.YEAR, (-1) * numberOfYears)
}

fun Timestamp.addMonths(numberOfMonths: Int): Timestamp {
    return this.add(Calendar.MONTH, numberOfMonths)
}

fun Timestamp.add(field: Int, amount: Int): Timestamp {
    val cal = Calendar.getInstance()
    cal.time = this.toDate()
    cal.add(field, amount)
    return Timestamp(cal.time)
}

fun Timestamp.subtractDays(numberOfDays: Int): Timestamp{
    return this.add(Calendar.DAY_OF_YEAR, (-1)*numberOfDays)
}

fun determineZodiacSign(dateOfBirth: Timestamp?): String? {
    val calendar = Calendar.getInstance()
    if (dateOfBirth == null)
        return null
    calendar.time = dateOfBirth.toDate()
    val month = calendar.get(Calendar.MONTH)
    val day = calendar.get(Calendar.DAY_OF_MONTH)
    return when (month) {
        0 -> if (day < 20) "Capricorn" else "Aquarius"
        1 -> if (day < 19) "Aquarius" else "Pisces"
        2 -> if (day < 21) "Pisces" else "Aries"
        3 -> if (day < 20) "Aries" else "Taurus"
        4 -> if (day < 21) "Taurus" else "Gemini"
        5 -> if (day < 21) "Gemini" else "Cancer"
        6 -> if (day < 23) "Cancer" else "Leo"
        7 -> if (day < 23) "Leo" else "Virgo"
        8 -> if (day < 23) "Virgo" else "Libra"
        9 -> if (day < 23) "Libra" else "Scorpio"
        10 -> if (day < 22) "Scorpio" else "Sagittarius"
        11 -> if (day < 22) "Sagittarius" else "Capricorn"
        else -> null
    }
}
