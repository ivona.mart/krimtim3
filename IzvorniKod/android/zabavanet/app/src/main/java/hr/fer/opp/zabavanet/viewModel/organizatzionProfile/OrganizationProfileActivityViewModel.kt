package hr.fer.opp.zabavanet.viewModel.organizatzionProfile

import androidx.lifecycle.LiveData
import com.google.firebase.Timestamp
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.storage.StorageReference
import hr.fer.opp.zabavanet.app.di.ApplicationComponent
import hr.fer.opp.zabavanet.base.BaseViewModel
import hr.fer.opp.zabavanet.model.Event
import hr.fer.opp.zabavanet.model.Organization
import hr.fer.opp.zabavanet.model.User
import hr.fer.opp.zabavanet.utlis.networking.firebaseStorage.FirebaseStorageService
import hr.fer.opp.zabavanet.utlis.networking.firestore.FirestoreService
import hr.fer.opp.zabavanet.utlis.session.Session
import javax.inject.Inject

class OrganizationProfileActivityViewModel : BaseViewModel() {

    @Inject
    lateinit var storage: FirebaseStorageService

    @Inject
    lateinit var firestoreService: FirestoreService

    @Inject
    lateinit var session: Session

    override fun injectToAppComponent(applicationComponent: ApplicationComponent) {
        applicationComponent.plusOrganizationProfile().inject(this)
    }

    fun getImageReferences(user: User): LiveData<List<StorageReference>> {
        return storage.getImageReferences(user.organizationId!!, errors)
    }

    fun getOrganization(user: User): LiveData<List<Organization>> {
        return firestoreService.getOrganization(user, errors)
    }

    fun getOrganizedEvents(user: User, since: Timestamp): LiveData<List<Event>> {
        return firestoreService.getOrganizersEvents(user.email!!, since, errors)
    }

    fun getUserRef(user: User): DocumentReference {
        return firestoreService.getDocumentReference(
            FirestoreService.USERS_COLLECTION,
            user.email!!
        )
    }

    fun getCurrentUserRef(): DocumentReference {
        return firestoreService.getDocumentReference(
            FirestoreService.USERS_COLLECTION,
            session.user?.email!!
        )
    }
}