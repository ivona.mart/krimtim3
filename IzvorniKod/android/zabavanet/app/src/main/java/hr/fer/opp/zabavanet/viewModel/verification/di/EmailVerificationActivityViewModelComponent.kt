package hr.fer.opp.zabavanet.viewModel.login.di

import dagger.Subcomponent
import hr.fer.opp.zabavanet.viewModel.verification.EmailVerificationActivityViewModel

@Subcomponent
interface EmailVerificationActivityViewModelComponent {

    fun inject(emailVerificationActivityViewModel: EmailVerificationActivityViewModel)
}