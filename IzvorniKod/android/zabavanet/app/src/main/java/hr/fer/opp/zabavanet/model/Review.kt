package hr.fer.opp.zabavanet.model

import com.google.firebase.Timestamp
import com.google.firebase.firestore.DocumentReference

data class Review(
    val createdAt: Timestamp?,
    val text: String?,
    val userRef: DocumentReference?,
    val rating: Int?
) {

    companion object {
        const val CREATED_AT = "createdAt"
        const val TEXT = "text"
        const val USER = "userRef"
        const val RATING = "rating"
    }

    constructor() : this(null, null, null, null)

    lateinit var user: User

    fun asMap(): Map<String, Any?> {
        return mapOf(
            CREATED_AT to createdAt,
            TEXT to text,
            USER to userRef,
            RATING to rating
        )
    }
}