package hr.fer.opp.zabavanet.app.di

import dagger.Component
import hr.fer.opp.zabavanet.app.di.diScope.PerApplication
import hr.fer.opp.zabavanet.utlis.networking.firebaseAuth.di.FirebaseAuthServiceModule
import hr.fer.opp.zabavanet.utlis.networking.firebaseStorage.di.FirebaseStorageServiceModule
import hr.fer.opp.zabavanet.utlis.networking.firestore.di.FirestoreServiceModule
import hr.fer.opp.zabavanet.utlis.networking.okhttp.di.OkHttpModule
import hr.fer.opp.zabavanet.utlis.performanceChecker.PerformanceChecker
import hr.fer.opp.zabavanet.utlis.session.di.SessionModule
import hr.fer.opp.zabavanet.viewModel.addEvent.di.AddEventActivityViewModelComponent
import hr.fer.opp.zabavanet.viewModel.allEvents.di.AllEventsFragmentViewModelComponent
import hr.fer.opp.zabavanet.viewModel.basicInfo.di.BasicInfoViewModelComponent
import hr.fer.opp.zabavanet.viewModel.editOrganization.di.EditOrganizationViewModelComponent
import hr.fer.opp.zabavanet.viewModel.eventDetails.di.EventDetailsActivityViewModelComponent
import hr.fer.opp.zabavanet.viewModel.home.di.HomeActivityViewModelComponent
import hr.fer.opp.zabavanet.viewModel.login.di.EmailVerificationActivityViewModelComponent
import hr.fer.opp.zabavanet.viewModel.login.di.LoginActivityViewModelComponent
import hr.fer.opp.zabavanet.viewModel.myEvents.di.MyEventsFragmentViewModelComponent
import hr.fer.opp.zabavanet.viewModel.organizatzionProfile.di.OrganizationProfileViewModelComponent
import hr.fer.opp.zabavanet.viewModel.payment.di.PaymentViewModelComponent
import hr.fer.opp.zabavanet.viewModel.profile.di.ProfileFragmentViewModelComponent
import hr.fer.opp.zabavanet.viewModel.register.di.RegisterActivityViewModelComponent
import hr.fer.opp.zabavanet.viewModel.splash.di.SplashActivityViewModelComponent
import okhttp3.OkHttpClient

@PerApplication
@Component(modules = [ApplicationModule::class, SessionModule::class, FirebaseAuthServiceModule::class, FirestoreServiceModule::class, FirebaseStorageServiceModule::class, OkHttpModule::class])
interface ApplicationComponent {

    fun getPerformanceChecker(): PerformanceChecker

    fun getOkHttpClient(): OkHttpClient

    fun plusHome(): HomeActivityViewModelComponent

    fun plusLogin(): LoginActivityViewModelComponent

    fun plusRegister(): RegisterActivityViewModelComponent

    fun plusBasicInfo(): BasicInfoViewModelComponent

    fun plusEditOrganization(): EditOrganizationViewModelComponent

    fun plusOrganizationProfile(): OrganizationProfileViewModelComponent

    fun plusVerification(): EmailVerificationActivityViewModelComponent

    fun plusAllEvents(): AllEventsFragmentViewModelComponent

    fun plusMyEvents(): MyEventsFragmentViewModelComponent

    fun plusMyProfile(): ProfileFragmentViewModelComponent

    fun plusAddEvent(): AddEventActivityViewModelComponent

    fun plusSplash(): SplashActivityViewModelComponent

    fun plusPayment(): PaymentViewModelComponent

    fun plusEventDetails(): EventDetailsActivityViewModelComponent
}