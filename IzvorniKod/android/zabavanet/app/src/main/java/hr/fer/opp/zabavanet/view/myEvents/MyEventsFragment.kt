package hr.fer.opp.zabavanet.view.myEvents

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import hr.fer.opp.zabavanet.R
import hr.fer.opp.zabavanet.base.BaseFragment
import hr.fer.opp.zabavanet.model.Event
import hr.fer.opp.zabavanet.model.User
import hr.fer.opp.zabavanet.utlis.adapter.EventsAdapter
import hr.fer.opp.zabavanet.utlis.codes.ActionCode
import hr.fer.opp.zabavanet.utlis.codes.RequestCode
import hr.fer.opp.zabavanet.utlis.codes.ResultCode
import hr.fer.opp.zabavanet.utlis.dialogManager.DialogManager
import hr.fer.opp.zabavanet.view.addEvent.AddEventActivity
import hr.fer.opp.zabavanet.view.confirmationDialog.ConfirmationDialogFragment
import hr.fer.opp.zabavanet.view.eventDetails.EventDetailsActivity
import hr.fer.opp.zabavanet.viewModel.myEvents.MyEventsFragmentViewModel
import kotlinx.android.synthetic.main.fragment_my_events.*

class MyEventsFragment : BaseFragment<MyEventsFragmentViewModel>() {

    private val USER_PARAM = "user"
    private var user: User? = null
    private lateinit var recyclerViewAdapter: EventsAdapter
    private lateinit var events: List<EventsAdapter.EventType>
    private lateinit var dialogManager: DialogManager

    companion object {
        @JvmStatic
        fun newInstance(userParam: User) =
            MyEventsFragment().apply {
                arguments = Bundle().apply {
                    putSerializable(USER_PARAM, userParam)
                }
            }
    }

    override fun getViewModel(): MyEventsFragmentViewModel {
        return ViewModelProviders.of(this).get(MyEventsFragmentViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            user = it.getSerializable(USER_PARAM) as User
        }

        dialogManager = DialogManager(fragmentManager!!)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_my_events, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        user?.let {
            initUI(it)
        }
    }

    private fun initUI(user: User) {

        events = arrayListOf()

        when (user.role) {
            "organizer" -> {
                getUserOrganizingEvents(user)
                fab_add_event.visibility = View.VISIBLE
                fab_add_event.setOnClickListener {
                    startAddEventActivity(user)
                }
            }

            "visitor" -> {
                fab_add_event.visibility = View.GONE
                getUserGoingEvents(user)
            }
        }

    }

    private fun getUserGoingEvents(user: User) {
        viewModel.getUserGoingEvents(user).observe(this, Observer {

            for (event in it) {
                (events as ArrayList).add(
                    EventsAdapter.EventType(
                        event,
                        EventsAdapter.ViewType.GOING
                    )
                )
            }
            events = events.distinctBy { eventType ->
                eventType.event
            }
            initEventsRecycler(events)
        })
    }

    private fun getUserOrganizingEvents(user: User) {
        viewModel.getOrganizersEvents(user).observe(this, Observer {

            for (event in it) {
                (events as ArrayList).add(
                    EventsAdapter.EventType(
                        event,
                        EventsAdapter.ViewType.ORGANIZER
                    )
                )
            }
            getUserGoingEvents(user)
        })
    }

    private fun initEventsRecycler(events: List<EventsAdapter.EventType>) {

        if (!::recyclerViewAdapter.isInitialized) {
            recyclerViewAdapter = EventsAdapter(
                events as ArrayList<EventsAdapter.EventType>,
                object : EventsAdapter.OnClickListener {
                    override fun onClick(item: EventsAdapter.EventType) {

                        if (item.viewType == EventsAdapter.ViewType.ORGANIZER) {
                            startEventDetailsActivityAsOrganizer(item.event)
                        } else {
                            startEventDetailsActivityAsVisitor(item.event)
                        }
                    }

                    override fun onLongClick(item: EventsAdapter.EventType): Boolean {
                        if (item.event.organizer == viewModel.getUserReference(user!!)) {
                            dialogManager.openConfirmationDialog(
                                getString(R.string.are_you_sure_you_want_to_delete_event),
                                getString(R.string.yes),
                                getString(R.string.no),
                                object : ConfirmationDialogFragment.DialogListener {
                                    override fun onPositive() {
                                        showLoading()
                                        viewModel.deleteEvent(item.event)
                                            .observe(this@MyEventsFragment, Observer {
                                                hideLoading()
                                                showMessage(getString(R.string.event_deleted))
                                                events.remove(item)
                                                initEventsRecycler(events)
                                            })
                                    }

                                    override fun onNegative() {
                                    }
                                })
                            return true
                        } else {
                            return false
                        }
                    }
                }
            )

            recycler_my_events.adapter = recyclerViewAdapter
            recycler_my_events.layoutManager = LinearLayoutManager(context)
        } else {
            recyclerViewAdapter.eventList = events
            recyclerViewAdapter.notifyDataSetChanged()
        }
    }

    private fun startAddEventActivity(user: User) {
        val intent = Intent(context, AddEventActivity::class.java)
        intent.putExtra(AddEventActivity.USER_PARAM, user)
        intent.action = ActionCode.CREATE
        startActivityForResult(intent, RequestCode.CREATE)
    }

    private fun startEventDetailsActivityAsOrganizer(event: Event) {
        EventDetailsActivity.eventStack.push(event)
        val intent = Intent(context, EventDetailsActivity::class.java)
        intent.action = ActionCode.OPEN_AS_ORGANIZER
        startActivity(intent)
    }

    private fun startEventDetailsActivityAsVisitor(event: Event) {
        EventDetailsActivity.eventStack.push(event)
        val intent = Intent(context, EventDetailsActivity::class.java)
        intent.action = ActionCode.OPEN_AS_VISITOR
        startActivity(intent)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == RequestCode.CREATE) {
            if (resultCode == ResultCode.SUCCESS) {
                showMessage(getString(R.string.event_created_successfully))
            } else if (resultCode == ResultCode.FAILURE) {
                showMessage(getString(R.string.failed_to_create_event))
            }
        }
    }

    override fun onResume() {
        super.onResume()
        initUI(user!!)
    }
}
