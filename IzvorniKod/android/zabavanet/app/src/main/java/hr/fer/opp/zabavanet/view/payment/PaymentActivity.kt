package hr.fer.opp.zabavanet.view.payment

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.firebase.Timestamp
import hr.fer.opp.zabavanet.R
import hr.fer.opp.zabavanet.base.BaseActivity
import hr.fer.opp.zabavanet.model.User
import hr.fer.opp.zabavanet.utlis.codes.ActionCode
import hr.fer.opp.zabavanet.utlis.codes.RequestCode
import hr.fer.opp.zabavanet.utlis.codes.ResultCode
import hr.fer.opp.zabavanet.utlis.extensionFunctions.addMonths
import hr.fer.opp.zabavanet.view.editOrganization.EditOrganizationActivity
import hr.fer.opp.zabavanet.view.profile.ProfileFragment
import hr.fer.opp.zabavanet.viewModel.payment.PaymentActivityViewModel
import kotlinx.android.synthetic.main.activity_payment.*

class PaymentActivity : BaseActivity<PaymentActivityViewModel>() {

    companion object {
        const val INTENT_USER = "user"
        const val ROLE_ORGANIZER = "organizer"
    }

    private var monthlyMembership = 0.0
    private lateinit var user: User

    override fun getViewModel(): PaymentActivityViewModel {
        return ViewModelProviders.of(this).get(PaymentActivityViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment)

        user = intent.getSerializableExtra(INTENT_USER) as User

        check(intent.action != null) { "Intent action must be set!" }

        showLoading()
        viewModel.getMonthlyMembership().observe(this, Observer {
            monthlyMembership = it.first().toDouble()
            hideLoading()
            initUI(intent.action!!)
        })

    }

    fun initUI(action: String) {

        monthly_membership.text = getString(R.string.membership_price, monthlyMembership)
        when (action) {
            ActionCode.PAYPAL -> {
                layout_credit_card_payment.visibility = View.GONE
            }
            ActionCode.CREDIT_CARD -> {
                layout_paypal_payment.visibility = View.GONE
            }
        }

        btn_pay.setOnClickListener {
            when (action) {
                ActionCode.PAYPAL -> {

                    if (text_paypal_email.text.isBlank()) {
                        showMessage("Email can't be empty!")
                        return@setOnClickListener
                    }

                    if (text_paypal_password.text.isBlank()) {
                        showMessage("PayPal password can't be empty!")
                        return@setOnClickListener
                    }
                }
                ActionCode.CREDIT_CARD -> {

                    if (text_card_number.text.isBlank()) {
                        showMessage("Card number can't be empty!")
                        return@setOnClickListener
                    }

                    if (text_valid_thru.text.isBlank()) {
                        showMessage("Valid thru can't be empty!")
                        return@setOnClickListener
                    }

                    if (text_security_code.text.isBlank()) {
                        showMessage("Security code can't be empty!")
                        return@setOnClickListener
                    }
                }
            }

            showLoading()
            viewModel.executePayment().observe(this, Observer {
                hideLoading()
                if (it) {
                    user.organizationId?.let {
                        becomeOrganizer(true)
                    } ?: becomeOrganizer(false)
                } else {
                    showMessage(getString(R.string.payment_unsuccessful))
                }
            })
        }
    }

    private fun becomeOrganizer(hasOrganization: Boolean) {

        val intent = Intent(this, EditOrganizationActivity::class.java)
        intent.putExtra(EditOrganizationActivity.INTENT_USER, user)

        if (hasOrganization) {
            viewModel.getOrganization(user).observe(this, Observer {
                val organization = it.first()
                intent.putExtra(EditOrganizationActivity.INTENT_ORGANIZATION, organization)
                intent.action = ActionCode.EDIT
                startActivityForResult(intent, RequestCode.EDIT)
            })
        } else {
            intent.action = ActionCode.CREATE
            startActivityForResult(intent, RequestCode.CREATE)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            RequestCode.CREATE -> {
                if (resultCode == ResultCode.SUCCESS) {
                    user = data?.getSerializableExtra(INTENT_USER) as User
                    user.role = ROLE_ORGANIZER
                    user.membershipDuration = Timestamp.now().addMonths(1)
                    viewModel.updateUserRole(user).observe(this, Observer {
                        viewModel.saveUserToPreferences(user)
                        finishAct(ResultCode.SUCCESS, user)
                        showMessage(getString(R.string.youre_an_organizer))
                    })
                }
            }

            RequestCode.EDIT -> {
                if (resultCode == ResultCode.SUCCESS) {
                    user = data?.getSerializableExtra(INTENT_USER) as User
                    user.role = ROLE_ORGANIZER
                    user.membershipDuration = Timestamp.now().addMonths(1)
                    viewModel.updateUserRole(user).observe(this, Observer {
                        viewModel.saveUserToPreferences(user)
                        finishAct(ResultCode.SUCCESS, user)
                        showMessage(getString(R.string.welcome_back))
                    })
                }
            }

            else -> {
                finishAct(ResultCode.FAILURE, user)
            }
        }
    }

    private fun finishAct(result: Int, user: User) {
        val resultIntent = Intent()
        resultIntent.putExtra(ProfileFragment.INTENT_USER, user)
        setResult(result, resultIntent)
        finish()
    }
}