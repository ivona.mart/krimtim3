package hr.fer.opp.zabavanet.model

import com.google.firebase.Timestamp
import com.google.firebase.firestore.DocumentReference

data class Notification(
    private val title: String?,
    private val content: String?,
    val eventType: String?,
    val location: String?,
    val after: Timestamp?,
    val before: Timestamp?
) {

    constructor() : this(null, null, null, null, null, null)

    lateinit var documentReference: DocumentReference

    companion object {
        const val TITLE = "title"
        const val CONTENT = "content"
        const val EVENT_TYPE = "eventType"
        const val LOCATION = "location"
        const val AFTER = "after"
        const val BEFORE = "before"
    }

    fun asMap(): Map<String, Any?> {
        return mapOf<String, Any?>(
            TITLE to title,
            CONTENT to content,
            EVENT_TYPE to eventType,
            LOCATION to location,
            AFTER to after,
            BEFORE to before
        )
    }
}