package hr.fer.opp.zabavanet.utlis.networking.firebaseStorage

import android.net.Uri
import androidx.core.net.toUri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask
import hr.fer.opp.zabavanet.model.MediaModel
import java.util.*

class FirebaseStorageService(private val storage: FirebaseStorage) {

    private val IMAGES_FOLDER = "images/"
    private val VIDEOS_FOLDER = "videos/"

    private val storageRef = storage.reference

    fun uploadMedia(mediaModel: MediaModel, subfolder: String?): UploadTask {

        return if (mediaModel.type == MediaModel.MediaType.IMAGE) {
            storageRef.child(IMAGES_FOLDER + subfolder + "/${UUID.randomUUID()}")
                .putFile(mediaModel.uri.toUri())
        } else {
            storageRef.child(VIDEOS_FOLDER + subfolder + "/${UUID.randomUUID()}")
                .putFile(mediaModel.uri.toUri())
        }
    }

    fun getImageReferences(
        subfolder: String,
        errors: MutableLiveData<String>
    ): LiveData<List<StorageReference>> {
        val mutable = MutableLiveData<List<StorageReference>>()

        storage.reference
            .child(IMAGES_FOLDER + subfolder)
            .listAll()
            .addOnSuccessListener {
                val refs = arrayListOf<StorageReference>()
                it.items.forEach { storageReference ->
                    refs.add(storageReference)
                }
                mutable.postValue(refs)
            }
            .addOnFailureListener {
                errors.postValue(it.message)
            }

        return mutable
    }

    fun getVideoReferences(
        subfolder: String,
        errors: MutableLiveData<String>
    ): LiveData<List<StorageReference>> {
        val mutable = MutableLiveData<List<StorageReference>>()

        storage.reference.child(VIDEOS_FOLDER + subfolder)
            .listAll()
            .addOnSuccessListener {
                val refs = arrayListOf<StorageReference>()
                it.items.forEach {
                    refs.add(it)
                }
                mutable.postValue(refs)
            }
            .addOnFailureListener {
                errors.postValue(it.message)
            }

        return mutable
    }

    fun getUri(
        storageReference: StorageReference,
        errors: MutableLiveData<String>
    ): LiveData<Uri> {
        val mutable = MutableLiveData<Uri>()

        storageReference.downloadUrl
            .addOnSuccessListener {
                mutable.postValue(it)
            }
            .addOnFailureListener {
                errors.postValue(it.message)
            }

        return mutable
    }

    fun deleteProfilePicture(
        email: String,
        errors: MutableLiveData<String>
    ): LiveData<Unit> {
        val mutable = MutableLiveData<Unit>()

        storageRef.child(IMAGES_FOLDER + email).listAll()
            .addOnSuccessListener {
                if (it.items.isNotEmpty()) {
                    it.items[0].delete()
                        .addOnSuccessListener { mutable.postValue(null) }
                        .addOnFailureListener { errors.postValue(it.message) }
                } else {
                    mutable.postValue(null)
                }
            }.addOnFailureListener {
                errors.postValue(it.message)
            }

        return mutable
    }

    fun uploadProfilePicture(
        mediaModel: MediaModel,
        subfolder: String?,
        errors: MutableLiveData<String>
    ): LiveData<StorageReference> {
        val mutable = MutableLiveData<StorageReference>()

        uploadMedia(mediaModel, subfolder)
            .addOnSuccessListener {
                mutable.postValue(it.metadata?.reference)
            }
            .addOnFailureListener {
                errors.postValue(it.message)
            }

        return mutable
    }

    fun deleteMedia(refs: List<StorageReference>) {
        refs.forEach {
            it.delete()
        }
    }
}
