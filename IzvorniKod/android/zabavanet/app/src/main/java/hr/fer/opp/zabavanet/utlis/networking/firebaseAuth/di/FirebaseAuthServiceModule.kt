package hr.fer.opp.zabavanet.utlis.networking.firebaseAuth.di

import com.google.firebase.auth.FirebaseAuth
import dagger.Module
import dagger.Provides
import hr.fer.opp.zabavanet.app.di.diScope.PerApplication
import hr.fer.opp.zabavanet.utlis.networking.firebaseAuth.FirebaseAuthService

@Module
class FirebaseAuthServiceModule {

    @Provides
    @PerApplication
    fun provideFirebaseAuthService(): FirebaseAuthService {
        return FirebaseAuthService(FirebaseAuth.getInstance())
    }
}