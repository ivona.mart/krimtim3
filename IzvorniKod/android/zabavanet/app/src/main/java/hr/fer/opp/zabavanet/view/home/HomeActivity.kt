package hr.fer.opp.zabavanet.view.home

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import hr.fer.opp.zabavanet.R
import hr.fer.opp.zabavanet.base.BaseActivity
import hr.fer.opp.zabavanet.base.BaseFragment
import hr.fer.opp.zabavanet.model.User
import hr.fer.opp.zabavanet.utlis.codes.ActionCode
import hr.fer.opp.zabavanet.utlis.codes.RequestCode
import hr.fer.opp.zabavanet.utlis.codes.ResultCode
import hr.fer.opp.zabavanet.view.allEvents.AllEventsFragment
import hr.fer.opp.zabavanet.view.basicInfo.BasicInfoActivity
import hr.fer.opp.zabavanet.view.eventDetails.EventDetailsActivity
import hr.fer.opp.zabavanet.view.myEvents.MyEventsFragment
import hr.fer.opp.zabavanet.view.profile.ProfileFragment
import hr.fer.opp.zabavanet.viewModel.allEvents.AllEventsFragmentViewModel
import hr.fer.opp.zabavanet.viewModel.home.HomeActivityViewModel
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : BaseActivity<HomeActivityViewModel>() {

    companion object {
        const val INTENT_USER = "user_param"
        const val INTENT_EVENT_ID = "event_id"
    }

    private lateinit var mainContentFragmentManager: FragmentManager
    private lateinit var allEventsFragment: BaseFragment<AllEventsFragmentViewModel>
    private lateinit var user: User

    private var eventId: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        allEventsFragment = AllEventsFragment.newInstance()

        mainContentFragmentManager = supportFragmentManager
        mainContentFragmentManager
            .beginTransaction()
            .add(R.id.fragment_container, allEventsFragment)
            .commit()

        user = viewModel.getUserFromPreferences()!!

        if (user.firstName.isNullOrBlank() || user.lastName.isNullOrBlank() || user.username.isNullOrBlank()) {
            startBasicInfoActivity(user, ActionCode.FIRST_EDIT)
        } else if (user.dateOfBirthLong == null || user.dateOfBirth == null || user.zodiacSign.isNullOrBlank()) {
            startBasicInfoActivity(user, ActionCode.GOOGLE_SET_BASIC_INFO)
        }

        eventId = intent.getStringExtra(INTENT_EVENT_ID)
        eventId?.let {
            startEventDetailsActivity(it)
        }

        viewModel.setDeviceToken(user)

        initUI()
    }

    override fun getViewModel(): HomeActivityViewModel {
        return ViewModelProviders.of(this).get(HomeActivityViewModel::class.java)
    }

    private fun initUI() {
        viewModel.errors.observe(this, Observer {
            showErrorAlert(it)
            hideLoading()
        })

        bottom_navigation.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.all_events -> {
                    replaceFragment(AllEventsFragment.newInstance())
                    true
                }

                R.id.my_events -> {
                    replaceFragment(MyEventsFragment.newInstance(viewModel.getUserFromPreferences()!!))
                    true
                }

                R.id.profile -> {
                    replaceFragment(ProfileFragment.newInstance(viewModel.getUserFromPreferences()!!))
                    true
                }

                else -> {
                    showErrorAlert("No such fragment")
                    true
                }
            }
        }
    }

    private fun replaceFragment(fragment: Fragment) {
        mainContentFragmentManager
            .beginTransaction()
            .replace(R.id.fragment_container, fragment)
            .commit()
    }

    private fun startBasicInfoActivity(user: User, action: String) {
        val intent = Intent(this, BasicInfoActivity::class.java)
        intent.putExtra(BasicInfoActivity.INTENT_USER, user)
        intent.action = action
        startActivityForResult(intent, RequestCode.EDIT)
    }

    private fun startEventDetailsActivity(eventId: String) {
        viewModel.getEvent(eventId)
            .observe(this, Observer {
                val intent = Intent(this, EventDetailsActivity::class.java)
                if (it.organizer == viewModel.getUserRef(user)) {
                    intent.action = ActionCode.OPEN_AS_ORGANIZER
                } else {
                    intent.action = ActionCode.OPEN_AS_VISITOR
                }
                EventDetailsActivity.eventStack.push(it)
                startActivity(intent)
            })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        data ?: return

        if (requestCode == RequestCode.EDIT) {
            if (resultCode == ResultCode.SET_BASIC_INFO) {
                val user = data.getSerializableExtra(INTENT_USER) as User
                viewModel.saveUserToPreferences(user)
                initUI()
            }
        }
    }
}
