package hr.fer.opp.zabavanet.viewModel.basicInfo.di

import dagger.Subcomponent
import hr.fer.opp.zabavanet.app.di.diScope.PerViewModel
import hr.fer.opp.zabavanet.viewModel.basicInfo.BasicInfoActivityViewModel

@PerViewModel
@Subcomponent
interface BasicInfoViewModelComponent {

    fun inject(basicInfoActivityViewModel: BasicInfoActivityViewModel)
}