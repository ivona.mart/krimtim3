package hr.fer.opp.zabavanet.utlis.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.storage.StorageReference
import hr.fer.opp.zabavanet.R
import hr.fer.opp.zabavanet.model.User
import hr.fer.opp.zabavanet.utlis.glide.GlideRequests
import kotlinx.android.synthetic.main.item_user_short.view.*

class InterestedVisitorsAdapter(
    var userList: List<Attendee>,
    glideRequests: GlideRequests,
    private val listener: OnClickListener
) : RecyclerView.Adapter<InterestedVisitorsAdapter.ViewHolder>() {

    private val requestBuilder = glideRequests.asDrawable().fitCenter()

    override fun getItemCount(): Int {
        return userList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)
        val recyclerViewItem = inflater.inflate(R.layout.item_user_short, parent, false)
        return ViewHolder(recyclerViewItem)
    }

    override fun getItemViewType(position: Int): Int {
        return userList[position].attendance.intValue
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val current = userList[position]
        val currentUser = current.user

        holder.itemView.text_username.text = currentUser.username

        current.profilePicture?.let {
            requestBuilder.clone().load(it)
                .into(holder.itemView.image_profile_picture)

        } ?: requestBuilder.clone().load(
            ContextCompat.getDrawable(
                holder.itemView.context,
                R.drawable.default_profile_picture
            )
        ).into(holder.itemView.image_profile_picture)


        holder.itemView.setOnClickListener {
            listener.onClick(current, position)
        }

        holder.itemView.setOnLongClickListener {
            listener.onLongClick(current, position)
        }

        when (holder.itemViewType) {
            Attendance.GOING.intValue -> {
                holder.itemView.background =
                    ContextCompat.getDrawable(
                        holder.itemView.context,
                        R.drawable.background_event_going
                    )
            }

            Attendance.MAYBE_GOING.intValue -> {
                holder.itemView.background =
                    ContextCompat.getDrawable(
                        holder.itemView.context,
                        R.drawable.background_event_maybe_going
                    )
            }

            Attendance.NOT_GOING.intValue -> {
                holder.itemView.background =
                    ContextCompat.getDrawable(
                        holder.itemView.context,
                        R.drawable.background_event_not_going
                    )
            }
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    data class Attendee(
        val user: User,
        val profilePicture: StorageReference?,
        val attendance: Attendance
    )

    enum class Attendance(val intValue: Int) {
        GOING(1),
        MAYBE_GOING(2),
        NOT_GOING(3)
    }

    interface OnClickListener {
        fun onClick(item: Attendee, position: Int)
        fun onLongClick(item: Attendee, position: Int): Boolean
    }
}