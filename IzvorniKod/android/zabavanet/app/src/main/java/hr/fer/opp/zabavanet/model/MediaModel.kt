package hr.fer.opp.zabavanet.model

import java.io.Serializable

data class MediaModel(val uri: String, val type: MediaType) : Serializable {

    enum class MediaType {
        IMAGE,
        VIDEO
    }
}