package hr.fer.opp.zabavanet.utlis.networking.firebaseAuth

import android.os.Handler
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.GoogleAuthProvider

class FirebaseAuthService(private val auth: FirebaseAuth) {

    fun createUserWithEmailAndPassword(
        email: String,
        password: String,
        errors: MutableLiveData<String>
    ): LiveData<AuthResult> {
        val mutable = MutableLiveData<AuthResult>()

        auth.createUserWithEmailAndPassword(email, password)
            .addOnSuccessListener { mutable.postValue(it) }
            .addOnFailureListener { errors.postValue(it.message) }

        return mutable
    }

    fun getCurrentUser(): FirebaseUser? {
        return auth.currentUser
    }

    fun signInWithEmailAndPassword(
        email: String,
        password: String,
        errors: MutableLiveData<String>
    ): LiveData<AuthResult> {
        val mutable = MutableLiveData<AuthResult>()

        auth.signInWithEmailAndPassword(email, password)
            .addOnSuccessListener { mutable.postValue(it) }
            .addOnFailureListener { errors.postValue(it.message) }

        return mutable
    }

    fun waitForVerifyEmail(
        email: String,
        password: String,
        errors: MutableLiveData<String>
    ): LiveData<Unit> {

        val mutable = MutableLiveData<Unit>()
        val handler = Handler()

        val runnable = object : Runnable {
            override fun run() {
                auth.signInWithEmailAndPassword(email, password)
                    .addOnSuccessListener { authResult ->
                        if (authResult.user!!.isEmailVerified) {
                            mutable.postValue(null)
                        } else {
                            handler.postDelayed(this, 2000)
                        }

                    }.addOnFailureListener { exception ->
                        errors.postValue(exception.message.toString())
                    }
            }
        }
        handler.post(runnable)
        return mutable
    }

    fun signInWithCredential(
        account: GoogleSignInAccount,
        webClientId: String,
        errors: MutableLiveData<String>
    ): LiveData<Unit> {

        val mutable = MutableLiveData<Unit>()
        val credential = GoogleAuthProvider.getCredential(account.idToken, webClientId)

        auth.signInWithCredential(credential)
            .addOnSuccessListener {
                mutable.postValue(null)
            }
            .addOnFailureListener {
                errors.postValue(it.message)
            }

        return mutable
    }

    fun resetPassword(email: String, errors: MutableLiveData<String>): LiveData<Unit> {
        val mutable = MutableLiveData<Unit>()
        auth.sendPasswordResetEmail(email)
            .addOnSuccessListener {
                mutable.postValue(null)
            }
            .addOnFailureListener {
                errors.postValue(it.message)
            }
        return mutable
    }

    fun signOut() {
        auth.signOut()
    }
}