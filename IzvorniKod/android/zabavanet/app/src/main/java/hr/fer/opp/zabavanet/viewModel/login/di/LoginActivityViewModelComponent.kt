package hr.fer.opp.zabavanet.viewModel.login.di

import dagger.Subcomponent
import hr.fer.opp.zabavanet.viewModel.login.LoginActivityViewModel

@Subcomponent
interface LoginActivityViewModelComponent {

    fun inject(loginActivityViewModel: LoginActivityViewModel)
}