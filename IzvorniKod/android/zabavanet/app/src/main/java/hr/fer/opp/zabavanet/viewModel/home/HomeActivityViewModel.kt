package hr.fer.opp.zabavanet.viewModel.home

import androidx.lifecycle.LiveData
import com.google.firebase.firestore.DocumentReference
import hr.fer.opp.zabavanet.app.di.ApplicationComponent
import hr.fer.opp.zabavanet.base.BaseViewModel
import hr.fer.opp.zabavanet.model.Event
import hr.fer.opp.zabavanet.model.User
import hr.fer.opp.zabavanet.utlis.networking.firebaseAuth.FirebaseAuthService
import hr.fer.opp.zabavanet.utlis.networking.firestore.FirestoreService
import hr.fer.opp.zabavanet.utlis.session.Session
import javax.inject.Inject

class HomeActivityViewModel : BaseViewModel() {

    @Inject
    lateinit var firebaseAuth: FirebaseAuthService

    @Inject
    lateinit var session: Session

    @Inject
    lateinit var firestoreService: FirestoreService

    override fun injectToAppComponent(applicationComponent: ApplicationComponent) {
        return applicationComponent.plusHome().inject(this)
    }

    fun getUserFromPreferences(): User? {
        return session.user
    }

    fun saveUserToPreferences(user: User) {
        session.user = user
    }

    fun setDeviceToken(user: User) {
        return firestoreService.setDeviceToken(user.email!!)
    }

    fun getEvent(eventId: String): LiveData<Event> {
        return firestoreService.getEvent(eventId, errors)
    }

    fun getUserRef(user: User): DocumentReference {
        return firestoreService.getDocumentReference(
            FirestoreService.USERS_COLLECTION,
            user.email!!
        )
    }
}