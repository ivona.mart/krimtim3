package hr.fer.opp.zabavanet.base

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import hr.fer.opp.zabavanet.app.App
import hr.fer.opp.zabavanet.app.di.ApplicationComponent

abstract class BaseViewModel: ViewModel() {

    internal val errors: MutableLiveData<String> = MutableLiveData()

    init {
        this.injectToAppComponent(App.instance.applicationComponent)
    }

    abstract fun injectToAppComponent(applicationComponent: ApplicationComponent)
}