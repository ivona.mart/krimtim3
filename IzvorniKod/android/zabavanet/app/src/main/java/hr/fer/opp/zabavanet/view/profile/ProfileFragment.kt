package hr.fer.opp.zabavanet.view.profile

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.storage.StorageReference
import hr.fer.opp.zabavanet.R
import hr.fer.opp.zabavanet.base.BaseFragment
import hr.fer.opp.zabavanet.model.Notification
import hr.fer.opp.zabavanet.model.User
import hr.fer.opp.zabavanet.utlis.adapter.NotificationsAdapter
import hr.fer.opp.zabavanet.utlis.codes.ActionCode
import hr.fer.opp.zabavanet.utlis.codes.RequestCode
import hr.fer.opp.zabavanet.utlis.codes.ResultCode
import hr.fer.opp.zabavanet.utlis.dialogManager.DialogManager
import hr.fer.opp.zabavanet.utlis.extensionFunctions.toPattern
import hr.fer.opp.zabavanet.utlis.glide.GlideApp
import hr.fer.opp.zabavanet.view.basicInfo.BasicInfoActivity
import hr.fer.opp.zabavanet.view.confirmationDialog.ConfirmationDialogFragment
import hr.fer.opp.zabavanet.view.login.LoginActivity
import hr.fer.opp.zabavanet.view.organizationProfile.OrganizationProfileActivity
import hr.fer.opp.zabavanet.view.payment.PaymentActivity
import hr.fer.opp.zabavanet.viewModel.profile.ProfileFragmentViewModel
import kotlinx.android.synthetic.main.fragment_profile.*

class ProfileFragment : BaseFragment<ProfileFragmentViewModel>() {

    private val USER_PARAM = "user"
    private var user: User? = null

    private lateinit var dialogManager: DialogManager

    private lateinit var imageRefs: List<StorageReference>
    private lateinit var notifications: List<Notification>
    private lateinit var notificationsAdapter: NotificationsAdapter

    companion object {
        @JvmStatic
        fun newInstance(userParam: User) =
            ProfileFragment().apply {
                arguments = Bundle().apply {
                    putSerializable(USER_PARAM, userParam)
                }
            }

        const val INTENT_USER = "user"
        const val ROLE_VISITOR = "visitor"
        const val ROLE_ORGANIZER = "organizer"
    }

    override fun getViewModel(): ProfileFragmentViewModel {
        return ViewModelProviders.of(this).get(ProfileFragmentViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            user = it.getSerializable(USER_PARAM) as User
        }
        dialogManager = DialogManager(fragmentManager!!)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.errors.observe(this, Observer {
            showErrorAlert(it)
        })

        initUI()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    private fun initUI() {

        viewModel.getNotifications(user!!).observe(this, Observer {
            notifications = it
            initNotificationsRecycler(notifications)
        })

        btn_sign_out.setOnClickListener { signOut() }

        if (user!!.role == ROLE_VISITOR) {
            btn_organization_profile.visibility = View.GONE

            btn_become_organizer.visibility = View.VISIBLE

            btn_become_organizer.setOnClickListener {
                dialogManager.openConfirmationDialog(
                    "Choose payment method",
                    "PayPal",
                    "Credit card",
                    object : ConfirmationDialogFragment.DialogListener {
                        override fun onPositive() {
                            startPaymentActivity(ActionCode.PAYPAL)
                        }

                        override fun onNegative() {
                            startPaymentActivity(ActionCode.CREDIT_CARD)
                        }
                    })
            }
        } else if (user!!.role == ROLE_ORGANIZER) {
            btn_become_organizer.visibility = View.GONE

            btn_organization_profile.visibility = View.VISIBLE

            btn_organization_profile.setOnClickListener {
                startOrganizationProfileAcitivity()
            }
        }

        fab_edit_profile.setOnClickListener { startBasicInfoActivity() }

        user?.let { user ->
            text_email.text = user.email
            text_username.text = user.username
            text_first_name.text = user.firstName
            text_last_name.text = user.lastName
            text_date_of_birth.text = user.dateOfBirth?.toPattern("dd.MM.yyyy")
            text_zodiac_sign.text = user.zodiacSign

            viewModel.getImageReferences(user.email!!).observe(this, Observer {
                if (it.isNotEmpty()) {
                    imageRefs = it as List<StorageReference>

                    GlideApp.with(this).load(imageRefs.first())
                        .into(image_profile_picture)
                } else {
                    image_profile_picture.setImageResource(R.drawable.default_profile_picture)
                }
            })


        }
            ?: throw IllegalStateException("Static variable 'user' in ProfileFragment must be set before starting the activity")

    }

    private fun signOut() {
        viewModel.signOut()
        startLoginActivity()
    }

    private fun startPaymentActivity(action: String) {
        val intent = Intent(context, PaymentActivity::class.java)
        intent.putExtra(PaymentActivity.INTENT_USER, user)
        intent.action = action
        startActivityForResult(intent, RequestCode.ORGANIZER)
    }

    private fun startLoginActivity() {
        val intent = Intent(context, LoginActivity::class.java)
        intent.addFlags(
            Intent.FLAG_ACTIVITY_CLEAR_TOP
                    or Intent.FLAG_ACTIVITY_NEW_TASK
                    or Intent.FLAG_ACTIVITY_CLEAR_TASK
        )
        startActivity(intent)
    }

    private fun startBasicInfoActivity() {
        val intent = Intent(context, BasicInfoActivity::class.java)
        intent.putExtra(BasicInfoActivity.INTENT_USER, user)
        intent.action = ActionCode.EDIT
        startActivityForResult(intent, RequestCode.EDIT)
    }

    private fun startOrganizationProfileAcitivity() {
        val intent = Intent(context, OrganizationProfileActivity::class.java)
        intent.putExtra(OrganizationProfileActivity.INTENT_USER, user)
        intent.action = ActionCode.EDIT
        startActivity(intent)
    }

    private fun initNotificationsRecycler(notifications: List<Notification>) {

        if (notifications.isEmpty()) {
            layout_notifications.visibility = View.GONE
        } else {
            layout_notifications.visibility = View.VISIBLE
        }

        if (!::notificationsAdapter.isInitialized) {
            notificationsAdapter =
                NotificationsAdapter(notifications, object : NotificationsAdapter.OnClickListener {
                    override fun onClick(item: Notification, position: Int) {
                    }

                    override fun onLongClick(item: Notification, position: Int): Boolean {
                        dialogManager.openConfirmationDialog(getString(R.string.are_you_sure_you_want_to_delete_notification),
                            getString(R.string.yes),
                            getString(R.string.no),
                            object : ConfirmationDialogFragment.DialogListener {
                                override fun onNegative() {
                                }

                                override fun onPositive() {
                                    showLoading()
                                    viewModel.deleteNotification(item)
                                        .observe(activity!!, Observer {
                                            (notifications as ArrayList).remove(item)
                                            initNotificationsRecycler(notifications)
                                            hideLoading()
                                        })
                                }
                            })
                        return false
                    }
                })

            recycler_notifications.adapter = notificationsAdapter
            recycler_notifications.layoutManager =
                LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        } else {
            notificationsAdapter.notifications = notifications
            notificationsAdapter.notifyDataSetChanged()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        data ?: return

        if (requestCode == RequestCode.EDIT) {
            if (resultCode == ResultCode.SUCCESS) {
                user = data.getSerializableExtra(INTENT_USER) as User
                user?.let {
                    initUI()
                }
            } else {
                showMessage("ERROR")
            }
        } else if (requestCode == RequestCode.ORGANIZER) {
            if (resultCode == ResultCode.SUCCESS) {
                user = data.getSerializableExtra(INTENT_USER) as User
                user?.let {
                    initUI()
                }
            } else {
                showMessage("ERROR")
            }
        }
    }
}