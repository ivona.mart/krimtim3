package hr.fer.opp.zabavanet.utlis.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.storage.StorageReference
import hr.fer.opp.zabavanet.R
import hr.fer.opp.zabavanet.utlis.glide.GlideRequests
import kotlinx.android.synthetic.main.item_image.view.*

class ImageRefAdapter(private var imageRefs: List<StorageReference>, glideRequests: GlideRequests) :
    RecyclerView.Adapter<ImageRefAdapter.ImageRefViewHolder>() {

    private val requestBuilder = glideRequests.asDrawable().fitCenter()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageRefViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_image, parent, false)

        return ImageRefViewHolder(view)
    }

    override fun onBindViewHolder(holder: ImageRefViewHolder, position: Int) {
        val current = imageRefs[position]

        requestBuilder.clone()
//            .placeholder(R.drawable.ic_cloud_download_black_24dp)
            .load(current)
            .into(holder.itemView.image)
    }

    override fun getItemCount(): Int {
        return imageRefs.size
    }

    class ImageRefViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}