package hr.fer.opp.zabavanet.view.splash

import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.firebase.Timestamp
import hr.fer.opp.zabavanet.R
import hr.fer.opp.zabavanet.base.BaseActivity
import hr.fer.opp.zabavanet.model.User
import hr.fer.opp.zabavanet.view.home.HomeActivity
import hr.fer.opp.zabavanet.view.login.LoginActivity
import hr.fer.opp.zabavanet.viewModel.splash.SplashActivityViewModel

class SplashActivity : BaseActivity<SplashActivityViewModel>() {

    companion object {
        const val INTENT_EVENT_ID = "eventId"
    }

    private var eventId: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        eventId = intent.getStringExtra(INTENT_EVENT_ID)

        checkLastUser()
    }

    override fun getViewModel(): SplashActivityViewModel {
        return ViewModelProviders.of(this).get(SplashActivityViewModel::class.java)
    }

    private fun checkLastUser() {
        val firebaseUser = viewModel.getCurrentUser()

        firebaseUser?.let {
            logD("Firebase user not null")
            if (it.isEmailVerified) {
                viewModel.getUserFromDb(it.email!!).observe(this, Observer { user ->
                    if (user == null) {
                        logD("No such user in database")
                        startLoginActivity(eventId)
                    } else {
                        user.membershipDuration?.let { membershipDuration ->
                            if (membershipDuration < Timestamp.now() && user.role == "organizer") {
                                user.role = "visitor"
                                viewModel.updateUserRole(user).observe(this, Observer {
                                    startHomeActivity(user, eventId) // membership not valid anymore
                                    showMessage(getString(R.string.membership_inactive))
                                })
                            } else {
                                startHomeActivity(user, eventId) // membership still valid

                            }
                        } ?: startHomeActivity(user, eventId) // never was an organizer

                        logD("Found user in database")
                    }
                })
            } else {
                startLoginActivity(eventId)
            }
        } ?: startLoginActivity(eventId)
    }

    private fun startHomeActivity(user: User, eventId: String?) {
        viewModel.saveUserToPreferences(user)
        val intent = Intent(this, HomeActivity::class.java)
        intent.addFlags(
            Intent.FLAG_ACTIVITY_CLEAR_TOP
                    or Intent.FLAG_ACTIVITY_NEW_TASK
                    or Intent.FLAG_ACTIVITY_CLEAR_TASK
        )
        intent.putExtra(HomeActivity.INTENT_EVENT_ID, eventId)
        startActivity(intent)
    }

    private fun startLoginActivity(eventId: String?) {
        logD("Starting login activity")
        val intent = Intent(this, LoginActivity::class.java)
        intent.addFlags(
            Intent.FLAG_ACTIVITY_CLEAR_TOP
                    or Intent.FLAG_ACTIVITY_NEW_TASK
                    or Intent.FLAG_ACTIVITY_CLEAR_TASK
        )
        intent.putExtra(LoginActivity.INTENT_EVENT_ID, eventId)
        startActivity(intent)
    }
}
