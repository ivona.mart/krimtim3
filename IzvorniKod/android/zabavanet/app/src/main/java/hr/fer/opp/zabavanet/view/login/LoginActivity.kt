package hr.fer.opp.zabavanet.view.login

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import hr.fer.opp.zabavanet.R
import hr.fer.opp.zabavanet.base.BaseActivity
import hr.fer.opp.zabavanet.model.User
import hr.fer.opp.zabavanet.view.emailVerification.EmailVerificationActivity
import hr.fer.opp.zabavanet.view.home.HomeActivity
import hr.fer.opp.zabavanet.view.register.RegisterActivity
import hr.fer.opp.zabavanet.viewModel.login.LoginActivityViewModel
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : BaseActivity<LoginActivityViewModel>() {

    companion object {
        const val INTENT_EVENT_ID = "event id"
    }

    private var eventId: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        eventId = intent.getStringExtra(INTENT_EVENT_ID)

        initUI()
    }

    override fun getViewModel(): LoginActivityViewModel {
        return ViewModelProviders.of(this).get(LoginActivityViewModel::class.java)
    }

    private fun initUI() {
        viewModel.errors.observe(this, Observer {
            hideLoading()
            showErrorAlert(it)
        })

        btn_forgot_password.setOnClickListener { resetPassword() }

        btn_login.setOnClickListener { loginUser() }

        btn_register.setOnClickListener {
            val intent = Intent(this@LoginActivity, RegisterActivity::class.java)
            startActivity(intent)
        }

        btn_sign_in_with_google.setOnClickListener {
            val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build()

            val googleSignInClient = GoogleSignIn.getClient(this, gso)

            val intent = googleSignInClient.signInIntent
            startActivityForResult(intent, 0)
        }
    }

    private fun resetPassword() {
        val email = text_email.text.toString()

        if (email.isEmpty()) {
            showMessage(getString(R.string.please_enter_email))
            return
        }

        viewModel.resetPassword(email).observe(this, Observer {
            showMessage(getString(R.string.password_reset_email_sent))
        })
    }

    private fun loginUser() {
        val email = text_email.text.toString()
        val password = text_password.text.toString()

        if (email.isEmpty()) {
            showMessage(getString(R.string.please_enter_email))
            return
        }

        if (password.isEmpty()) {
            showMessage(getString(R.string.please_enter_pass))
            return
        }

        showLoading()

        viewModel.signIn(email, password).observe(this, Observer {
            hideLoading()
            if (viewModel.isEmailVerified()) {
                viewModel.getUserFromDatabase(email).observe(this, Observer { user ->
                    startHomeActivity(user, eventId)
                })
            } else {
                startEmailVerificationActivity(email, password)
            }
        })
    }

    private fun signInOrCreateUser(account: GoogleSignInAccount?) {
        showLoading()
        viewModel.logInWithCredentials(account!!, getString(R.string.default_web_client_id))
            .observe(this, Observer {
                viewModel.getUserFromDatabase(viewModel.getCurrentUser()?.email)
                    .observe(this, Observer { user ->
                        if (user == null) {
                            val userModel = User(
                                account.givenName.toString(),
                                account.familyName.toString(),
                                account.email.toString(),
                                account.displayName.toString(),
                                "visitor"
                            )
                            viewModel.createUserInDatabase(
                                userModel
                            ).observe(this, Observer {
                                viewModel.getUserFromDatabase(userModel.email)
                                    .observe(this, Observer {
                                        viewModel.saveUserToPreferences(it)
                                        hideLoading()
                                        startHomeActivity(it, eventId)
                                    })
                            })
                        } else {
                            hideLoading()
                            startHomeActivity(user, eventId)
                        }
                    })
            })
    }

    private fun startHomeActivity(user: User, eventId: String?) {
        viewModel.saveUserToPreferences(user)
        val intent = Intent(this, HomeActivity::class.java)
        intent.putExtra(HomeActivity.INTENT_EVENT_ID, eventId)
        startActivity(intent)
        finish()
    }

    private fun startEmailVerificationActivity(email: String, password: String) {
        val intent = Intent(this, EmailVerificationActivity::class.java)
        intent.putExtra(EmailVerificationActivity.INTENT_PARAM_EMAIL, email)
        intent.putExtra(EmailVerificationActivity.INTENT_PARAM_PASSWORD, password)
        startActivity(intent)
        finish()
    }

    private fun handleSignInResult(task: Task<GoogleSignInAccount>?) {
        try {
            val account = task?.getResult(ApiException::class.java)
            signInOrCreateUser(account)
        } catch (e: ApiException) {
            Log.w("Zabava.net", "signInResult:failed code=" + e.statusCode)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 0) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            handleSignInResult(task)
        }
    }
}
