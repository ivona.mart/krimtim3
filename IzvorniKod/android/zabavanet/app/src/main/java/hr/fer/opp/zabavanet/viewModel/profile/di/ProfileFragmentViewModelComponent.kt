package hr.fer.opp.zabavanet.viewModel.profile.di

import dagger.Subcomponent
import hr.fer.opp.zabavanet.viewModel.profile.ProfileFragmentViewModel

@Subcomponent
interface ProfileFragmentViewModelComponent {

    fun inject(profileFragmentViewModel: ProfileFragmentViewModel)
}