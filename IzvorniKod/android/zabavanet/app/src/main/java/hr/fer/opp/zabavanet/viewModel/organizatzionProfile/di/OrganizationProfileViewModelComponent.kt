package hr.fer.opp.zabavanet.viewModel.organizatzionProfile.di

import dagger.Subcomponent
import hr.fer.opp.zabavanet.app.di.diScope.PerViewModel
import hr.fer.opp.zabavanet.viewModel.organizatzionProfile.OrganizationProfileActivityViewModel

@PerViewModel
@Subcomponent
interface OrganizationProfileViewModelComponent {

    fun inject(organizationInfoActivityViewModel: OrganizationProfileActivityViewModel)
}