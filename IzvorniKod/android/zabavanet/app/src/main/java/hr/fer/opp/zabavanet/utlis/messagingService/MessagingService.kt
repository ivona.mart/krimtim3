package hr.fer.opp.zabavanet.utlis.messagingService

import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.core.app.NotificationCompat
import com.google.firebase.FirebaseApp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import hr.fer.opp.zabavanet.R
import hr.fer.opp.zabavanet.app.App
import hr.fer.opp.zabavanet.utlis.networking.firebaseAuth.FirebaseAuthService
import hr.fer.opp.zabavanet.utlis.networking.firestore.FirestoreService
import hr.fer.opp.zabavanet.view.splash.SplashActivity

class MessagingService : FirebaseMessagingService() {

    companion object {
        private const val TAG = "Zabavanet notif"
    }

    private lateinit var firestoreService: FirestoreService

    override fun onNewToken(token: String) {

        val firestore = FirestoreService(FirebaseFirestore.getInstance())
        val auth = FirebaseAuthService(FirebaseAuth(FirebaseApp.getInstance()))

        val firebaseUser = auth.getCurrentUser()

        firebaseUser?.email?.let {
            firestore.updateDeviceToken(it, token)
        }
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)

        firestoreService = FirestoreService(FirebaseFirestore.getInstance())

        Log.d(TAG, "From: ${remoteMessage.from}")

        // Check if message contains a data payload.


        // Check if message contains a notification payload.
        remoteMessage.notification?.let { notif ->

            Log.d(TAG, "Message Notification Body: ${notif.body}")

            remoteMessage.data.isNotEmpty().let {

                if (it) {

                    Log.d(TAG, "Message data: ${remoteMessage.data}")
                    val intent = Intent(baseContext, SplashActivity::class.java)
                    intent.putExtra(SplashActivity.INTENT_EVENT_ID, remoteMessage.data["eventId"])

                    val pendingIntent =
                        PendingIntent.getActivity(
                            baseContext,
                            0,
                            intent,
                            PendingIntent.FLAG_CANCEL_CURRENT
                        )

                    val notification = NotificationCompat.Builder(baseContext, App.EVENT_CHANNEL_ID)
                        .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                        .setContentText(notif.body)
                        .setContentTitle(notif.title)
                        .setSmallIcon(R.drawable.ic_confetti)
                        .setContentIntent(pendingIntent)
                        .build()

                    val notificationManger =
                        getSystemService((Context.NOTIFICATION_SERVICE)) as NotificationManager
                    notificationManger.notify(0, notification)
                }
            }
        }
    }
}