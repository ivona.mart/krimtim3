package hr.fer.opp.zabavanet.view.emailVerification

import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import hr.fer.opp.zabavanet.R
import hr.fer.opp.zabavanet.base.BaseActivity
import hr.fer.opp.zabavanet.model.User
import hr.fer.opp.zabavanet.view.home.HomeActivity
import hr.fer.opp.zabavanet.viewModel.verification.EmailVerificationActivityViewModel
import kotlinx.android.synthetic.main.activity_verification.*

class EmailVerificationActivity : BaseActivity<EmailVerificationActivityViewModel>() {

    companion object {
        var INTENT_PARAM_EMAIL = "email"
        var INTENT_PARAM_PASSWORD = "password"
    }

    private fun initUI() {
        viewModel.errors.observe(this, Observer {
            hideLoading()
            showErrorAlert(it)
        })

        btn_sendAgain.setOnClickListener { sendAgain() }
    }

    private fun sendAgain() {
        viewModel.sendVerificationEmail()
    }

    override fun getViewModel(): EmailVerificationActivityViewModel {
        return ViewModelProviders.of(this).get(EmailVerificationActivityViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_verification)

        initUI()

        val email = intent.getStringExtra(INTENT_PARAM_EMAIL) as String
        val password = intent.getStringExtra(INTENT_PARAM_PASSWORD) as String
        viewModel.waitForEmailVerification(email, password).observe(this, Observer {
            val user = User("", "", email, "", "visitor")
            viewModel.createUserInDatabase(user)
            viewModel.saveUserToPreferences(user)
            val intent = Intent(this, HomeActivity::class.java)
            intent.addFlags(
                Intent.FLAG_ACTIVITY_CLEAR_TOP
                        or Intent.FLAG_ACTIVITY_NEW_TASK
                        or Intent.FLAG_ACTIVITY_CLEAR_TASK
            )
            startActivity(intent)
        })
    }
}
