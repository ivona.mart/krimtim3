package hr.fer.opp.zabavanet.app

import android.app.Application
import android.app.NotificationChannel
import android.app.NotificationManager
import android.os.Build
import com.google.firebase.messaging.FirebaseMessagingService
import hr.fer.opp.zabavanet.R
import hr.fer.opp.zabavanet.app.di.ApplicationComponent
import hr.fer.opp.zabavanet.app.di.ApplicationModule
import hr.fer.opp.zabavanet.app.di.DaggerApplicationComponent
import hr.fer.opp.zabavanet.utlis.networking.firebaseAuth.di.FirebaseAuthServiceModule
import hr.fer.opp.zabavanet.utlis.networking.firebaseStorage.di.FirebaseStorageServiceModule
import hr.fer.opp.zabavanet.utlis.networking.firestore.di.FirestoreServiceModule
import hr.fer.opp.zabavanet.utlis.networking.okhttp.di.OkHttpModule
import hr.fer.opp.zabavanet.utlis.session.di.SessionModule


class App : Application() {

    companion object {
        lateinit var instance: App
            private set

        const val EVENT_CHANNEL_ID = "event_notif"
    }

    lateinit var applicationComponent: ApplicationComponent

    override fun onCreate() {
        super.onCreate()

        instance = this
        initDaggerComponent()
        createNotificationChannel()
    }

    private fun initDaggerComponent() {
        applicationComponent = DaggerApplicationComponent.builder()
            .applicationModule(ApplicationModule(this))
            .sessionModule(SessionModule(this))
            .firebaseAuthServiceModule(FirebaseAuthServiceModule())
            .firestoreServiceModule(FirestoreServiceModule())
            .firebaseStorageServiceModule(FirebaseStorageServiceModule())
            .okHttpModule(OkHttpModule())
            .build()
    }

    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = getString(R.string.on_create_event_notification_channel_title)
            val descriptionText =
                getString(R.string.on_create_event_notification_channel_description)
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(EVENT_CHANNEL_ID, name, importance)
            channel.description = descriptionText

            val notificationManager =
                getSystemService(FirebaseMessagingService.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }
}