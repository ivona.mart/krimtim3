package hr.fer.opp.zabavanet.utlis.networking.firestore.di

import com.google.firebase.firestore.FirebaseFirestore
import dagger.Module
import dagger.Provides
import hr.fer.opp.zabavanet.app.di.diScope.PerApplication
import hr.fer.opp.zabavanet.utlis.networking.firestore.FirestoreService

@Module
class FirestoreServiceModule {

    @Provides
    @PerApplication
    fun provideFirestoreService(): FirestoreService {
        return FirestoreService(FirebaseFirestore.getInstance())
    }
}