package hr.fer.opp.zabavanet.utlis.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import hr.fer.opp.zabavanet.model.Event
import hr.fer.opp.zabavanet.utlis.extensionFunctions.toPattern
import kotlinx.android.synthetic.main.item_event_short.view.*

open class GenericAdapter<T>(
    private val itemViewId: Int,
    var itemList: ArrayList<T>,
    private val listener: OnClickListener<T>?
) : RecyclerView.Adapter<GenericAdapter.ViewHolder>() {

    override fun getItemCount(): Int {
        return itemList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = itemList[position] as Event

        holder.itemView.text_title.text = item.title
        holder.itemView.text_description.text = item.description
        holder.itemView.text_start_time_end_time.text =
            "${item.startTime?.toPattern(null)} - ${item.endTime?.toPattern(null)}"

        holder.itemView.setOnClickListener {
            listener?.onClick(itemList[position])
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)
        val recyclerViewItem = inflater.inflate(itemViewId, parent, false)
        return ViewHolder(recyclerViewItem)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    interface OnClickListener<T> {
        fun onClick(item: T)
    }
}