package hr.fer.opp.zabavanet.utlis.codes

object ActionCode {
    const val EDIT = "edit"
    const val CREATE = "create"
    const val PAYPAL = "paypal"
    const val CREDIT_CARD = "credit_card"
    const val FIRST_EDIT = "first_edit"
    const val OPEN_AS_ORGANIZER = "open_as_organizer"
    const val OPEN_AS_VISITOR = "open as visitor"
    const val SHOW_ORGANIZATION_PROFILE = "show_organization_profile"
    const val GOOGLE_SET_BASIC_INFO = "google_set_basic_info"
}