package hr.fer.opp.zabavanet.view.organizationProfile

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.google.firebase.Timestamp
import com.google.firebase.storage.StorageReference
import hr.fer.opp.zabavanet.R
import hr.fer.opp.zabavanet.base.BaseActivity
import hr.fer.opp.zabavanet.model.Event
import hr.fer.opp.zabavanet.model.Organization
import hr.fer.opp.zabavanet.model.User
import hr.fer.opp.zabavanet.utlis.adapter.EventsAdapter
import hr.fer.opp.zabavanet.utlis.codes.ActionCode
import hr.fer.opp.zabavanet.utlis.codes.RequestCode
import hr.fer.opp.zabavanet.utlis.codes.ResultCode
import hr.fer.opp.zabavanet.utlis.extensionFunctions.subtractYears
import hr.fer.opp.zabavanet.view.editOrganization.EditOrganizationActivity
import hr.fer.opp.zabavanet.view.eventDetails.EventDetailsActivity
import hr.fer.opp.zabavanet.viewModel.organizatzionProfile.OrganizationProfileActivityViewModel
import kotlinx.android.synthetic.main.activity_organization_profile.*

class OrganizationProfileActivity : BaseActivity<OrganizationProfileActivityViewModel>() {

    companion object {
        const val INTENT_USER = "user"
        const val INTENT_ORGANIZATION = "organization"
    }

    private lateinit var user: User
    private lateinit var organization: Organization
    private lateinit var imageRefs: List<StorageReference>
    private lateinit var organizedEventsAdapter: EventsAdapter
    private var action: String? = null

    override fun getViewModel(): OrganizationProfileActivityViewModel {
        return ViewModelProviders.of(this).get(OrganizationProfileActivityViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_organization_profile)

        user = intent.getSerializableExtra(INTENT_USER) as User
        action = intent.action

        viewModel.errors.observe(this, Observer {
            hideLoading()
            showErrorAlert(it)
        })

        viewModel.getOrganization(user).observe(this, Observer {
            if (it.isNotEmpty()) {
                organization = it.first()
                initUI(action)
            }
        })
    }

    private fun initUI(action: String?) {


        if (viewModel.getUserRef(user) == viewModel.getCurrentUserRef()) {
            fab_edit_organization_profile.visibility = View.VISIBLE
            fab_edit_organization_profile.setOnClickListener {
                startOrganizationInfoActivity()
            }
        } else {
            fab_edit_organization_profile.visibility = View.GONE
        }

        text_organization_name.text = organization.organizationName
        text_web_page.text = organization.webPage
        text_address.text = organization.address

        viewModel.getImageReferences(user).observe(this, Observer {
            if (it.isNotEmpty()) {
                imageRefs = it as List<StorageReference>

                Glide.with(this).load(imageRefs.first())
                    .into(image_organization_picture)
            }
        })

        action?.let {
            if (it == ActionCode.SHOW_ORGANIZATION_PROFILE) {
                viewModel.getOrganizedEvents(user, Timestamp.now().subtractYears(2))
                    .observe(this, Observer {
                    initOrganizedEventsRecycler(it)
                })
            }
        }
    }

    private fun initOrganizedEventsRecycler(events: List<Event>) {
        val eventTypes = arrayListOf<EventsAdapter.EventType>()
        events.forEach {
            when {
                it.organizer == viewModel.getCurrentUserRef() -> eventTypes.add(
                    EventsAdapter.EventType(
                        it,
                        EventsAdapter.ViewType.ORGANIZER
                    )
                )
                it.going!!.contains(viewModel.getCurrentUserRef()) -> eventTypes.add(
                    EventsAdapter.EventType(
                        it,
                        EventsAdapter.ViewType.GOING
                    )
                )
                else -> eventTypes.add(EventsAdapter.EventType(it, EventsAdapter.ViewType.REGULAR))
            }
        }
        if (!::organizedEventsAdapter.isInitialized) {
            organizedEventsAdapter =
                EventsAdapter(eventTypes, object : EventsAdapter.OnClickListener {
                    override fun onClick(item: EventsAdapter.EventType) {
                        startEventDetailsActivity(item.event)
                    }

                    override fun onLongClick(item: EventsAdapter.EventType): Boolean {
                        return false
                    }
                })

            recycler_organized_events.adapter = organizedEventsAdapter
            recycler_organized_events.layoutManager = LinearLayoutManager(this)
        } else {
            organizedEventsAdapter.eventList = eventTypes
            organizedEventsAdapter.notifyDataSetChanged()
        }

        layout_organized_events.visibility = View.VISIBLE
    }

    private fun startEventDetailsActivity(event: Event) {
        val intent = Intent(this, EventDetailsActivity::class.java)
        if (event.organizer == viewModel.getCurrentUserRef()) {
            intent.action = ActionCode.OPEN_AS_ORGANIZER
        } else {
            intent.action = ActionCode.OPEN_AS_VISITOR
        }
        EventDetailsActivity.eventStack.push(event)
        startActivity(intent)
    }

    private fun startOrganizationInfoActivity() {
        val intent = Intent(this, EditOrganizationActivity::class.java)
        intent.putExtra(EditOrganizationActivity.INTENT_USER, user)
        intent.putExtra(EditOrganizationActivity.INTENT_ORGANIZATION, organization)
        intent.action = ActionCode.EDIT
        startActivityForResult(intent, RequestCode.EDIT)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        data ?: return

        if (requestCode == RequestCode.EDIT) {
            when (resultCode) {
                ResultCode.SUCCESS -> {
                    organization = data.getSerializableExtra(INTENT_ORGANIZATION) as Organization
                    initUI(action)
                }

                ResultCode.FAILURE -> showMessage("Failed")

                else -> showMessage("No result")
            }
        }
    }
}