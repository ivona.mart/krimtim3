package hr.fer.opp.zabavanet.base

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.util.Log
import android.widget.Toast
import androidx.fragment.app.Fragment
import hr.fer.opp.zabavanet.R

abstract class BaseFragment<T : BaseViewModel> : Fragment() {

    private lateinit var loadingDialog: Dialog
    internal lateinit var viewModel: T
    private val TAG = "Zabava.net"

    abstract fun getViewModel(): T

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (!::viewModel.isInitialized) {
            viewModel = getViewModel()
        }
    }

    fun showError(message: String) {
        Toast.makeText(
            context,
            message,
            Toast.LENGTH_SHORT
        ).show()
    }

    fun showErrorAlert(text: String) {
        AlertDialog.Builder(context!!)
            .setTitle(resources.getString(R.string.error))
            .setMessage(text)
            .setPositiveButton(resources.getString(R.string.positiveButton), null)
            .create()
            .show()
    }

    fun showMessage(message: String) {
        Toast.makeText(
            context,
            message,
            Toast.LENGTH_SHORT
        ).show()
    }

    fun hideLoading() {
        if (::loadingDialog.isInitialized) {
            loadingDialog.dismiss()
        }
    }

    fun showLoading() {
        if (!::loadingDialog.isInitialized) {
            val builder = AlertDialog.Builder(context as Context)
            builder.setView(R.layout.dialog_loading)
            loadingDialog = builder.create()
            loadingDialog.setCancelable(false)
            loadingDialog.setCanceledOnTouchOutside(false)
        }
        loadingDialog.show()
    }

    fun logD(message: String) {
        Log.d(TAG, message)
    }

    fun logW(message: String) {
        Log.w(TAG, message)
    }
}