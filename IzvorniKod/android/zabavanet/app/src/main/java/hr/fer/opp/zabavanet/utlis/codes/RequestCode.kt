package hr.fer.opp.zabavanet.utlis.codes

object RequestCode {
    const val EDIT = 0
    const val CREATE = 1
    const val IMAGE = 2
    const val VIDEO = 3
    const val ORGANIZER = 4
}