package hr.fer.opp.zabavanet.utlis.session

import android.content.Context
import android.content.SharedPreferences
import com.google.firebase.Timestamp
import com.google.gson.Gson
import hr.fer.opp.zabavanet.model.User

class Session(context: Context) {

    private val USER_STRING = "session_user"

    private val preferences: SharedPreferences =
        context.getSharedPreferences("Settings", Context.MODE_PRIVATE)

    var user: User?
        get() {
            val json = preferences.getString(USER_STRING, null)
            val retVal = Gson().fromJson(json, User::class.java)
            retVal.dateOfBirthLong?.let {
                retVal.dateOfBirth = Timestamp(it, 0)
            }
            return retVal
        }
        set(value) {
            val json = Gson().toJson(value)
            preferences.edit().putString(USER_STRING, json).apply()
        }
}