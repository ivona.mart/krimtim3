package hr.fer.opp.zabavanet.utlis.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.Nullable
import androidx.recyclerview.widget.RecyclerView

open class BaseSingleOptionAdapter<T>(
    private var itemViewId: Int,
    private var itemList: List<T>,
    @Nullable private var defaultSelection: T?,
    private val listener: OnItemSelectedListener<T>,
    private val internalListener: StateChangeListener<T>
) : RecyclerView.Adapter<BaseSingleOptionAdapter.BaseSingleOptionViewHolder>() {

    private var checkedPosition: Int? = 0

    val selected: T?
        get() {
            checkedPosition?.apply {
                return itemList[this]
            }
            return null
        }

    init {
        checkedPosition = if (defaultSelection == null) {
            null
        } else {
            itemList.indexOf(defaultSelection!!)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, i: Int): BaseSingleOptionViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)
        val recyclerViewItem = inflater.inflate(itemViewId, parent, false)
        return BaseSingleOptionViewHolder(
            recyclerViewItem
        )
    }

    override fun onBindViewHolder(holder: BaseSingleOptionViewHolder, position: Int) {

        val item = itemList[position]

        // for first time setup
        if (checkedPosition == position) {
            internalListener.onSelected(item, holder.itemView)
            holder.itemView.isSelected = true
        } else {
            internalListener.onUnselected(item, holder.itemView)
            holder.itemView.isSelected = false
        }

        holder.itemView.setOnClickListener {
            internalListener.onSelected(item, holder.itemView)
            holder.itemView.isSelected = true

            checkedPosition?.apply {
                notifyItemChanged(this)
            }

            if (checkedPosition != position) {
                checkedPosition = position
                notifyItemChanged(checkedPosition!!)
            }

            listener.onSelected(item)
        }
    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    class BaseSingleOptionViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    /**
     * used for changing view state (icon, text, etc.)
     */
    interface StateChangeListener<T> {
        fun onSelected(item: T, itemView: View)
        fun onUnselected(item: T, itemView: View)
    }

    /**
     * used for notifying activity
     */
    interface OnItemSelectedListener<T> {
        fun onSelected(item: T)
    }
}