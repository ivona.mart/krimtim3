package hr.fer.opp.zabavanet.view.addReview

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import hr.fer.opp.zabavanet.R
import kotlinx.android.synthetic.main.dialog_add_review.*

class AddReviewDialog(private val listener: DialogListener?) : DialogFragment() {

    companion object {
        private const val MIN_REVIEW_LENGTH = 1
        private const val MAX_REVIEW_LENGTH = 300
    }

    private var onViewCreatedListener: OnViewCreatedListener? = null

    private lateinit var stars: ArrayList<ImageView>
    private var numberOfStars = -1

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.dialog_add_review, container)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        stars = arrayListOf(img_star1, img_star2, img_star3, img_star4, img_star5)

        initUI()

        onViewCreatedListener?.onViewCreated(view)

    }

    private fun initUI() {
        for ((i, star) in stars.withIndex()) {
            star.setOnClickListener {
                numberOfStars = i
                setStarRating(stars, numberOfStars)
            }
        }

        text_review_text.hint =
            getString(R.string.review_length, MIN_REVIEW_LENGTH, MAX_REVIEW_LENGTH)

        btn_save.setOnClickListener {
            val text = text_review_text.text.toString()
            if (numberOfStars < 0) {
                Toast.makeText(context, getString(R.string.rating_must_be_set), Toast.LENGTH_SHORT)
                    .show()

            } else if (text.isBlank() || text.length > 300) {
                Toast.makeText(
                    context,
                    getString(R.string.text_lenght_warning, MIN_REVIEW_LENGTH, MAX_REVIEW_LENGTH),
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                listener?.onSave(text, numberOfStars + 1)
                dismiss()
            }
        }
    }

    private fun setStarRating(stars: ArrayList<ImageView>, numberOfStars: Int) {
        val totalNumberOfStars = stars.size
        check(totalNumberOfStars >= numberOfStars) { "Rating is higher than there are stars" }

        for (i in 0..numberOfStars) {
            stars[i].setImageResource(R.drawable.star_gold)
        }

        for (i in numberOfStars + 1 until totalNumberOfStars) {
            stars[i].setImageResource(R.drawable.star_grey)
        }
    }

    fun disableStarListeners() {
        if (::stars.isInitialized) {
            for (star in stars) {
                star.setOnClickListener(null)
            }
        }
    }

    fun setOnViewCreatedListener(listener: OnViewCreatedListener) {
        onViewCreatedListener = listener
    }

    fun setRating(rating: Int) {
        if (!::stars.isInitialized) {
            stars = arrayListOf(img_star1, img_star2, img_star3, img_star4, img_star5)

        }
        setStarRating(stars, rating - 1)
    }

    interface DialogListener {
        fun onSave(text: String, numberOfStars: Int)
    }

    interface OnViewCreatedListener {
        fun onViewCreated(view: View)
    }
}