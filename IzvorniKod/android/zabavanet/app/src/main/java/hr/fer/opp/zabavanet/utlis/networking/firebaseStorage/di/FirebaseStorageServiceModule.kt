package hr.fer.opp.zabavanet.utlis.networking.firebaseStorage.di

import com.google.firebase.storage.FirebaseStorage
import dagger.Module
import dagger.Provides
import hr.fer.opp.zabavanet.app.di.diScope.PerApplication
import hr.fer.opp.zabavanet.utlis.networking.firebaseStorage.FirebaseStorageService

@Module
class FirebaseStorageServiceModule {

    @Provides
    @PerApplication
    fun provideFirebaseStorageService(): FirebaseStorageService {
        return FirebaseStorageService(FirebaseStorage.getInstance())
    }

}