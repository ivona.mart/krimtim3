package hr.fer.opp.zabavanet.viewModel.payment

import android.os.Handler
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.firebase.Timestamp
import hr.fer.opp.zabavanet.app.di.ApplicationComponent
import hr.fer.opp.zabavanet.base.BaseViewModel
import hr.fer.opp.zabavanet.model.Organization
import hr.fer.opp.zabavanet.model.User
import hr.fer.opp.zabavanet.utlis.networking.firestore.FirestoreService
import hr.fer.opp.zabavanet.utlis.session.Session
import javax.inject.Inject

class PaymentActivityViewModel : BaseViewModel() {

    @Inject
    lateinit var firestoreService: FirestoreService

    @Inject
    lateinit var session: Session

    override fun injectToAppComponent(applicationComponent: ApplicationComponent) {
        applicationComponent.plusPayment().inject(this)
    }

    fun getMonthlyMembership(): LiveData<List<String>> {
        return firestoreService.getMonthlyMembership(errors)
    }

    fun executePayment(): LiveData<Boolean> {
        val mutable = MutableLiveData<Boolean>()

        Handler().postDelayed(object : Runnable {
            override fun run() {
                mutable.postValue((Timestamp.now().seconds % 2) == 0L)
            }
        }, 2000)

        return mutable
    }

    fun updateUserRole(user: User): LiveData<Unit> {
        return firestoreService.updateUserRole(user, errors)
    }

    fun getOrganization(user: User): LiveData<List<Organization>> {
        return firestoreService.getOrganization(user, errors)
    }

    fun saveUserToPreferences(user: User) {
        session.user = user
    }
}