package hr.fer.opp.zabavanet.viewModel.eventDetails

import android.net.Uri
import androidx.lifecycle.LiveData
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.storage.StorageReference
import hr.fer.opp.zabavanet.app.di.ApplicationComponent
import hr.fer.opp.zabavanet.base.BaseViewModel
import hr.fer.opp.zabavanet.model.*
import hr.fer.opp.zabavanet.utlis.networking.firebaseStorage.FirebaseStorageService
import hr.fer.opp.zabavanet.utlis.networking.firestore.FirestoreService
import hr.fer.opp.zabavanet.utlis.session.Session
import javax.inject.Inject

class EventDetailsActivityViewModel : BaseViewModel() {

    @Inject
    lateinit var storage: FirebaseStorageService

    @Inject
    lateinit var firestoreService: FirestoreService

    @Inject
    lateinit var session: Session

    override fun injectToAppComponent(applicationComponent: ApplicationComponent) {
        applicationComponent.plusEventDetails().inject(this)
    }

    fun getImageReferences(subfolder: String): LiveData<List<StorageReference>> {
        return storage.getImageReferences(subfolder, errors)
    }

    fun getVideoReferences(subfolder: String): LiveData<List<StorageReference>> {
        return storage.getVideoReferences(subfolder, errors)
    }

    fun getVideoUri(storageReference: StorageReference): LiveData<Uri> {
        return storage.getUri(storageReference, errors)
    }

    fun getReviews(documentReference: DocumentReference): LiveData<List<Review>> {
        return firestoreService.getReviews(documentReference, errors)
    }

    fun updateAttendance(
        event: Event,
        userRef: DocumentReference,
        attendance: Attendance
    ): LiveData<Unit> {

        return firestoreService.updateAttendance(event, errors)
    }

    fun getCurrentUserReference(): DocumentReference {
        return firestoreService.getDocumentReference(
            FirestoreService.USERS_COLLECTION,
            session.user!!.email!!
        )
    }

    fun createReviewInDatabase(event: Event, review: Review): LiveData<DocumentReference> {
        return firestoreService.createReviewInDatabase(event, review, errors)
    }

    fun getUserFromDatabase(documentReference: DocumentReference): LiveData<User> {
        return firestoreService.getUserFromDatabase(documentReference, errors)
    }

    fun getUserFromPreferences(): User {
        return session.user!!
    }

    fun getOrganization(userRef: DocumentReference): LiveData<List<Organization>> {
        return firestoreService.getOrganization(userRef, errors)
    }

    fun getUserRef(userId: String): DocumentReference {
        return firestoreService.getDocumentReference(
            FirestoreService.USERS_COLLECTION,
            userId
        )
    }

    fun getUsersFromDatabase(userRefs: List<DocumentReference>): LiveData<List<User>> {
        return firestoreService.getUsers(userRefs, errors)
    }

    fun getEvent(event: Event): LiveData<Event> {
        return firestoreService.getEvent(event.documentReference.id, errors)
    }
}