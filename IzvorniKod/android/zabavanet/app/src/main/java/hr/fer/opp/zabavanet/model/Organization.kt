package hr.fer.opp.zabavanet.model

import java.io.Serializable
import java.util.*

data class Organization(
    var organizationName: String?,
    var webPage: String?,
    var address: String?
) : Serializable {

    companion object {
        const val ORGANIZATION_NAME = "organizationName"
        const val WEB_PAGE = "webPage"
        const val ADDRESS = "address"
    }

    constructor() : this(null, null, null)

    fun asHashMap(): HashMap<String, Any?> {
        return hashMapOf(
            ORGANIZATION_NAME to organizationName,
            WEB_PAGE to webPage,
            ADDRESS to address
        )
    }

}