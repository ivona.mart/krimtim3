package hr.fer.opp.zabavanet.utlis.performanceChecker

import android.app.ActivityManager
import javax.inject.Inject

class PerformanceChecker @Inject constructor(activityManager: ActivityManager) {

    companion object {
        private const val OPTIMUM_CORE = 4
        private const val OPTIMUM_MEMORY_MB = 124
    }

    val isHighPerformingDevice =
        !activityManager.isLowRamDevice &&
                Runtime.getRuntime().availableProcessors() >= OPTIMUM_CORE &&
                activityManager.memoryClass >= OPTIMUM_MEMORY_MB

}