package hr.fer.opp.zabavanet.utlis.mediaUploaderService

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.IBinder
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.google.firebase.storage.FirebaseStorage
import hr.fer.opp.zabavanet.R
import hr.fer.opp.zabavanet.model.MediaModel
import hr.fer.opp.zabavanet.utlis.networking.firebaseStorage.FirebaseStorageService
import hr.fer.opp.zabavanet.view.home.HomeActivity

class MediaUploaderService : Service() {

    companion object {
        const val NOTIF_ID = 1
        const val NOTIF_CHANNEL_ID = "background_service_notification"
        const val NOTIF_CHANNEL_NAME = "Background upload service"
        const val INTENT_MEDIA_MODELS = "media_models"
        const val INTENT_SUBFOLDER = "subfolder"
        const val TAG = "Zabava.net LPS"
    }

    private lateinit var builder: NotificationCompat.Builder
    private lateinit var storage: FirebaseStorageService
    private var mediaModels: ArrayList<MediaModel>? = null
    private var i = 0

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        mediaModels = intent?.getSerializableExtra(INTENT_MEDIA_MODELS) as ArrayList<MediaModel>
        if (mediaModels == null || mediaModels!!.isEmpty()) {
            logD("Nothing to upload, stopping")
            stopSelf()
        }

        val subfolder = intent.getStringExtra(INTENT_SUBFOLDER)

        mediaModels?.let {
            startForeground()
            startUpload(it[0], subfolder!!)
        } ?: notifyFailed()

        return super.onStartCommand(intent, flags, startId)
    }

    override fun onCreate() {
        super.onCreate()

        storage = FirebaseStorageService(FirebaseStorage.getInstance())
        createBackgroundNotifChannel()
    }

    private fun startForeground() {
        val notifIntent = Intent(this, HomeActivity::class.java)
        val pendingIntent = PendingIntent.getActivity(this, 0, notifIntent, 0)

        builder = NotificationCompat.Builder(this, NOTIF_CHANNEL_ID)
            .setOngoing(true)
            .setSmallIcon(R.drawable.ic_confetti)
            .setContentTitle(getString(R.string.uploading_media))
            .setContentIntent(pendingIntent)
            .setCategory(NotificationCompat.CATEGORY_PROGRESS)
            .setOnlyAlertOnce(true)
            .setProgress(100, 0, false)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForeground(NOTIF_ID, builder.build())
        } else {
            builder.priority = NotificationCompat.PRIORITY_HIGH
            startForeground(NOTIF_ID, builder.build())
        }

    }

    private fun startUpload(mediaModel: MediaModel, subfolder: String?) {
        logD("Upload started")
        storage.uploadMedia(mediaModel, subfolder)
            .addOnSuccessListener {
                i++
                if (i < mediaModels!!.size) {
                    startUpload(mediaModels!![i], subfolder)
                } else {
                    notifyDone()
                }
            }.addOnProgressListener {
                updateProgress(i, mediaModels!!.size, it.bytesTransferred, it.totalByteCount)
            }
            .addOnFailureListener {
                notifyFailed()
            }
    }

    private fun updateProgress(currentIndex: Int, maxIndex: Int, current: Long, max: Long) {
        NotificationManagerCompat.from(this).apply {
            builder.setContentText(getString(R.string.uploading_n_m, currentIndex + 1, maxIndex))
            builder.setProgress(max.toInt(), current.toInt(), false)
            notify(NOTIF_ID, builder.build())
        }
    }

    private fun notifyDone() {
        logD("Upload complete")

        NotificationManagerCompat.from(this).apply {
            builder.setContentTitle(getString(R.string.app_name))
            builder.setContentText(getString(R.string.upload_complete))
            builder.setProgress(0, 0, false)
            builder.setOngoing(false)
            builder.setOnlyAlertOnce(false)
            notify(NOTIF_ID, builder.build())
            stopForeground(false)
        }
    }

    private fun notifyFailed() {
        logD("Upload failed")

        NotificationManagerCompat.from(this).apply {
            builder.setContentTitle(getString(R.string.app_name))
            builder.setContentText(getString(R.string.upload_failed))
            builder.setProgress(0, 0, false)
            builder.setOngoing(false)
            builder.setOnlyAlertOnce(false)
            notify(NOTIF_ID, builder.build())
            stopForeground(false)
        }
    }

    private fun createBackgroundNotifChannel() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            val channelBackground = NotificationChannel(
                NOTIF_CHANNEL_ID,
                NOTIF_CHANNEL_NAME,
                NotificationManager.IMPORTANCE_HIGH
            ).apply {
                description = getString(R.string.background_service_description)
                enableLights(false)
                enableVibration(false)
            }

            val notificationManager: NotificationManager =
                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

            notificationManager.createNotificationChannel(channelBackground)
        }
    }

    private fun logD(message: String) {
        Log.d(TAG, message)
    }
}