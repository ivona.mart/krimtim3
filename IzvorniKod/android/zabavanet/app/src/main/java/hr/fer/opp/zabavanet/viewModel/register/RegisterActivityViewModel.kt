package hr.fer.opp.zabavanet.viewModel.register

import androidx.lifecycle.LiveData
import com.google.firebase.auth.AuthResult
import hr.fer.opp.zabavanet.app.di.ApplicationComponent
import hr.fer.opp.zabavanet.base.BaseViewModel
import hr.fer.opp.zabavanet.utlis.networking.firebaseAuth.FirebaseAuthService
import javax.inject.Inject

class RegisterActivityViewModel : BaseViewModel() {

    @Inject
    lateinit var firebaseAuthService: FirebaseAuthService

    override fun injectToAppComponent(applicationComponent: ApplicationComponent) {
        applicationComponent.plusRegister().inject(this)
    }

    fun createUser(email: String, password: String): LiveData<AuthResult> {
        return firebaseAuthService.createUserWithEmailAndPassword(email, password, errors)
    }

    fun sendVerificationEmail() {
        val firebaseUser = firebaseAuthService.getCurrentUser()
        firebaseUser?.sendEmailVerification()
    }

    fun isEmailVerified(): Boolean {
        return firebaseAuthService.getCurrentUser()!!.isEmailVerified
    }
}