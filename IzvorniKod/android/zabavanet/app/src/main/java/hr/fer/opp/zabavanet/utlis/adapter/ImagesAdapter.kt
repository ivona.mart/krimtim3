package hr.fer.opp.zabavanet.utlis.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import hr.fer.opp.zabavanet.R
import hr.fer.opp.zabavanet.model.MediaModel
import hr.fer.opp.zabavanet.utlis.glide.GlideRequests
import kotlinx.android.synthetic.main.item_image.view.*

class ImagesAdapter(
    var data: List<MediaModel>,
    glideRequests: GlideRequests,
    private val listener: OnLongClickListener
) : RecyclerView.Adapter<ImagesAdapter.ListViewHolder>() {

    private val requestBuilder = glideRequests.asDrawable().fitCenter()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_image, parent, false)

        return ListViewHolder(view); }


    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {

        val current = data[position]

        requestBuilder.clone().load(current.uri).into(holder.itemView.image)

        holder.itemView.setOnLongClickListener {
            listener.onLongClick(current, position)
            true
        }

    }

    override fun getItemCount(): Int {
        return data.size
    }

    class ListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    interface OnLongClickListener {
        fun onLongClick(item: MediaModel, position: Int)
    }

    class LinearLayoutManagerWrapper(context: Context, orientation: Int, reverseLayout: Boolean) :
        LinearLayoutManager(context, orientation, reverseLayout) {

        override fun onLayoutChildren(
            recycler: RecyclerView.Recycler?,
            state: RecyclerView.State?
        ) {
            try {
                super.onLayoutChildren(recycler, state)
            } catch (exc: IndexOutOfBoundsException) {
                Log.e("Zabava.net", "Recycler view bug")
            }
        }
    }
}