package hr.fer.opp.zabavanet.utlis.dialogManager

import android.view.View
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentManager
import hr.fer.opp.zabavanet.R
import hr.fer.opp.zabavanet.model.Review
import hr.fer.opp.zabavanet.utlis.extensionFunctions.toPattern
import hr.fer.opp.zabavanet.view.addReview.AddReviewDialog
import hr.fer.opp.zabavanet.view.confirmationDialog.ConfirmationDialogFragment
import hr.fer.opp.zabavanet.view.datePicker.DatePickerFragment
import hr.fer.opp.zabavanet.view.timePicker.TimePickerFragment
import kotlinx.android.synthetic.main.dialog_add_review.view.*

class DialogManager(private val fragmentManager: FragmentManager) {

    private var timePickerFragment: TimePickerFragment? = null
    private var datePickerFragment: DatePickerFragment? = null
    private var confirmationDialog: ConfirmationDialogFragment? = null
    private var addReviewDialog: AddReviewDialog? = null

    fun openTimePickerDialog(
        hour: Int?,
        minute: Int?,
        timePickerListener: TimePickerFragment.TimePickerListener
    ) {
        timePickerFragment = TimePickerFragment(hour, minute)
        timePickerFragment?.addListener(timePickerListener)
        timePickerFragment?.show(fragmentManager, "Time picker fragment")
    }

    fun openDatePickerDialog(datePickerListener: DatePickerFragment.DatePickerListener) {
        datePickerFragment = DatePickerFragment()
        datePickerFragment?.addListener(datePickerListener)
        datePickerFragment?.show(fragmentManager, "Date picker fragment")
    }

    fun openConfirmationDialog(
        prompt: String,
        positive: String,
        negative: String,
        listener: ConfirmationDialogFragment.DialogListener
    ) {
        confirmationDialog = ConfirmationDialogFragment(prompt, positive, negative, listener)
        confirmationDialog?.show(fragmentManager, "Confirmation dialog")
    }

    fun openAddReviewDialog(listener: AddReviewDialog.DialogListener) {
        addReviewDialog = AddReviewDialog(listener)
        addReviewDialog?.show(fragmentManager, "Add review dialog")
    }

    fun openSeeReviewDialog(review: Review) {
        addReviewDialog = AddReviewDialog(null)


        addReviewDialog?.setOnViewCreatedListener(object : AddReviewDialog.OnViewCreatedListener {
            override fun onViewCreated(view: View) {
                addReviewDialog?.view?.apply {

                    text_review_dialog_title.text =
                        context.getString(
                            R.string.someones_review_at_timestamp,
                            review.user.username,
                            review.createdAt?.toPattern(null)
                        )

                    anchor_text_your_review.text =
                        context.getString(
                            R.string.someones_review,
                            review.user.username
                        )

                    text_your_rating.text =
                        context.getString(
                            R.string.someones_rating,
                            review.user.username
                        )

                    btn_save.visibility = View.GONE

                    text_review_text.setText(review.text)
                    text_review_text.setTextColor(
                        ContextCompat.getColor(
                            context,
                            R.color.text_color
                        )
                    )
                    text_review_text.setBackgroundColor(
                        ContextCompat.getColor(
                            context,
                            android.R.color.transparent
                        )
                    )
                    text_review_text.isEnabled = false

                    addReviewDialog?.disableStarListeners()

                    addReviewDialog?.setRating(review.rating!!)
                }
            }
        })

        addReviewDialog?.show(fragmentManager, "See review dialog")
    }
}