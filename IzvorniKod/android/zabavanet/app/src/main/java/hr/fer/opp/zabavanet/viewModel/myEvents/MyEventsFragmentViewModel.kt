package hr.fer.opp.zabavanet.viewModel.myEvents

import androidx.lifecycle.LiveData
import com.google.firebase.firestore.DocumentReference
import hr.fer.opp.zabavanet.app.di.ApplicationComponent
import hr.fer.opp.zabavanet.base.BaseViewModel
import hr.fer.opp.zabavanet.model.Event
import hr.fer.opp.zabavanet.model.User
import hr.fer.opp.zabavanet.utlis.networking.firestore.FirestoreService
import javax.inject.Inject

class MyEventsFragmentViewModel : BaseViewModel() {

    @Inject
    lateinit var firestoreService: FirestoreService

    override fun injectToAppComponent(applicationComponent: ApplicationComponent) {
        applicationComponent.plusMyEvents().inject(this)
    }

    fun getOrganizersEvents(user: User): LiveData<List<Event>> {
        return firestoreService.getOrganizersEvents(user.email!!, null, errors)
    }

    fun getUserGoingEvents(user: User): LiveData<List<Event>> {
        return firestoreService.getUserGoingEvents(user.email!!, errors)
    }

    fun getUserReference(user: User): DocumentReference {
        return firestoreService.getDocumentReference(
            FirestoreService.USERS_COLLECTION,
            user.email!!
        )
    }

    fun deleteEvent(event: Event): LiveData<Unit> {
        return firestoreService.deleteEvent(event.documentReference, errors)
    }
}