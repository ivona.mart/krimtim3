package hr.fer.opp.zabavanet.viewModel.splash.di

import dagger.Subcomponent
import hr.fer.opp.zabavanet.viewModel.splash.SplashActivityViewModel

@Subcomponent
interface SplashActivityViewModelComponent {

    fun inject(splashActivityViewModel: SplashActivityViewModel)
}