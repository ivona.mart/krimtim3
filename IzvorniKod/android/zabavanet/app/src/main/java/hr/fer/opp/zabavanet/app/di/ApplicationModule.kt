package hr.fer.opp.zabavanet.app.di

import android.app.ActivityManager
import android.content.Context
import android.content.res.Resources
import dagger.Module
import dagger.Provides
import hr.fer.opp.zabavanet.app.App
import hr.fer.opp.zabavanet.app.di.diScope.PerApplication

@Module
class ApplicationModule(private val app: App) {

    @PerApplication
    @Provides
    fun providesApplication(): App = app

    @PerApplication
    @Provides
    fun provideApplicationContext(): Context = app

    @PerApplication
    @Provides
    fun resources(): Resources = app.resources

    @PerApplication
    @Provides
    fun provideActivityManager(context: Context): ActivityManager {
        return context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
    }
}