package hr.fer.opp.zabavanet.viewModel.splash

import androidx.lifecycle.LiveData
import com.google.firebase.auth.FirebaseUser
import hr.fer.opp.zabavanet.app.di.ApplicationComponent
import hr.fer.opp.zabavanet.base.BaseViewModel
import hr.fer.opp.zabavanet.model.User
import hr.fer.opp.zabavanet.utlis.networking.firebaseAuth.FirebaseAuthService
import hr.fer.opp.zabavanet.utlis.networking.firestore.FirestoreService
import hr.fer.opp.zabavanet.utlis.session.Session
import javax.inject.Inject

class SplashActivityViewModel : BaseViewModel() {

    @Inject
    lateinit var session: Session

    @Inject
    lateinit var firebaseAuthService: FirebaseAuthService

    @Inject
    lateinit var firestoreService: FirestoreService

    override fun injectToAppComponent(applicationComponent: ApplicationComponent) {
        applicationComponent.plusSplash().inject(this)
    }

    fun getCurrentUser(): FirebaseUser? {
        return firebaseAuthService.getCurrentUser()
    }

    fun getUserFromDb(email: String): LiveData<User> {
        return firestoreService.getUserFromDatabase(email, errors)
    }

    fun saveUserToPreferences(user: User) {
        session.user = user
    }

    fun updateUserRole(user: User): LiveData<Unit> {
        saveUserToPreferences(user)
        return firestoreService.updateUserRole(user, errors)
    }
}