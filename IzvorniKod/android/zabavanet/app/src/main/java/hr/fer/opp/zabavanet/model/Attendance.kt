package hr.fer.opp.zabavanet.model

enum class Attendance(val text: String) {
    GOING("Going"),
    MAYBE_GOING("Maybe"),
    NOT_GOING("Not going")
}