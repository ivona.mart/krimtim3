package hr.fer.opp.zabavanet.utlis.networking.firestore

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.firebase.Timestamp
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QuerySnapshot
import com.google.firebase.iid.FirebaseInstanceId
import hr.fer.opp.zabavanet.model.*
import hr.fer.opp.zabavanet.utlis.extensionFunctions.*
import hr.fer.opp.zabavanet.view.allEvents.AllEventsFragment

class FirestoreService(private val firestore: FirebaseFirestore) {

    companion object {
        const val USERS_COLLECTION = "users"
        const val EVENTS_COLLECTION = "events"
        const val REVIEWS_COLLECTION = "reviews"
        const val CONSTANTS_COLLECTION = "constants"
        const val ORGANIZATIONS_COLLECTION = "organizations"
        const val NOTIFICATIONS_COLLECTION = "notifications"
        private const val TAG = "Zabava.net Firestore"
    }

    fun updateBasicUserInfo(
        user: User,
        errors: MutableLiveData<String>
    ): LiveData<Any> {
        val mutable = MutableLiveData<Any>()

        val document = firestore.collection(USERS_COLLECTION).document(user.email!!)

        document.update(user.asHashMap())
            .addOnSuccessListener {
                mutable.postValue(it)
            }
            .addOnFailureListener {
                errors.postValue(it.message)
            }

        return mutable
    }

    fun updateUserRole(user: User, errors: MutableLiveData<String>): LiveData<Unit> {
        val mutable = MutableLiveData<Unit>()

        firestore.collection(USERS_COLLECTION).document(user.email!!).update(user.roleMap())
            .addOnSuccessListener {
                mutable.postValue(null)
            }
            .addOnFailureListener {
                errors.postValue(it.message)
            }

        return mutable
    }

    fun createUserInDatabase(user: User, errors: MutableLiveData<String>): LiveData<Unit> {
        val mutable = MutableLiveData<Unit>()

        firestore.collection(USERS_COLLECTION)
            .document(user.email!!)
            .set(user.asHashMap())
            .addOnSuccessListener {
                mutable.postValue(null)
            }
            .addOnFailureListener {
                errors.postValue(it.message)
            }

        return mutable
    }

    fun getUserFromDatabase(
        email: String?,
        errors: MutableLiveData<String>
    ): LiveData<User> {
        val mutable = MutableLiveData<User>()

        if (email != null) {
            getDocumentReference(USERS_COLLECTION, email).get()
                .addOnSuccessListener { documentSnapshot ->
                    getUserFromDocumentSnapshot(documentSnapshot, mutable, errors)
                }
                .addOnFailureListener { task ->
                    errors.postValue(task.message.toString())
                }
        } else {
            mutable.postValue(null)
        }

        return mutable
    }

    fun getUserFromDatabase(
        documentReference: DocumentReference,
        errors: MutableLiveData<String>
    ): LiveData<User> {
        val mutable = MutableLiveData<User>()

        documentReference.get()
            .addOnSuccessListener {
                getUserFromDocumentSnapshot(it, mutable, errors)
            }
            .addOnFailureListener {
                errors.postValue(it.message)
            }

        return mutable
    }

    private fun getUserFromDocumentSnapshot(
        documentSnapshot: DocumentSnapshot,
        mutable: MutableLiveData<User>,
        errors: MutableLiveData<String>
    ) {
        val user = getOnlyUserFromDocumentSnapshot(documentSnapshot)
        documentSnapshot.reference.collection(ORGANIZATIONS_COLLECTION).get()
            .addOnSuccessListener { qs ->
                if (qs.documents.isNotEmpty()) {
                    user?.organizationId = qs.documents.first().id
                }
                mutable.postValue(user)
            }
            .addOnFailureListener { exc ->
                errors.postValue(exc.message)
            }
    }

    fun getAllEvents(errors: MutableLiveData<String>): LiveData<List<Event>> {
        val mutable = MutableLiveData<List<Event>>()

        firestore.collection(EVENTS_COLLECTION).get()
            .addOnSuccessListener {
                val events = getEventsFromQuerySnapshot(it)

                for (i in events.indices) {
                    events[i].documentReference = it.documents[i].reference
                }
                mutable.postValue(events)
            }
            .addOnFailureListener {
                errors.postValue(it.message)
            }

        return mutable
    }

    fun getReviews(
        documentReference: DocumentReference,
        errors: MutableLiveData<String>
    ): LiveData<List<Review>> {
        val mutable = MutableLiveData<List<Review>>()

        documentReference.collection(REVIEWS_COLLECTION).get()
            .addOnSuccessListener {
                val reviews = it.toObjects(Review::class.java)
                mutable.postValue(reviews)
            }
            .addOnFailureListener {
                errors.postValue(it.message)
            }

        return mutable
    }

    fun getOrganization(
        user: User,
        errors: MutableLiveData<String>
    ): LiveData<List<Organization>> {
        return getOrganization(getDocumentReference(USERS_COLLECTION, user.email!!), errors)
    }

    fun getOrganization(
        userRef: DocumentReference,
        errors: MutableLiveData<String>
    ): LiveData<List<Organization>> {
        val mutable = MutableLiveData<List<Organization>>()

        userRef.collection(ORGANIZATIONS_COLLECTION).get()
            .addOnSuccessListener {
                val organizations = it.toObjects(Organization::class.java)
                mutable.postValue(organizations)
            }
            .addOnFailureListener {
                errors.postValue(it.message)
            }

        return mutable
    }

    fun createEventInDatabase(
        event: Event,
        errors: MutableLiveData<String>
    ): LiveData<DocumentReference> {
        val mutable = MutableLiveData<DocumentReference>()

        firestore.collection(EVENTS_COLLECTION).add(event.asHashMap())
            .addOnSuccessListener {
                mutable.postValue(it)
            }.addOnFailureListener {
                errors.postValue(it.message)
            }

        return mutable
    }

    fun getDocumentReference(collection: String, documentId: String): DocumentReference {
        return firestore.collection(collection).document(documentId)
    }

    fun updateAttendance(event: Event, errors: MutableLiveData<String>): LiveData<Unit> {
        val mutable = MutableLiveData<Unit>()

        event.documentReference.update(event.attendanceAsMap())
            .addOnSuccessListener {
                mutable.postValue(null)
            }
            .addOnFailureListener {
                errors.postValue(it.message)
            }

        return mutable
    }

    private fun getConstants(
        documentId: String,
        itemName: String,
        errors: MutableLiveData<String>
    ): LiveData<List<String>> {
        val mutable = MutableLiveData<List<String>>()

        firestore.collection(CONSTANTS_COLLECTION).document(documentId).get()
            .addOnSuccessListener {
                val list = it.data!![itemName] as ArrayList<String>
                mutable.postValue(list)
            }
            .addOnFailureListener {
                errors.postValue(it.message)
            }


        return mutable
    }

    fun getCities(errors: MutableLiveData<String>): LiveData<List<String>> {
        return getConstants("cities", "croatia", errors)
    }

    fun getEventTypes(errors: MutableLiveData<String>): LiveData<List<String>> {
        return getConstants("eventTypes", "types", errors)
    }

    fun getMonthlyMembership(errors: MutableLiveData<String>): LiveData<List<String>> {
        return getConstants("prices", "monthlyMembership", errors)
    }

    fun getFilteredEvents(
        filter: AllEventsFragment.Filter,
        errors: MutableLiveData<String>
    ): LiveData<List<Event>> {
        val mutable = MutableLiveData<List<Event>>()

        firestore.collection(EVENTS_COLLECTION)
            .queryLocation(filter.city)
            .queryEventType(filter.eventType)
            .queryTimeAfter(filter.timeAfter)
            .queryTimeBefore(filter.timeBefore)
            .get()
            .addOnSuccessListener {
                mutable.postValue(getEventsFromQuerySnapshot(it))
            }
            .addOnFailureListener {
                Log.d("Zabava.net", it.message.toString())
                errors.postValue(it.message)
            }

        return mutable
    }

    fun createReviewInDatabase(
        event: Event,
        review: Review,
        errors: MutableLiveData<String>
    ): LiveData<DocumentReference> {
        val mutable = MutableLiveData<DocumentReference>()

        event.documentReference.collection(REVIEWS_COLLECTION).add(review.asMap())
            .addOnSuccessListener {
                mutable.postValue(it)
            }
            .addOnFailureListener {
                errors.postValue(it.message)
            }

        return mutable
    }

    fun getOrganizationReference(user: User): DocumentReference {
        return getDocumentReference(USERS_COLLECTION, user.email!!)
            .collection(ORGANIZATIONS_COLLECTION)
            .document(user.organizationId!!)
    }

    fun createOrganizationInDatabase(
        user: User,
        organization: Organization,
        errors: MutableLiveData<String>
    ): LiveData<DocumentReference> {
        val mutable = MutableLiveData<DocumentReference>()

        getDocumentReference(USERS_COLLECTION, user.email!!)
            .collection(ORGANIZATIONS_COLLECTION).add(organization.asHashMap())
            .addOnSuccessListener {
                mutable.postValue(it)
            }
            .addOnFailureListener {
                errors.postValue(it.message)
            }

        return mutable
    }

    fun updateOrganizationInfo(
        user: User,
        organization: Organization,
        errors: MutableLiveData<String>
    ): LiveData<Unit> {
        val mutable = MutableLiveData<Unit>()

        getOrganizationReference(user)
            .update(organization.asHashMap())
            .addOnSuccessListener {
                mutable.postValue(null)
            }
            .addOnFailureListener {
                errors.postValue(it.message)
            }

        return mutable
    }

    fun getOrganizersEvents(
        documentId: String,
        since: Timestamp?,
        errors: MutableLiveData<String>
    ): LiveData<List<Event>> {
        val mutable = MutableLiveData<List<Event>>()

        firestore.collection(EVENTS_COLLECTION).queryOrganizersEvents(
            getDocumentReference(
                USERS_COLLECTION, documentId
            )
        ).queryTimeAfter(since)
            .get()
            .addOnSuccessListener {
                mutable.postValue(getEventsFromQuerySnapshot(it))
            }
            .addOnFailureListener {
                errors.postValue(it.message)
            }

        return mutable
    }

    fun getUserGoingEvents(
        documentId: String,
        errors: MutableLiveData<String>
    ): LiveData<List<Event>> {
        val mutable = MutableLiveData<List<Event>>()

        firestore.collection(EVENTS_COLLECTION).queryUserGoingEvents(
            getDocumentReference(
                USERS_COLLECTION, documentId
            )
        ).get()
            .addOnSuccessListener {
                mutable.postValue(getEventsFromQuerySnapshot(it))
            }
            .addOnFailureListener {
                errors.postValue(it.message)
            }

        return mutable
    }

    private fun getEventsFromQuerySnapshot(querySnapshot: QuerySnapshot): List<Event> {
        val events = querySnapshot.toObjects(Event::class.java)
        for ((i, event) in events.withIndex()) {
            event.documentReference = querySnapshot.documents[i].reference
        }
        return events
    }

    private fun getEventFromDocumentSnapshot(documentSnapshot: DocumentSnapshot): Event? {
        val event = documentSnapshot.toObject(Event::class.java)
        event?.documentReference = documentSnapshot.reference
        return event
    }

    fun deleteEvent(
        documentReference: DocumentReference,
        errors: MutableLiveData<String>
    ): LiveData<Unit> {
        val mutable = MutableLiveData<Unit>()

        documentReference.delete()
            .addOnSuccessListener {
                mutable.postValue(null)
            }.addOnFailureListener {
                errors.postValue(it.message)
            }

        return mutable
    }

    fun updateEvent(event: Event, errors: MutableLiveData<String>): LiveData<Unit> {
        val mutable = MutableLiveData<Unit>()

        event.documentReference.update(event.asHashMap())
            .addOnSuccessListener {
                mutable.postValue(null)
            }
            .addOnFailureListener {
                errors.postValue(it.message)
            }

        return mutable
    }

    fun updateDeviceToken(userId: String, deviceToken: String): LiveData<Unit> {
        val mutable = MutableLiveData<Unit>()

        firestore.collection(USERS_COLLECTION)
            .document(userId)
            .update(mapOf("deviceToken" to deviceToken))
            .addOnSuccessListener {
                Log.d(TAG, "Updated $userId")
                mutable.postValue(null)
            }.addOnFailureListener {
                Log.d(TAG, it.message.toString())
            }

        return mutable
    }

    fun setDeviceToken(userId: String) {
        FirebaseInstanceId.getInstance().instanceId
            .addOnSuccessListener {
                updateDeviceToken(userId, it.token)
            }
    }

    fun addNotification(
        userId: String,
        notification: Notification,
        errors: MutableLiveData<String>
    ): LiveData<DocumentReference> {
        val mutable = MutableLiveData<DocumentReference>()

        firestore.collection(USERS_COLLECTION).document(userId).collection(NOTIFICATIONS_COLLECTION)
            .add(notification.asMap())
            .addOnSuccessListener {
                mutable.postValue(it)
            }
            .addOnFailureListener {
                errors.postValue(it.message)
            }

        return mutable
    }

    fun getEvent(eventId: String, errors: MutableLiveData<String>): LiveData<Event> {
        val mutable = MutableLiveData<Event>()

        firestore.collection(EVENTS_COLLECTION).document(eventId).get()
            .addOnSuccessListener {
                mutable.postValue(getEventFromDocumentSnapshot(it))
            }
            .addOnFailureListener {
                errors.postValue(it.message)
            }

        return mutable
    }

    private fun getNotificationsFromQuerySnapshot(querySnapshot: QuerySnapshot): List<Notification> {
        val notifications = querySnapshot.toObjects(Notification::class.java)
        for ((i, notif) in notifications.withIndex()) {
            notif.documentReference = querySnapshot.documents[i].reference
        }
        return notifications
    }

    fun getNotifications(
        userId: String,
        errors: MutableLiveData<String>
    ): LiveData<List<Notification>> {
        val mutable = MutableLiveData<List<Notification>>()

        firestore.collection(USERS_COLLECTION).document(userId).collection(NOTIFICATIONS_COLLECTION)
            .get()
            .addOnSuccessListener {
                mutable.postValue(getNotificationsFromQuerySnapshot(it))
            }
            .addOnFailureListener {
                errors.postValue(it.message)
            }

        return mutable
    }

    fun deleteNotification(
        documentReference: DocumentReference,
        errors: MutableLiveData<String>
    ): LiveData<Unit> {
        val mutable = MutableLiveData<Unit>()

        documentReference.delete()
            .addOnSuccessListener {
                mutable.postValue(null)
            }
            .addOnFailureListener {
                errors.postValue(it.message)
            }

        return mutable
    }

    private fun getOnlyUserFromDocumentSnapshot(documentSnapshot: DocumentSnapshot): User? {
        val user = documentSnapshot.toObject(User::class.java)
        user?.email = documentSnapshot.id
        user?.dateOfBirth = documentSnapshot[User.DATE_OF_BIRTH] as Timestamp?
        user?.membershipDuration = documentSnapshot[User.MEMEBERSHIP_DURATION] as Timestamp?
        return user
    }

    fun getUsers(
        list: List<DocumentReference>,
        errors: MutableLiveData<String>
    ): LiveData<List<User>> {
        val mutable = MutableLiveData<List<User>>()
        firestore.collection(USERS_COLLECTION).get()
            .addOnSuccessListener { querySnap ->
                val retVal = arrayListOf<User>()
                list.forEach { docRef ->
                    querySnap.documents.forEach { queryDocSnap ->
                        if (docRef.path.trim() == queryDocSnap.reference.path.trim()) {
                            getOnlyUserFromDocumentSnapshot(queryDocSnap)?.let { retVal.add(it) }
                        }
                    }
                }
                mutable.postValue(retVal)
            }.addOnFailureListener {
                errors.postValue(it.message)
            }
        return mutable
    }


}