package hr.fer.opp.zabavanet.viewModel.editOrganization.di

import dagger.Subcomponent
import hr.fer.opp.zabavanet.app.di.diScope.PerViewModel
import hr.fer.opp.zabavanet.viewModel.editOrganization.EditOrganizationActivityViewModel

@PerViewModel
@Subcomponent
interface EditOrganizationViewModelComponent {

    fun inject(editOrganizationActivityViewModel: EditOrganizationActivityViewModel)
}