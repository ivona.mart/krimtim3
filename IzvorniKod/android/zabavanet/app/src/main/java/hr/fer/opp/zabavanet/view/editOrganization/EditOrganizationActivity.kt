package hr.fer.opp.zabavanet.view.editOrganization

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.OpenableColumns
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import hr.fer.opp.zabavanet.R
import hr.fer.opp.zabavanet.base.BaseActivity
import hr.fer.opp.zabavanet.model.MediaModel
import hr.fer.opp.zabavanet.model.Organization
import hr.fer.opp.zabavanet.model.User
import hr.fer.opp.zabavanet.utlis.codes.ActionCode
import hr.fer.opp.zabavanet.utlis.codes.RequestCode
import hr.fer.opp.zabavanet.utlis.codes.ResultCode
import hr.fer.opp.zabavanet.utlis.glide.GlideApp
import hr.fer.opp.zabavanet.view.organizationProfile.OrganizationProfileActivity
import hr.fer.opp.zabavanet.view.payment.PaymentActivity
import hr.fer.opp.zabavanet.viewModel.editOrganization.EditOrganizationActivityViewModel
import kotlinx.android.synthetic.main.activity_edit_organization.*

class EditOrganizationActivity : BaseActivity<EditOrganizationActivityViewModel>() {

    companion object {
        const val INTENT_USER = "user"
        const val INTENT_ORGANIZATION = "organization"
        const val MAX_IMAGE_SIZE = 5 * 1024 * 1024 // in bytes

        private const val MAX_CHARACTER_COUNT_ORGANIZATION_NAME = 50
        private const val MAX_CHARACTER_COUNT_WEB_PAGE = 200
        private const val MAX_CHARACTER_COUNT_ADDRESS = 100
    }

    private lateinit var user: User
    private lateinit var imageModel: MediaModel
    private lateinit var organization: Organization
    private lateinit var action: String
    private var isImageUpdated = false

    private val IMAGE_TYPE = "image/*"


    override fun getViewModel(): EditOrganizationActivityViewModel {
        return ViewModelProviders.of(this).get(EditOrganizationActivityViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_organization)

        user = intent.getSerializableExtra(INTENT_USER) as User

        check(intent.action != null) { "Intent action must be set!" }

        action = intent.action!!

        when (action) {
            ActionCode.CREATE -> {
                initUI(action)
            }
            ActionCode.EDIT -> {
                showLoading()
                viewModel.getOrganizationPictureRef(user).observe(this, Observer {
                    if (it.isNotEmpty()) {
                        viewModel.getUri(it.first()).observe(this, Observer { uri ->
                            imageModel = MediaModel(uri.toString(), MediaModel.MediaType.IMAGE)
                            Glide.with(this).load(imageModel).into(image_organization_picture)
                        })
                    }
                    hideLoading()
                    initUI(action)
                })
                organization = intent.getSerializableExtra(INTENT_ORGANIZATION) as Organization
            }
        }
    }

    private fun initUI(action: String) {

        setListeners(action)

        when (action) {
            ActionCode.EDIT -> {
                organization.organizationName?.let { text_organization_name.setText(it) }
                organization.webPage?.let { text_web_page.setText(it) }
                organization.address?.let { text_address.setText(it) }
            }

            ActionCode.CREATE -> {

            }
        }


    }

    private fun setListeners(action: String) {

        btn_edit_organization_picture.setOnClickListener {
            openImagePicker()
        }

        btn_save.setOnClickListener {

            if (text_organization_name.text.isBlank()) {
                showMessage("Organization name can't be empty!")
                return@setOnClickListener
            }
            if (text_organization_name.text.length > MAX_CHARACTER_COUNT_ORGANIZATION_NAME) {
                showMessage("Organization name can't be longer than " + MAX_CHARACTER_COUNT_ORGANIZATION_NAME + " characters!")
                return@setOnClickListener
            }
            if (text_web_page.text.isBlank()) {
                showMessage("Web page can't be empty!")
                return@setOnClickListener
            }
            if (text_web_page.text.length > MAX_CHARACTER_COUNT_WEB_PAGE) {
                showMessage("Web page can't be longer than " + MAX_CHARACTER_COUNT_WEB_PAGE + " characters!")
                return@setOnClickListener
            }
            if (text_address.text.isBlank()) {
                showMessage("Address can't be empty!")
                return@setOnClickListener
            }
            if (text_address.text.length > MAX_CHARACTER_COUNT_ADDRESS) {
                showMessage("Address can't be longer than " + MAX_CHARACTER_COUNT_ADDRESS + " characters!")
                return@setOnClickListener
            }
            if (!::imageModel.isInitialized) {
                showMessage("Must set picture")
                return@setOnClickListener
            }

            val name: String = text_organization_name.text.toString()
            val webPage = text_web_page.text.toString()
            val address = text_address.text.toString()


            when (action) {
                ActionCode.EDIT -> {
                    organization.apply {
                        this.organizationName = name
                        this.webPage = webPage
                        this.address = address
                    }

                    showLoading()
                    viewModel.updateOrganizationInfo(user, organization)
                        .observe(this, Observer {
                            if (isImageUpdated) {
                                viewModel.deleteOrganizationPicture(user.organizationId!!)
                                    .observe(this, Observer {
                                        viewModel.uploadOrganizationPicture(
                                            imageModel,
                                            user.organizationId!!
                                        ).observe(this, Observer {
                                            finishAct(user)
                                        })
                                    })
                            } else {
                                finishAct(user)
                            }
                        })
                }

                ActionCode.CREATE -> {
                    organization = Organization(name, webPage, address)
                    showLoading()
                    viewModel.createOrganizationInDatabase(user, organization)
                        .observe(this, Observer {
                            user.organizationId = it.id
                            viewModel.saveUserToPreferences(user)
                            viewModel.uploadOrganizationPicture(
                                imageModel,
                                user.organizationId!!
                            ).observe(this, Observer {
                                finishAct(user)
                            })
                        })

                }
            }
        }

    }

    private fun finishAct(user: User) {
        hideLoading()
        viewModel.saveUserToPreferences(user)
        val resultIntent = Intent()
        resultIntent.putExtra(PaymentActivity.INTENT_USER, user)

        when (intent.action) {
            ActionCode.CREATE -> {
                setResult(ResultCode.SUCCESS, resultIntent)
            }
            ActionCode.EDIT -> {
                resultIntent.putExtra(OrganizationProfileActivity.INTENT_ORGANIZATION, organization)
                setResult(ResultCode.SUCCESS, resultIntent)
            }
            else -> setResult(ResultCode.FAILURE)
        }

        finish()
    }

    private fun openImagePicker() {
        val intent = Intent(Intent.ACTION_OPEN_DOCUMENT)
        intent.addCategory(Intent.CATEGORY_OPENABLE)
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, false)
        intent.type = IMAGE_TYPE
        startActivityForResult(intent, RequestCode.IMAGE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        data ?: return

        when (requestCode) {
            RequestCode.IMAGE -> {
                data.data?.let { uri ->
                    getFileSize(uri)?.let {
                        if (it > MAX_IMAGE_SIZE) {
                            showMessage(getString(R.string.image_too_big, MAX_IMAGE_SIZE))
                            return
                        } else {
                            imageModel =
                                MediaModel(
                                    uri.toString(),
                                    MediaModel.MediaType.IMAGE
                                )
                            GlideApp.with(this).load(imageModel.uri)
                                .into(image_organization_picture)
                            isImageUpdated = true
                        }
                    }
                }
            }
        }
    }

    private fun getFileSize(uri: Uri): Long? {
        var size: Long? = null

        contentResolver.query(uri, null, null, null, null)?.use {
            it.moveToFirst()
            val sizeColumn = it.getColumnIndex(OpenableColumns.SIZE)
            size = it.getLong(sizeColumn)
            it.close()
        }

        return size
    }

    override fun onBackPressed() {
        if (intent.action == ActionCode.CREATE) {
            showMessage(getString(R.string.must_enter_data))
            return
        } else {
            setResult(ResultCode.FAILURE)
            super.onBackPressed()
        }
    }
}