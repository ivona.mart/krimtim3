/* eslint-disable promise/no-nesting */
const functions = require('firebase-functions');
const admin = require('firebase-admin')

admin.initializeApp();
const db = admin.firestore();

const USERS_COLLECTION = "users";
const EVENTS_COLLECTION = "events";
const NOTIFICATIONS_COLLECTION = "notifications";

class Event {
    constructor(id, title, description, eventType, location, startTime, endTime) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.eventType = eventType;
        this.location = location;
        this.startTime = startTime;
        this.endTime = endTime;
    }
}

class Notification {
    constructor(title, content, eventType, location, after, before) {
        this.title = title;
        this.content = content;
        this.eventType = eventType;
        this.location = location;
        this.after = after;
        this.before = before;
    }
}

class User {
    constructor(firstName, lastName, deviceToken) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.deviceToken = deviceToken;
    }
}

exports.sendNotif = functions.firestore
    .document(EVENTS_COLLECTION + '/{eventId}')
    .onCreate((eventSnap, context) => {
        const newValue = eventSnap.data();
        const event = new Event(
            eventSnap.id,
            newValue.title,
            newValue.description,
            newValue.eventType,
            newValue.location,
            newValue.startTime,
            newValue.endTime
        )

        console.log(`Event created: ${JSON.stringify(event)}`);

        const usersRef = db.collection(USERS_COLLECTION)

        usersRef.get()
            .then((userQuerySnap) => {
                userQuerySnap.docs.forEach((userSnap) => {
                    const data = userSnap.data()
                    const user = new User(data.firstName, data.lastName, data.deviceToken)

                    userSnap.ref.collection(NOTIFICATIONS_COLLECTION).get()
                        .then((notifQuerySnap) => {
                            notifQuerySnap.docs.forEach((notifSnap) => {
                                const data = notifSnap.data();
                                const notif = new Notification(data.title, data.content, data.eventType, data.location, data.after, data.before)

                                console.log(JSON.stringify(notif))

                                if (((notif.eventType !== null && event.eventType.includes(notif.eventType)) || notif.eventType === null) &&
                                    ((notif.location !== null && notif.location === event.location) || notif.location === null) &&
                                    ((notif.after !== null && notif.after <= event.startTime) || notif.after === null) &&
                                    ((notif.before !== null && notif.before >= event.startTime) || notif.before === null)) {
                                
                                    const payload = {
                                        notification: {
                                            title: `${notif.title}`,
                                            body: `${notif.content}`
                                        },
                                        data: {
                                            eventId: `${event.id}`
                                        }
                                    };

                                    console.log(`wanted notification: ${JSON.stringify(payload)}`)

                                    return admin.messaging().sendToDevice(user.deviceToken, payload)
                                        .then((response) => {
                                            console.log(response)
                                            return response
                                        })
                                        .catch((e) => {
                                            console.error(e)
                                        })
                                }
                                return 1
                            })
                            return 2
                        })
                        .catch((e) => console.error(e))
                })
                console.log("finished execution")
                return 3
            })
            .catch((e) => console.error(e))
    })