import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.ElementOption;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;

//example of all the things that need to be added to the event before event can be created

public class Automation {

    public static void main(String args[]) throws MalformedURLException, InterruptedException {

        //setup

        DesiredCapabilities dc = new DesiredCapabilities();

        dc.setCapability(MobileCapabilityType.DEVICE_NAME, "emulator-5554");
        dc.setCapability( "platformName", "android");
        dc.setCapability( "appPackage", "hr.fer.opp.zabavanet");
        dc.setCapability("appActivity", ".view.splash.SplashActivity");

        AndroidDriver<AndroidElement> ad = new AndroidDriver<AndroidElement>(new URL("http://127.0.0.1:4723/wd/hub"), dc);

        //login

        MobileElement el1 = (MobileElement) ad.findElementById("hr.fer.opp.zabavanet:id/text_email");
        el1.sendKeys("dgb.extra000@gmail.com");
        MobileElement el2 = (MobileElement) ad.findElementById("hr.fer.opp.zabavanet:id/text_password");
        el2.sendKeys("budala555");
        MobileElement el3 = (MobileElement) ad.findElementById("hr.fer.opp.zabavanet:id/btn_login");
        el3.click();
        Thread.sleep(3000);

        //start creating an event

        MobileElement el4 = (MobileElement) ad.findElementByAccessibilityId("My events");
        el4.click();
        MobileElement el5 = (MobileElement) ad.findElementById("hr.fer.opp.zabavanet:id/fab_add_event");
        el5.click();
        Thread.sleep(1000);

        //add start time

        MobileElement el6 = (MobileElement) ad.findElementById("hr.fer.opp.zabavanet:id/btn_edit_start_time");
        el6.click();
        MobileElement el7 = (MobileElement) ad.findElementById("android:id/button1");
        el7.click();
        Thread.sleep(500);
        MobileElement el8 = (MobileElement) ad.findElementById("android:id/button1");
        el8.click();

        //what happens if we try putting lower date as end time than the start time
        //add end time

        MobileElement el9 = (MobileElement) ad.findElementById("hr.fer.opp.zabavanet:id/btn_edit_end_time");
        el9.click();
        Thread.sleep(500);
        MobileElement el10 = (MobileElement) ad.findElementByAccessibilityId("02 January 2020");
        el10.click();
        MobileElement el11 = (MobileElement) ad.findElementById("android:id/button1");
        el11.click();
        MobileElement el12 = (MobileElement) ad.findElementById("android:id/button1");
        el12.click();
        //we get the message on the screen that we can't do this

        //we fill in the end time

        MobileElement el13 = (MobileElement) ad.findElementById("hr.fer.opp.zabavanet:id/btn_edit_end_time");
        el13.click();
        Thread.sleep(500);
        MobileElement el14 = (MobileElement) ad.findElementByAccessibilityId("31 January 2020");
        el14.click();
        MobileElement el15 = (MobileElement) ad.findElementById("android:id/button1");
        el15.click();
        MobileElement el16 = (MobileElement) ad.findElementById("android:id/button1");
        el16.click();


        //we try saving the event with just start time and end time

        MobileElement el17 = (MobileElement) ad.findElementById("hr.fer.opp.zabavanet:id/btn_save_event");
        el17.click();


        //we also need at least one picture

        MobileElement el18 = (MobileElement) ad.findElementById("hr.fer.opp.zabavanet:id/btn_add_images");
        el18.click();
        Thread.sleep(500);
        MobileElement el19 = (MobileElement) ad.findElementById("com.android.documentsui:id/icon_thumb");
        el19.click();

        //we scroll down to see the save button

        Thread.sleep(1500);
        TouchAction touchAction1 = new TouchAction(ad);
        touchAction1.press(ElementOption.element(el13))
                .waitAction(WaitOptions.waitOptions(Duration.ofMillis(500)))
                .moveTo(ElementOption.element(el6))
                .release().perform();

        //we try saving the event

        MobileElement el20 = (MobileElement) ad.findElementById("hr.fer.opp.zabavanet:id/btn_save_event");
        el20.click();

        //we also need to have event type

        MobileElement el21 = (MobileElement) ad.findElementById("hr.fer.opp.zabavanet:id/btn_edit_event_types");
        el21.click();
        MobileElement el22 = (MobileElement) ad.findElementByXPath("/hierarchy/android.widget.FrameLayout/" +
                "android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/" +
                "android.widget.FrameLayout/android.widget.ListView/android.widget.CheckedTextView[3]");
        el22.click();
        MobileElement el23 = (MobileElement) ad.findElementById("android:id/button1");
        el23.click();

        //we try saving the event
        Thread.sleep(500);
        MobileElement el24 = (MobileElement) ad.findElementById("hr.fer.opp.zabavanet:id/btn_save_event");
        el24.click();

        //add exact address
        MobileElement el50 = (MobileElement) ad.findElementById("hr.fer.opp.zabavanet:id/text_event_address");
        el50.sendKeys("Iz grada");

        // we need to fill all fields, we add event title and description

        MobileElement el25 = (MobileElement) ad.findElementById("hr.fer.opp.zabavanet:id/text_event_title");
        el25.sendKeys("testtest");
        MobileElement el26 = (MobileElement) ad.findElementById("hr.fer.opp.zabavanet:id/text_event_description");
        el26.sendKeys("opis testa");

        //we save the event now

        MobileElement el27 = (MobileElement) ad.findElementById("hr.fer.opp.zabavanet:id/btn_save_event");
        el27.click();

    }
}
