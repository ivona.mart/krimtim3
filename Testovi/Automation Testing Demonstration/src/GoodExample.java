import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.ElementOption;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;

//example that show the good example of creating an event

public class GoodExample {

    public static void main(String args[]) throws MalformedURLException, InterruptedException {

        //setup

        DesiredCapabilities dc = new DesiredCapabilities();

        dc.setCapability(MobileCapabilityType.DEVICE_NAME,"emulator-5554");
        dc.setCapability("platformName","android");
        dc.setCapability("appPackage","hr.fer.opp.zabavanet");
        dc.setCapability("appActivity",".view.splash.SplashActivity");

        AndroidDriver<AndroidElement> ad = new AndroidDriver<AndroidElement>(new URL("http://127.0.0.1:4723/wd/hub"), dc);

        //login

        MobileElement el1 = (MobileElement) ad.findElementById("hr.fer.opp.zabavanet:id/text_email");
        el1.sendKeys("dgb.extra000@gmail.com");
        MobileElement el2 = (MobileElement) ad.findElementById("hr.fer.opp.zabavanet:id/text_password");
        el2.sendKeys("budala555");
        MobileElement el3 = (MobileElement) ad.findElementById("hr.fer.opp.zabavanet:id/btn_login");
        el3.click();
        Thread.sleep(3000);

        //start creating event

        MobileElement el4 = (MobileElement) ad.findElementByAccessibilityId("My events");
        el4.click();
        MobileElement el5 = (MobileElement) ad.findElementById("hr.fer.opp.zabavanet:id/fab_add_event");
        el5.click();
        Thread.sleep(1000);

        //add picture

        MobileElement el6 = (MobileElement) ad.findElementById("hr.fer.opp.zabavanet:id/btn_add_images");
        el6.click();
        Thread.sleep(1000);
        MobileElement el7 = (MobileElement) ad.findElementById("com.android.documentsui:id/icon_thumb");
        el7.click();
        Thread.sleep(1000);

        //add title and description

        MobileElement el8 = (MobileElement) ad.findElementById("hr.fer.opp.zabavanet:id/text_event_title");
        el8.sendKeys("test");
        MobileElement el9 = (MobileElement) ad.findElementById("hr.fer.opp.zabavanet:id/text_event_description");
        el9.sendKeys("test");

        //add exact address
        MobileElement el50 = (MobileElement) ad.findElementById("hr.fer.opp.zabavanet:id/text_event_address");
        el50.sendKeys("Iz grada");



        //add event type

        MobileElement el10 = (MobileElement) ad.findElementById("hr.fer.opp.zabavanet:id/btn_edit_event_types");
        el10.click();
        MobileElement el11 = (MobileElement) ad.findElementByXPath("/hierarchy/android.widget.FrameLayout/" +
                "android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/" +
                "android.widget.FrameLayout/android.widget.ListView/android.widget.CheckedTextView[3]");
        el11.click();
        MobileElement el12 = (MobileElement) ad.findElementById("android:id/button1");
        el12.click();

        //add start time

        MobileElement el13 = (MobileElement) ad.findElementById("hr.fer.opp.zabavanet:id/btn_edit_start_time");
        el13.click();
        MobileElement el14 = (MobileElement) ad.findElementByAccessibilityId("14 January 2020");
        el14.click();
        MobileElement el15 = (MobileElement) ad.findElementById("android:id/button1");
        el15.click();
        MobileElement el16 = (MobileElement) ad.findElementById("android:id/button1");
        el16.click();

        //add end time

        MobileElement el17 = (MobileElement) ad.findElementById("hr.fer.opp.zabavanet:id/btn_edit_end_time");
        el17.click();
        MobileElement el18 = (MobileElement) ad.findElementByAccessibilityId("22 January 2020");
        el18.click();
        MobileElement el19 = (MobileElement) ad.findElementById("android:id/button1");
        el19.click();
        MobileElement el20 = ad.findElementById("android:id/button1");
        el20.click();
    Thread.sleep(1000);
        //scroll to see the save button

        TouchAction touchAction = new TouchAction(ad);
        touchAction.press(ElementOption.element(el17))
                .waitAction(WaitOptions.waitOptions(Duration.ofMillis(500)))
                .moveTo(ElementOption.element(el13))
                .release().perform();

        //save the created event

        MobileElement el21 = (MobileElement) ad.findElementById("hr.fer.opp.zabavanet:id/btn_save_event");
        el21.click();
        Thread.sleep(1000);

        MobileElement el22 = (MobileElement) ad.findElementByXPath("//android.widget.FrameLayout" +
                "[@content-desc=\"Events\"]/android.widget.ImageView");
        el22.click();

    }
}
