import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;

//example of trying to put too many pictures

public class TooManyPictures {

    public static void main(String args[]) throws MalformedURLException, InterruptedException {

        //setup

        DesiredCapabilities dc = new DesiredCapabilities();

        dc.setCapability(MobileCapabilityType.DEVICE_NAME, "emulator-5554");
        dc.setCapability( "platformName", "android");
        dc.setCapability( "appPackage", "hr.fer.opp.zabavanet");
        dc.setCapability("appActivity", ".view.splash.SplashActivity");

        AndroidDriver<AndroidElement> ad = new AndroidDriver<AndroidElement>(new URL("http://127.0.0.1:4723/wd/hub"), dc);

        //login

        MobileElement el1 = (MobileElement) ad.findElementById("hr.fer.opp.zabavanet:id/text_email");
        el1.sendKeys("dgb.extra000@gmail.com");
        MobileElement el2 = (MobileElement) ad.findElementById("hr.fer.opp.zabavanet:id/text_password");
        el2.sendKeys("budala555");
        MobileElement el3 = (MobileElement) ad.findElementById("hr.fer.opp.zabavanet:id/btn_login");
        el3.click();
        Thread.sleep(3000);

        //start creating an event

        MobileElement el4 = (MobileElement) ad.findElementByAccessibilityId("My events");
        el4.click();
        MobileElement el5 = (MobileElement) ad.findElementById("hr.fer.opp.zabavanet:id/fab_add_event");
        el5.click();
        Thread.sleep(1000);

        //let's add 10 pictures

        MobileElement el6 = (MobileElement) ad.findElementById("hr.fer.opp.zabavanet:id/btn_add_images");
        el6.click();
        Thread.sleep(500);
        MobileElement el7 = (MobileElement) ad.findElementByXPath("/hierarchy/android.widget.FrameLayout/" +
                "android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/" +
                "android.support.v4.widget.DrawerLayout/android.widget.LinearLayout/android.widget.FrameLayout/" +
                "android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/" +
                "android.view.ViewGroup/android.support.v7.widget.RecyclerView/android.widget.LinearLayout[1]/" +
                "android.widget.RelativeLayout/android.widget.FrameLayout/android.widget.ImageView");
        el7.click();
        Thread.sleep(1000);
        MobileElement el8 = (MobileElement) ad.findElementById("hr.fer.opp.zabavanet:id/btn_add_images");
        el8.click();
        Thread.sleep(500);
        MobileElement el9 = (MobileElement) ad.findElementByXPath("/hierarchy/android.widget.FrameLayout/" +
                "android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/" +
                "android.support.v4.widget.DrawerLayout/android.widget.LinearLayout/android.widget.FrameLayout/" +
                "android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/" +
                "android.view.ViewGroup/android.support.v7.widget.RecyclerView/android.widget.LinearLayout[2]/" +
                "android.widget.RelativeLayout/android.widget.FrameLayout/android.widget.ImageView[1]");
        el9.click();
        Thread.sleep(1000);
        MobileElement el10 = (MobileElement) ad.findElementById("hr.fer.opp.zabavanet:id/btn_add_images");
        el10.click();
        Thread.sleep(500);
        MobileElement el11 = (MobileElement) ad.findElementByXPath("/hierarchy/android.widget.FrameLayout/" +
                "android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/" +
                "android.support.v4.widget.DrawerLayout/android.widget.LinearLayout/android.widget.FrameLayout/" +
                "android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/" +
                "android.view.ViewGroup/android.support.v7.widget.RecyclerView/android.widget.LinearLayout[1]/" +
                "android.widget.RelativeLayout/android.widget.FrameLayout/android.widget.ImageView[1]");
        el11.click();
        Thread.sleep(1000);
        MobileElement el12 = (MobileElement) ad.findElementById("hr.fer.opp.zabavanet:id/btn_add_images");
        el12.click();
        Thread.sleep(500);
        MobileElement el13 = (MobileElement) ad.findElementByXPath("/hierarchy/android.widget.FrameLayout/" +
                "android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/" +
                "android.support.v4.widget.DrawerLayout/android.widget.LinearLayout/android.widget.FrameLayout/" +
                "android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/" +
                "android.view.ViewGroup/android.support.v7.widget.RecyclerView/android.widget.LinearLayout[1]/" +
                "android.widget.RelativeLayout/android.widget.FrameLayout/android.widget.ImageView[1]");
        el13.click();
        Thread.sleep(1000);
        MobileElement el14 = (MobileElement) ad.findElementById("hr.fer.opp.zabavanet:id/btn_add_images");
        el14.click();
        Thread.sleep(500);
        MobileElement el15 = (MobileElement) ad.findElementByXPath("/hierarchy/android.widget.FrameLayout/" +
                "android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/" +
                "android.support.v4.widget.DrawerLayout/android.widget.LinearLayout/android.widget.FrameLayout/" +
                "android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/" +
                "android.view.ViewGroup/android.support.v7.widget.RecyclerView/android.widget.LinearLayout[4]/" +
                "android.widget.RelativeLayout/android.widget.FrameLayout/android.widget.ImageView[1]");
        el15.click();
        Thread.sleep(1000);
        MobileElement el16 = (MobileElement) ad.findElementById("hr.fer.opp.zabavanet:id/btn_add_images");
        el16.click();
        Thread.sleep(500);
        MobileElement el17 = (MobileElement) ad.findElementByXPath("/hierarchy/android.widget.FrameLayout/" +
                "android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/" +
                "android.support.v4.widget.DrawerLayout/android.widget.LinearLayout/android.widget.FrameLayout/" +
                "android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/" +
                "android.view.ViewGroup/android.support.v7.widget.RecyclerView/android.widget.LinearLayout[3]/" +
                "android.widget.RelativeLayout/android.widget.FrameLayout/android.widget.ImageView[1]");
        el17.click();
        Thread.sleep(1000);
        MobileElement el18 = (MobileElement) ad.findElementById("hr.fer.opp.zabavanet:id/btn_add_images");
        el18.click();
        Thread.sleep(500);
        MobileElement el19 = (MobileElement) ad.findElementByXPath("/hierarchy/android.widget.FrameLayout/" +
                "android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/" +
                "android.support.v4.widget.DrawerLayout/android.widget.LinearLayout/android.widget.FrameLayout/" +
                "android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/" +
                "android.view.ViewGroup/android.support.v7.widget.RecyclerView/android.widget.LinearLayout[4]/" +
                "android.widget.RelativeLayout/android.widget.FrameLayout/android.widget.ImageView[1]");
        el19.click();
        Thread.sleep(1000);
        MobileElement el20 = (MobileElement) ad.findElementById("hr.fer.opp.zabavanet:id/btn_add_images");
        el20.click();
        Thread.sleep(500);
        MobileElement el21 = (MobileElement) ad.findElementByXPath("/hierarchy/android.widget.FrameLayout/" +
                "android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/" +
                "android.support.v4.widget.DrawerLayout/android.widget.LinearLayout/android.widget.FrameLayout/" +
                "android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/" +
                "android.view.ViewGroup/android.support.v7.widget.RecyclerView/android.widget.LinearLayout[1]/" +
                "android.widget.RelativeLayout/android.widget.FrameLayout/android.widget.ImageView[1]");
        el21.click();
        Thread.sleep(1000);
        MobileElement el22 = (MobileElement) ad.findElementById("hr.fer.opp.zabavanet:id/btn_add_images");
        el22.click();
        Thread.sleep(500);
        MobileElement el23 = (MobileElement) ad.findElementByXPath("/hierarchy/android.widget.FrameLayout/" +
                "android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/" +
                "android.support.v4.widget.DrawerLayout/android.widget.LinearLayout/" +
                "android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/" +
                "android.widget.LinearLayout/android.view.ViewGroup/android.support.v7.widget.RecyclerView/" +
                "android.widget.LinearLayout[3]/android.widget.RelativeLayout/android.widget.FrameLayout/" +
                "android.widget.ImageView[1]");
        el23.click();
        Thread.sleep(1000);
        MobileElement el24 = (MobileElement) ad.findElementById("hr.fer.opp.zabavanet:id/btn_add_images");
        el24.click();
        Thread.sleep(500);
        MobileElement el25 = (MobileElement) ad.findElementByXPath("/hierarchy/android.widget.FrameLayout/" +
                "android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/" +
                "android.support.v4.widget.DrawerLayout/android.widget.LinearLayout/android.widget.FrameLayout/" +
                "android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/" +
                "android.view.ViewGroup/android.support.v7.widget.RecyclerView/android.widget.LinearLayout[3]/" +
                "android.widget.RelativeLayout/android.widget.FrameLayout/android.widget.ImageView[1]");
        el25.click();
        Thread.sleep(1000);

        //now let's try adding another one

        MobileElement el26 = (MobileElement) ad.findElementById("hr.fer.opp.zabavanet:id/btn_add_images");
        el26.click();
        Thread.sleep(500);
        MobileElement el27 = (MobileElement) ad.findElementByXPath("/hierarchy/android.widget.FrameLayout/" +
                "android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/" +
                "android.support.v4.widget.DrawerLayout/android.widget.LinearLayout/android.widget.FrameLayout/" +
                "android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/" +
                "android.view.ViewGroup/android.support.v7.widget.RecyclerView/android.widget.LinearLayout[1]/" +
                "android.widget.RelativeLayout/android.widget.FrameLayout/android.widget.ImageView[1]");
        el27.click();
        //we get a message that we can't add more pictures

    }
}
